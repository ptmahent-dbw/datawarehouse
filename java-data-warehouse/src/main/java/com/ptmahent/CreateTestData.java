package com.ptmahent;

import com.ptmahent.CSVtoDB.CSVtoDbStores;
import com.ptmahent.CSVtoDB.CsvFileReader;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class CreateTestData {

    final static Logger logger = Logger.getLogger(CreateTestData.class);

    public static void main(String[] args) {

        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            Connection con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:ORCL","ptmahent","test@2016");
            con.setAutoCommit(true);

            ArrayList<String> sqlSequenceList = new ArrayList<String>();

            createAndResetSequences(con, sqlSequenceList);

            ArrayList<String> sqlTableList = new ArrayList<String>();
                    // 71 - 54 = 17


            /*

DELETE FROM OLTP_CUSTOMER;
DELETE FROM OLTP_SALES_PERSON;
DELETE FROM OLTP_STORE;
DELETE FROM OLTP_STORE_LOCATION;
DELETE FROM OLTP_DEMOGRAPHICS_BY_CITY;
DELETE FROM OLTP_PRODUCT_INVENTORY;
DELETE FROM OLTP_PRODUCT;
DELETE FROM OLTP_PRODUCT_AGE_GROUP;
DELETE FROM OLTP_PRODUCT_CATEGORY;
DELETE FROM OLTP_PRODUCT_INVENTORY;
DELETE FROM OLTP_PROVINCE;
DELETE FROM OLTP_CITY;
             */
            truncateOrDeleteTable(con, sqlTableList);
            //
            // DROP SEQUENCE oe.customers_seq;

            /*

            CREATE SEQUENCE customers_seq
             START WITH     1000
             INCREMENT BY   1
             NOCACHE
             NOCYCLE;
             */

            System.out.println("LOOPING THROUGH CSV FILES");
            // Insert Cities
            /*
            for (Map.Entry<String, String[]> fileMapping: CsvFileReader.fileHeaderMappings.entrySet()) {
                System.out.println(fileMapping.getKey());
                CsvFileReader.readCsvFile(fileMapping.getKey(), con);
            }*/

            //1: CSV_FILE_CITIES
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_CITIES, con);

            //2: CSV_FILE_PROVINCE
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_PROVINCE, con);

            //3: CSV_FILE_AGE_GROUP
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_AGE_GROUP, con);

            //4: CSV_FILE_CATEGORIES
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_CATEGORIES, con);

            //5: CSV_FILE_STORES
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_STORES, con);

            //6:  CSV_FILE_SALES_PERSON
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_SALES_PERSON, con);

            //7: CSV_FILE_PRODUCTS
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_PRODUCTS, con);


            //8: CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN, con);

            //9: CSV_FILE_DEMOGRAPHICS
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_DEMOGRAPHICS, con);

            //10: CSV_FILE_BABY_NAMES_MALE
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_BABY_NAMES_MALE, con);

            //11: CSV_FILE_BABY_NAMES_FEMALE
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_BABY_NAMES_FEMALE, con);


            // 12: CUSTOMER CSV
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_CUSTOMERS, con);

            //13: CSV_FILE_SALES_ORDER
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_SALES_ORDER, con);

            //14: CSV_FILE_SALES_ORDER_LINE
            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_SALES_ORDER_LINE, con);





            /*
            PreparedStatement pstmtSimpleQuery = con.prepareStatement("SELECT * FROM PTMAHENT.OLTP_PRODUCT_AGE_GROUP");
            ResultSet rs = pstmtSimpleQuery.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("PROD_AG_CODE"));
                System.out.println(rs.getLong("PROD_AG_ID"));
            }
            rs.close();

            CsvFileReader.readCsvFile(CsvFileReader.CSV_FILE_AGE_GROUP, con);
            */
                // Insert Provinces



                // Insert Store

            // Insert Store Location

                // Insert Customer


                // Insert Product Category

                // Insert Product Age Group


                // Insert Product Inventory

                // Insert Sales Order

                // Insert Sales Order Line Item

                // Sale

            con.close();

        } catch(Exception ex) {
            ex.printStackTrace();
        }

    }

    private static void truncateOrDeleteTable(Connection con, ArrayList<String> sqlTableList) {
        sqlTableList.add("PTMAHENT.OLTP_SALE_ORDER_LINE");
        sqlTableList.add("PTMAHENT.OLTP_SALE_ORDER");

        sqlTableList.add("PTMAHENT.OLTP_CUSTOMER");
        sqlTableList.add("PTMAHENT.OLTP_SALES_PERSON");
        sqlTableList.add("PTMAHENT.OLTP_STORE");
        sqlTableList.add("PTMAHENT.OLTP_STORE_LOCATION");

        sqlTableList.add("PTMAHENT.OLTP_DEMOGRAPHICS_BY_CITY");
        sqlTableList.add("PTMAHENT.OLTP_PRODUCT_INVENTORY");
        sqlTableList.add("PTMAHENT.OLTP_PRODUCT");
        sqlTableList.add("PTMAHENT.OLTP_PRODUCT_AGE_GROUP");
        sqlTableList.add("PTMAHENT.OLTP_PRODUCT_CATEGORY");


        sqlTableList.add("PTMAHENT.OLTP_AVERAGE_LIFE_SPAN");
        sqlTableList.add("PTMAHENT.OLTP_TOP_BABY_NAMES");
        sqlTableList.add("PTMAHENT.OLTP_BABY_FREQUENCY_PER_YEAR");

        sqlTableList.add("PTMAHENT.OLTP_BABY_FREQUENCY_PER_YEAR");

        sqlTableList.add("PTMAHENT.OLTP_CITY");
        sqlTableList.add("PTMAHENT.OLTP_PROVINCE");


        for (String sqlTable : sqlTableList) {

            try (Statement stmtTruncateTbl = con.createStatement()) {
                stmtTruncateTbl.execute("TRUNCATE TABLE " + sqlTable);

                System.out.println("TRUNCATING TABLE " + sqlTable + " - succeeded");
                stmtTruncateTbl.close();

            } catch(SQLException ex) {
                System.out.println(" TRUNCATING TABLE " + sqlTable + " - failed");

                try (Statement stmtDeleteRows = con.createStatement() ) {
                    stmtDeleteRows.execute("DELETE FROM " + sqlTable);
                    stmtDeleteRows.getConnection().commit();
                    stmtDeleteRows.close();
                    System.out.println("DELETE ALL ROWS from " + sqlTable + " - success");
                } catch (SQLException sqlEx) {
                    System.out.println("DELETE ALL ROWS from " + sqlTable + " - failed");
                }


            }
        }
    }

    private static void createAndResetSequences(Connection con, ArrayList<String> sqlSequenceList) {
        sqlSequenceList.add("PTMAHENT.OLTP_PRODUCT_AGE_GROUP_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_AVERAGE_LIFE_SPAN_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_PRODUCT_CATEGORY_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_CITY_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_TOP_BABY_NAMES_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_CUSTOMER_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_PROVINCE_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_PRODUCT_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_STORE_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_PROVINCE_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_SALES_PERSON_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_SALES_ORDER_SEQ");
        sqlSequenceList.add("PTMAHENT.OLTP_SALES_ORDER_LINE_SEQ");

        for (String seq: sqlSequenceList) {
            String sSqlDropSeq =
                    " DROP SEQUENCE " + seq;

            System.out.println(sSqlDropSeq);

            try (Statement stmtDropSeq = con.createStatement()){

                stmtDropSeq.execute(sSqlDropSeq);
                stmtDropSeq.close();
                System.out.println("DROP SEQUENCE " + seq + " - SUCCEEDED");
            } catch (SQLException ex) {
                System.out.println("DROP SEQUENCE " + seq + " - FAILED");
            }
            System.out.println("DROPPING SEQ " + seq);
            // CREATE SEQUENCE DIM_FISCAL_PERIOD_SEQ INCREMENT BY 1
            // MAXVALUE 9999999999999999999999999999 MINVALUE 1 CACHE 20;

            String sSqlCreateSeq = "CREATE SEQUENCE " + seq + " INCREMENT BY 1 " +
                    " MAXVALUE 999999999999999 MINVALUE 1 CACHE 20";

            try (Statement stmtCreateSeq = con.createStatement()) {
                System.out.println(sSqlCreateSeq);
                stmtCreateSeq.execute(sSqlCreateSeq);
                stmtCreateSeq.close();
                System.out.println("CREATING SEQ " + seq + " - SUCCEEDED");

            } catch (SQLException ex) {
                System.out.println("CREATING SEQ " + seq + " - FAILED");
            }

        }
    }
}
