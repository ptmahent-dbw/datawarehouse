package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class StoreLocation {

    private long storeLocationId;
    private Store store;
    private Province province;
    private City city;
    private String address;

    public StoreLocation() {
    }

    public StoreLocation(long storeLocationId, Store store, Province province, City city, String address) {
        this.storeLocationId = storeLocationId;
        this.store = store;
        this.province = province;
        this.city = city;
        this.address = address;
    }

    @Override
    public String toString() {
        return "StoreLocation{" +
                "storeLocationId=" + storeLocationId +
                ", store=" + store +
                ", province=" + province +
                ", city=" + city +
                ", address='" + address + '\'' +
                '}';
    }

    public long getStoreLocationId() {
        return storeLocationId;
    }

    public void setStoreLocationId(long storeLocationId) {
        this.storeLocationId = storeLocationId;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
