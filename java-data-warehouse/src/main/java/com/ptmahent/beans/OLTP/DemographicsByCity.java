package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class DemographicsByCity {
    private City city;
    private Province province;
    private long year;
    private double population;

    public DemographicsByCity(City city, long year, double population) {
        this.city = city;
        this.year = year;
        this.population = population;
    }

    public DemographicsByCity(City city, long year) {

        this.city = city;
        this.year = year;
    }

    public DemographicsByCity(City city) {

        this.city = city;
    }

    public DemographicsByCity() {

    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public double getPopulation() {
        return population;
    }

    public void setPopulation(double population) {
        this.population = population;
    }

    @Override
    public String toString() {
        return "DemographicsByCity{" +
                "city=" + city +
                ", province=" + province +
                ", year=" + year +
                ", population=" + population +
                '}';
    }
}
