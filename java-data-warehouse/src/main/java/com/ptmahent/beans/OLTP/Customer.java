package com.ptmahent.beans.OLTP;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Customer {
    private long customerId;
    private String firstName;
    private String lastName;
    private String gender;
    private Date dob;
    private String address;
    private String phoneNumber;
    private String emailAddr;
    private City city;
    private Province prov;

    public Customer() {
    }

    public Customer(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Customer(long customerId, String firstName, String lastName, String gender, Date dob, String address, String phoneNumber, String emailAddr, City city, Province prov) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.emailAddr = emailAddr;
        this.city = city;
        this.prov = prov;
    }

    public Customer(String firstName, String lastName, String gender, String emailAddr) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.emailAddr = emailAddr;
    }

    public Customer(long customerId) {
        this.customerId = customerId;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddr() {
        return emailAddr;
    }

    public void setEmailAddr(String emailAddr) {
        this.emailAddr = emailAddr;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Province getProv() {
        return prov;
    }

    public void setProv(Province prov) {
        this.prov = prov;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", dob=" + dob +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddr='" + emailAddr + '\'' +
                ", city=" + city +
                ", prov=" + prov +
                '}';
    }
}
