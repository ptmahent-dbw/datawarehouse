package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Store {
    private long storeId;
    private String storeName;
    private Province prov;
    private City city;
    private String address;


    public Store() {
    }

    public Store(String storeName) {
        this.storeName = storeName;
    }

    public Store(long storeId, String storeName) {
        this.storeId = storeId;
        this.storeName = storeName;
    }

    @Override
    public String toString() {
        return "Store{" +
                "storeId=" + storeId +
                ", storeName='" + storeName + '\'' +
                '}';
    }

    public long getStoreId() {
        return storeId;
    }

    public void setStoreId(long storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Province getProv() {
        return prov;
    }

    public void setProv(Province prov) {
        this.prov = prov;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
