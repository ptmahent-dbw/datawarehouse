package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProductCategory {
    private long productCategoryId;
    private String productCategoryName;
    private String productCategoryDesc;

    public ProductCategory(String productCategoryName) {

        this.productCategoryName = productCategoryName;
    }

    public ProductCategory(long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public ProductCategory() {


    }

    public long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getProductCategoryName() {
        return productCategoryName;
    }

    public void setProductCategoryName(String productCategoryName) {
        this.productCategoryName = productCategoryName;
    }

    public String getProductCategoryDesc() {
        return productCategoryDesc;
    }

    public void setProductCategoryDesc(String productCategoryDesc) {
        this.productCategoryDesc = productCategoryDesc;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "productCategoryId=" + productCategoryId +
                ", productCategoryName='" + productCategoryName + '\'' +
                ", productCategoryDesc='" + productCategoryDesc + '\'' +
                '}';
    }
}
