package com.ptmahent.beans.OLTP;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProductInventory {
    private long productId;
    private long itemsInStock;
    private long itemsOrderFromVendor;
    private long shelf;
    private long bin;
    private Date modifiedDate;

    public ProductInventory(long productId, long itemsInStock, long itemsOrderFromVendor, long shelf, long bin, Date modifiedDate) {
        this.productId = productId;
        this.itemsInStock = itemsInStock;
        this.itemsOrderFromVendor = itemsOrderFromVendor;
        this.shelf = shelf;
        this.bin = bin;
        this.modifiedDate = modifiedDate;
    }

    public ProductInventory(long productId) {

        this.productId = productId;
    }

    public ProductInventory() {

    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getItemsInStock() {
        return itemsInStock;
    }

    public void setItemsInStock(long itemsInStock) {
        this.itemsInStock = itemsInStock;
    }

    public long getItemsOrderFromVendor() {
        return itemsOrderFromVendor;
    }

    public void setItemsOrderFromVendor(long itemsOrderFromVendor) {
        this.itemsOrderFromVendor = itemsOrderFromVendor;
    }

    public long getShelf() {
        return shelf;
    }

    public void setShelf(long shelf) {
        this.shelf = shelf;
    }

    public long getBin() {
        return bin;
    }

    public void setBin(long bin) {
        this.bin = bin;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String toString() {
        return "ProductInventory{" +
                "productId=" + productId +
                ", itemsInStock=" + itemsInStock +
                ", itemsOrderFromVendor=" + itemsOrderFromVendor +
                ", shelf=" + shelf +
                ", bin=" + bin +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}
