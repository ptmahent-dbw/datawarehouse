package com.ptmahent.beans.OLTP;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class SalesOrder {
    private long salesOrderId;
    private Store store;
    private SalesPerson salesPerson;
    private Date salesDate;
    private double totalSalesBeforeTax;
    private double totalDiscount;
    private double totalTaxAmount;
    private double totalAmountCharged;
    private String paymentMethod;
    private Customer customer;

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    private String customerAwareByAdCompaign;
    private long invoiceId;

    public SalesOrder() {
    }



    public SalesOrder(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public long getSalesOrderId() {
        return salesOrderId;
    }

    public void setSalesOrderId(long salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public double getTotalSalesBeforeTax() {
        return totalSalesBeforeTax;
    }

    public void setTotalSalesBeforeTax(double totalSalesBeforeTax) {
        this.totalSalesBeforeTax = totalSalesBeforeTax;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public double getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(double totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public double getTotalAmountCharged() {
        return totalAmountCharged;
    }

    public void setTotalAmountCharged(double totalAmountCharged) {
        this.totalAmountCharged = totalAmountCharged;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    public String getCustomerAwareByAdCompaign() {
        return customerAwareByAdCompaign;
    }

    public void setCustomerAwareByAdCompaign(String customerAwareByAdCompaign) {
        this.customerAwareByAdCompaign = customerAwareByAdCompaign;
    }

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }
}
