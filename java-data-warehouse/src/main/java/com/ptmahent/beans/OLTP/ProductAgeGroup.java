package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProductAgeGroup {
    private long productAgeGroupId;
    private String productAgeGroupCode;
    private String productAgeGroupDesc;
    private long productMinAge;
    private long productMaxAge;

    public ProductAgeGroup(long productAgeGroupId) {
        this.productAgeGroupId = productAgeGroupId;
    }

    public ProductAgeGroup(long productAgeGroupId, String productAgeGroupCode, long productMinAge, long productMaxAge) {

        this.productAgeGroupId = productAgeGroupId;
        this.productAgeGroupCode = productAgeGroupCode;
        this.productMinAge = productMinAge;
        this.productMaxAge = productMaxAge;
    }

    public ProductAgeGroup(String productAgeGroupCode) {

        this.productAgeGroupCode = productAgeGroupCode;
    }

    public ProductAgeGroup() {

    }

    public long getProductAgeGroupId() {
        return productAgeGroupId;
    }

    public void setProductAgeGroupId(long productAgeGroupId) {
        this.productAgeGroupId = productAgeGroupId;
    }

    public String getProductAgeGroupCode() {
        return productAgeGroupCode;
    }

    public void setProductAgeGroupCode(String productAgeGroupCode) {
        this.productAgeGroupCode = productAgeGroupCode;
    }

    public String getProductAgeGroupDesc() {
        return productAgeGroupDesc;
    }

    public void setProductAgeGroupDesc(String productAgeGroupDesc) {
        this.productAgeGroupDesc = productAgeGroupDesc;
    }

    public long getProductMinAge() {
        return productMinAge;
    }

    public void setProductMinAge(long productMinAge) {
        this.productMinAge = productMinAge;
    }

    public long getProductMaxAge() {
        return productMaxAge;
    }

    public void setProductMaxAge(long productMaxAge) {
        this.productMaxAge = productMaxAge;
    }

    @Override
    public String toString() {
        return "ProductAgeGroup{" +
                "productAgeGroupId=" + productAgeGroupId +
                ", productAgeGroupCode='" + productAgeGroupCode + '\'' +
                ", productAgeGroupDesc='" + productAgeGroupDesc + '\'' +
                ", productMinAge=" + productMinAge +
                ", productMaxAge=" + productMaxAge +
                '}';
    }
}
