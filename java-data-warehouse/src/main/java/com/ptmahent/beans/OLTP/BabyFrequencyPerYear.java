package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class BabyFrequencyPerYear {
    private long babyNameId;
    private long year;
    private long frequency;

    public long getBabyNameId() {
        return babyNameId;
    }

    public void setBabyNameId(long babyNameId) {
        this.babyNameId = babyNameId;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }

    @Override
    public String toString() {
        return "BabyFrequencyPerYear{" +
                "babyNameId=" + babyNameId +
                ", year=" + year +
                ", frequency=" + frequency +
                '}';
    }
}
