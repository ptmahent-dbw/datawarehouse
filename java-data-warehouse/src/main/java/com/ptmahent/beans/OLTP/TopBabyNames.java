package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class TopBabyNames {
    private long babyNameId;
    private String babyName;
    private String gender;

    public TopBabyNames(long babyNameId, String babyName, String gender) {
        this.babyNameId = babyNameId;
        this.babyName = babyName;
        this.gender = gender;
    }

    public TopBabyNames() {
    }

    @Override
    public String toString() {
        return "TopBabyNames{" +
                "babyNameId=" + babyNameId +
                ", babyName='" + babyName + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }

    public long getBabyNameId() {
        return babyNameId;
    }

    public void setBabyNameId(long babyNameId) {
        this.babyNameId = babyNameId;
    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
