package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class SalesOrderLine {

    private long salesOrderLineId;
    private SalesOrder salesOrder;
    private Product product;
    private double price;
    private double discount;
    private long prodCount;
    private double lineTotal;

    public long getProdCount() {
        return prodCount;
    }

    public void setProdCount(long prodCount) {
        this.prodCount = prodCount;
    }

    public SalesOrderLine() {

    }


    public SalesOrderLine(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public SalesOrderLine(Product product) {
        this.product = product;
    }

    public SalesOrderLine(long salesOrderLineId, SalesOrder salesOrder, Product product, double price, double discount, double lineTotal) {
        this.salesOrderLineId = salesOrderLineId;
        this.salesOrder = salesOrder;
        this.product = product;
        this.price = price;
        this.discount = discount;
        this.lineTotal = lineTotal;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public long getSalesOrderLineId() {
        return salesOrderLineId;
    }

    public void setSalesOrderLineId(long salesOrderLineId) {
        this.salesOrderLineId = salesOrderLineId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(double lineTotal) {
        this.lineTotal = lineTotal;
    }
}
