package com.ptmahent.beans.OLTP;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class AverageLifeSpan {
    private long averageLifeSpanId;
    private long refStartYear;
    private long refEndYear;
    private String geo;
    private String gender;
    private String ageGroup;
    private double lifeSpan;

    public long getAverageLifeSpanId() {
        return averageLifeSpanId;
    }

    public void setAverageLifeSpanId(long averageLifeSpanId) {
        this.averageLifeSpanId = averageLifeSpanId;
    }

    public long getRefStartYear() {
        return refStartYear;
    }

    public void setRefStartYear(long refStartYear) {
        this.refStartYear = refStartYear;
    }

    public long getRefEndYear() {
        return refEndYear;
    }

    public void setRefEndYear(long refEndYear) {
        this.refEndYear = refEndYear;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public double getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(double lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    @Override
    public String toString() {
        return "AverageLifeSpan{" +
                "averageLifeSpanId=" + averageLifeSpanId +
                ", refStartYear=" + refStartYear +
                ", refEndYear=" + refEndYear +
                ", geo='" + geo + '\'' +
                ", gender='" + gender + '\'' +
                ", ageGroup='" + ageGroup + '\'' +
                ", lifeSpan=" + lifeSpan +
                '}';
    }
}
