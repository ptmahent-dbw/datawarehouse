package com.ptmahent.beans.OLTP;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Product {
    private long productId;
    private ProductCategory productCategory;
    private ProductAgeGroup productAgeGroup;
    private String productName;
    private double productCost;
    private double productSellingPrice;

    public Product() {
    }

    public Product(long productId, ProductCategory productCategory, ProductAgeGroup productAgeGroup, String productName, double productCost, double productSellingPrice) {
        this.productId = productId;
        this.productCategory = productCategory;
        this.productAgeGroup = productAgeGroup;
        this.productName = productName;
        this.productCost = productCost;
        this.productSellingPrice = productSellingPrice;
    }

    public Product(long productId) {

        this.productId = productId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public ProductAgeGroup getProductAgeGroup() {
        return productAgeGroup;
    }

    public void setProductAgeGroup(ProductAgeGroup productAgeGroup) {
        this.productAgeGroup = productAgeGroup;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductCost() {
        return productCost;
    }

    public void setProductCost(double productCost) {
        this.productCost = productCost;
    }

    public double getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(double productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId=" + productId +
                ", productCategory=" + productCategory +
                ", productAgeGroup=" + productAgeGroup +
                ", productName='" + productName + '\'' +
                ", productCost=" + productCost +
                ", productSellingPrice=" + productSellingPrice +
                '}';
    }
}
