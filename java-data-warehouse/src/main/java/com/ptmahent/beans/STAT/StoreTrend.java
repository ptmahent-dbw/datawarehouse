package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class StoreTrend {
    private long fiscalPeriod;
    private long storeKey;
    private String trend;

    public long getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(long fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }

    public long getStoreKey() {
        return storeKey;
    }

    public void setStoreKey(long storeKey) {
        this.storeKey = storeKey;
    }

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }
}
