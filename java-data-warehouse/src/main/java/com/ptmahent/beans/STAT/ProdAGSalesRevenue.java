package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProdAGSalesRevenue {
    private long fiscalPeriodKey;
    private long prodAgKey;
    private double salesRevenue;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getProdAgKey() {
        return prodAgKey;
    }

    public void setProdAgKey(long prodAgKey) {
        this.prodAgKey = prodAgKey;
    }

    public double getSalesRevenue() {
        return salesRevenue;
    }

    public void setSalesRevenue(double salesRevenue) {
        this.salesRevenue = salesRevenue;
    }
}
