package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProdSalesRevenueVolume {
    private long fiscalPeriodKey;
    private long prodKey;
    private double salesRevenue;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getProdKey() {
        return prodKey;
    }

    public void setProdKey(long prodKey) {
        this.prodKey = prodKey;
    }

    public double getSalesRevenue() {
        return salesRevenue;
    }

    public void setSalesRevenue(double salesRevenue) {
        this.salesRevenue = salesRevenue;
    }
}
