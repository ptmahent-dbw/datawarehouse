package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProdAGSalesVolume {

    private long fiscalPeriodKey;
    private long prodAGId;
    private String prodAGCode;
    private long prodMinAge;
    private long prodMaxAge;
    private double salesVolume;
    private long prodAGKey;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getProdAGId() {
        return prodAGId;
    }

    public void setProdAGId(long prodAGId) {
        this.prodAGId = prodAGId;
    }

    public String getProdAGCode() {
        return prodAGCode;
    }

    public void setProdAGCode(String prodAGCode) {
        this.prodAGCode = prodAGCode;
    }

    public long getProdMinAge() {
        return prodMinAge;
    }

    public void setProdMinAge(long prodMinAge) {
        this.prodMinAge = prodMinAge;
    }

    public long getProdMaxAge() {
        return prodMaxAge;
    }

    public void setProdMaxAge(long prodMaxAge) {
        this.prodMaxAge = prodMaxAge;
    }

    public double getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(double salesVolume) {
        this.salesVolume = salesVolume;
    }

    public long getProdAGKey() {
        return prodAGKey;
    }

    public void setProdAGKey(long prodAGKey) {
        this.prodAGKey = prodAGKey;
    }
}
