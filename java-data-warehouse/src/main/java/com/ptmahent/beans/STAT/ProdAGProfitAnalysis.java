package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProdAGProfitAnalysis {
    private long fiscalPeriodKey;
    private long prodAGKey;
    private double profit;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getProdAGKey() {
        return prodAGKey;
    }

    public void setProdAGKey(long prodAGKey) {
        this.prodAGKey = prodAGKey;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}
