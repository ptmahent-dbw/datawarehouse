package com.ptmahent.beans.STAT;

import com.ptmahent.beans.DIM.Store;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class StoreAnalysisByFiscal {
    private long fiscalPeriodKey;
    private Store storeRevenueLease;
    private Store storeRevenueMost;
    private Store storeProfitLeast;
    private Store storeProfitMost;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public Store getStoreRevenueLease() {
        return storeRevenueLease;
    }

    public void setStoreRevenueLease(Store storeRevenueLease) {
        this.storeRevenueLease = storeRevenueLease;
    }

    public Store getStoreRevenueMost() {
        return storeRevenueMost;
    }

    public void setStoreRevenueMost(Store storeRevenueMost) {
        this.storeRevenueMost = storeRevenueMost;
    }

    public Store getStoreProfitLeast() {
        return storeProfitLeast;
    }

    public void setStoreProfitLeast(Store storeProfitLeast) {
        this.storeProfitLeast = storeProfitLeast;
    }

    public Store getStoreProfitMost() {
        return storeProfitMost;
    }

    public void setStoreProfitMost(Store storeProfitMost) {
        this.storeProfitMost = storeProfitMost;
    }
}
