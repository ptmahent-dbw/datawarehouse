package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class AdditionalStatsByFg {
    private long fiscalPeriod;
    private String nameMostSuccess;
    private String genderMostSuccess;
    private long topSalesPersonKey;
    private double cashPaymentMethodPercent;
    private double creditPaymentMethodPercent;
    private double salesDrivenByAdPercent;

    public long getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(long fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }

    public String getNameMostSuccess() {
        return nameMostSuccess;
    }

    public void setNameMostSuccess(String nameMostSuccess) {
        this.nameMostSuccess = nameMostSuccess;
    }

    public String getGenderMostSuccess() {
        return genderMostSuccess;
    }

    public void setGenderMostSuccess(String genderMostSuccess) {
        this.genderMostSuccess = genderMostSuccess;
    }

    public long getTopSalesPersonKey() {
        return topSalesPersonKey;
    }

    public void setTopSalesPersonKey(long topSalesPersonKey) {
        this.topSalesPersonKey = topSalesPersonKey;
    }

    public double getCashPaymentMethodPercent() {
        return cashPaymentMethodPercent;
    }

    public void setCashPaymentMethodPercent(double cashPaymentMethodPercent) {
        this.cashPaymentMethodPercent = cashPaymentMethodPercent;
    }

    public double getCreditPaymentMethodPercent() {
        return creditPaymentMethodPercent;
    }

    public void setCreditPaymentMethodPercent(double creditPaymentMethodPercent) {
        this.creditPaymentMethodPercent = creditPaymentMethodPercent;
    }

    public double getSalesDrivenByAdPercent() {
        return salesDrivenByAdPercent;
    }

    public void setSalesDrivenByAdPercent(double salesDrivenByAdPercent) {
        this.salesDrivenByAdPercent = salesDrivenByAdPercent;
    }
}
