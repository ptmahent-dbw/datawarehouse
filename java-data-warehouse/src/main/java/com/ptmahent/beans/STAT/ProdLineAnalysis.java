package com.ptmahent.beans.STAT;

import com.ptmahent.beans.DIM.Product;
import com.ptmahent.beans.OLTP.ProductAgeGroup;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProdLineAnalysis {
    private long fiscalPeriodKey;
    private Product prodMost;
    private Product prodLeast;
    private ProductAgeGroup prodAgMost;
    private ProductAgeGroup prodAgLeast;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public Product getProdMost() {
        return prodMost;
    }

    public void setProdMost(Product prodMost) {
        this.prodMost = prodMost;
    }

    public Product getProdLeast() {
        return prodLeast;
    }

    public void setProdLeast(Product prodLeast) {
        this.prodLeast = prodLeast;
    }

    public ProductAgeGroup getProdAgMost() {
        return prodAgMost;
    }

    public void setProdAgMost(ProductAgeGroup prodAgMost) {
        this.prodAgMost = prodAgMost;
    }

    public ProductAgeGroup getProdAgLeast() {
        return prodAgLeast;
    }

    public void setProdAgLeast(ProductAgeGroup prodAgLeast) {
        this.prodAgLeast = prodAgLeast;
    }
}
