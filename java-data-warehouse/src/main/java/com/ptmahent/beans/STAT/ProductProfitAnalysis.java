package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProductProfitAnalysis {
    private long fiscalPeriodKey;
    private long productKey;
    private double profit;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getProductKey() {
        return productKey;
    }

    public void setProductKey(long productKey) {
        this.productKey = productKey;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}
