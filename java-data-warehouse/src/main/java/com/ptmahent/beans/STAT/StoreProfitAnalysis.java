package com.ptmahent.beans.STAT;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class StoreProfitAnalysis {
    private long fiscalPeriodKey;
    private long storeKey;
    private double profit;

    public long getFiscalPeriodKey() {
        return fiscalPeriodKey;
    }

    public void setFiscalPeriodKey(long fiscalPeriodKey) {
        this.fiscalPeriodKey = fiscalPeriodKey;
    }

    public long getStoreKey() {
        return storeKey;
    }

    public void setStoreKey(long storeKey) {
        this.storeKey = storeKey;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }
}
