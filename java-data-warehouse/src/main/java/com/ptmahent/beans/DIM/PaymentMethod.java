package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class PaymentMethod {
    private long paymentMethodKey;
    private String paymentMethod;

    public long getPaymentMethodKey() {
        return paymentMethodKey;
    }

    public void setPaymentMethodKey(long paymentMethodKey) {
        this.paymentMethodKey = paymentMethodKey;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
