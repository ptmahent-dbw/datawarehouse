package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class City {
    private long cityKey;
    private String cityName;
    private long cityId;

    public long getCityKey() {
        return cityKey;
    }

    public void setCityKey(long cityKey) {
        this.cityKey = cityKey;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }
}
