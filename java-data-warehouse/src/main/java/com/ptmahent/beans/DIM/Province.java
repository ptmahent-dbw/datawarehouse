package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Province {
    private long provKey;
    private String provName;
    private long provId;

    public long getProvKey() {
        return provKey;
    }

    public void setProvKey(long provKey) {
        this.provKey = provKey;
    }

    public String getProvName() {
        return provName;
    }

    public void setProvName(String provName) {
        this.provName = provName;
    }

    public long getProvId() {
        return provId;
    }

    public void setProvId(long provId) {
        this.provId = provId;
    }
}
