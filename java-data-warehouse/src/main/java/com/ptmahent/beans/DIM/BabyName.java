package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-12.
 */
public class BabyName {
    private long babyNameKey;
    private long babyNameId;
    private String babyName;
    private String gender;
    private long babyYear;
    private long frequency;

    public BabyName() {
    }

    public BabyName(long babyNameKey, long babyNameId, String babyName, String gender) {
        this.babyNameKey = babyNameKey;
        this.babyNameId = babyNameId;
        this.babyName = babyName;
        this.gender = gender;
    }

    public long getBabyNameKey() {
        return babyNameKey;
    }

    public long getBabyYear() {
        return babyYear;
    }

    public void setBabyYear(long babyYear) {
        this.babyYear = babyYear;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }

    public void setBabyNameKey(long babyNameKey) {
        this.babyNameKey = babyNameKey;
    }

    public long getBabyNameId() {
        return babyNameId;
    }

    public void setBabyNameId(long babyNameId) {
        this.babyNameId = babyNameId;
    }

    public String getBabyName() {
        return babyName;
    }

    public void setBabyName(String babyName) {
        this.babyName = babyName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
