package com.ptmahent.beans.DIM;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class DimDate {
    private long instanceDateKey;
    private Date instanceDate;


    public DimDate(long instanceDateKey, Date instanceDate) {
        this.instanceDateKey = instanceDateKey;
        this.instanceDate = instanceDate;

    }

    public DimDate() {

    }

    public long getFSYear() {

        Calendar cal = Calendar.getInstance();
        cal.setTime(instanceDate);

        return cal.get(Calendar.YEAR);
    }

    public int getFSMonth() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(instanceDate);

        return cal.get(Calendar.MONTH) + 1;
    }

    public long getFSQuarter() {
        int month = getFSMonth();
        if (month <= 3) {
            return 1;
        } else if (month <= 6) {
            return 2;
        } else if (month <= 9){
            return 3;
        } else {
            return 4;
        }
    }

    public long getInstanceDateKey() {

        return instanceDateKey;
    }

    public void setInstanceDateKey(long instanceDateKey) {
        this.instanceDateKey = instanceDateKey;
    }

    public Date getInstanceDate() {
        return instanceDate;
    }

    public void setInstanceDate(Date instanceDate) {
        this.instanceDate = instanceDate;
    }


    public String getFiscalQuarter() {

        return  "" + getFSYear() + "Q" + getFSQuarter();
    }
}
