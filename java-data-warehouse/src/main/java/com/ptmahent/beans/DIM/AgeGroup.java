package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class AgeGroup {
    private long ageGroupId;
    private String ageGroupCode;
    private String ageGroupDesc;
    private long ageGroupKey;
    private long minAge;
    private long maxAge;

    public long getAgeGroupId() {
        return ageGroupId;
    }

    public void setAgeGroupId(long ageGroupId) {
        this.ageGroupId = ageGroupId;
    }

    public String getAgeGroupCode() {
        return ageGroupCode;
    }

    public void setAgeGroupCode(String ageGroupCode) {
        this.ageGroupCode = ageGroupCode;
    }

    public String getAgeGroupDesc() {
        return ageGroupDesc;
    }

    public void setAgeGroupDesc(String ageGroupDesc) {
        this.ageGroupDesc = ageGroupDesc;
    }

    public long getAgeGroupKey() {
        return ageGroupKey;
    }

    public void setAgeGroupKey(long ageGroupKey) {
        this.ageGroupKey = ageGroupKey;
    }

    public long getMinAge() {
        return minAge;
    }

    public void setMinAge(long minAge) {
        this.minAge = minAge;
    }

    public long getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(long maxAge) {
        this.maxAge = maxAge;
    }
}
