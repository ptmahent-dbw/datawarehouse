package com.ptmahent.beans.DIM;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Customer {
    private long custKey;
    private String custFirstName;
    private String custLastName;
    private String custAddress;
    private Gender gender;
    private City city;
    private Province prov;
    private long custId;
    private String emailAddress;
    private Date dob;
    private String phoneNumber;


    public Customer() {
    }

    public Customer(long custKey, String custFirstName, String custLastName, String custAddress, City city, Province prov, long custId) {
        this.custKey = custKey;
        this.custFirstName = custFirstName;
        this.custLastName = custLastName;
        this.custAddress = custAddress;
        this.city = city;
        this.prov = prov;
        this.custId = custId;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }


    public long getCustKey() {
        return custKey;
    }

    public void setCustKey(long custKey) {
        this.custKey = custKey;
    }

    public String getCustFirstName() {
        return custFirstName;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    public String getCustLastName() {
        return custLastName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Province getProv() {
        return prov;
    }

    public void setProv(Province prov) {
        this.prov = prov;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
