package com.ptmahent.beans.DIM;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class SalesPerson {
    private long salesPersonKey;
    private long salesPersonId;
    private String firstName;
    private String lastName;
    private Gender gender;
    private String emailAddress;
    private Province prov;
    private City city;
    private Date dob;
    private String phoneNumber;
    private String address;



    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Province getProv() {
        return prov;
    }

    public void setProv(Province prov) {
        this.prov = prov;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getSalesPersonKey() {
        return salesPersonKey;
    }

    public void setSalesPersonKey(long salesPersonKey) {
        this.salesPersonKey = salesPersonKey;
    }

    public long getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(long salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
