package com.ptmahent.beans.DIM;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Gender {
    private long genderKey;
    private String gender;

    public long getGenderKey() {
        return genderKey;
    }

    public void setGenderKey(long genderKey) {
        this.genderKey = genderKey;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
