package com.ptmahent.beans.FACT;

import com.ptmahent.beans.OLTP.*;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Sales {
    private long salesId;
    private Date salesDate;
    private double totalSalesBeforeTax;
    private double totalDiscount;
    private double totalTaxAmount;
    private double totalAmountCharged;
    private String paymentMethod;

    private Customer customer;
    private String customerFirstName;
    private String customerLastName;
    private String customerGender;
    private Date customerDOB;
    private String customerAddress;
    private String customerPhoneNumber;
    private String customerEmailAddress;

    private City customerCity;
    private String customerCityName;

    private Province customerProvince;
    private String customerProvinceName;

    private SalesPerson salesPerson;
    private String salesPersonFirstName;
    private String salesPersonLastName;

    private Store store;
    private String storeName;
    private String storeId;
    private String storeAddress;
    private String storeCityName;
    private String storeProvinceName;

    private String fiscalQuarter;

    // CUST_ID
    // SALES_PERSON_ID
    // CUST_KEY
    // STORE_KEY
    // SALES_PERSON_KEY
    // INSTANCE_DATE_KEY
    // CITY_KEY
    // PROV_KEY
    // FISCAL_PERIOD_KEY

    private String customerAwareByAdCompaign;

    public long getSalesId() {
        return salesId;
    }

    public void setSalesId(long salesId) {
        this.salesId = salesId;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public double getTotalSalesBeforeTax() {
        return totalSalesBeforeTax;
    }

    public void setTotalSalesBeforeTax(double totalSalesBeforeTax) {
        this.totalSalesBeforeTax = totalSalesBeforeTax;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public double getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(double totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

    public double getTotalAmountCharged() {
        return totalAmountCharged;
    }

    public void setTotalAmountCharged(double totalAmountCharged) {
        this.totalAmountCharged = totalAmountCharged;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCustomerFirstName() {
        return customerFirstName;
    }

    public void setCustomerFirstName(String customerFirstName) {
        this.customerFirstName = customerFirstName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }

    public Date getCustomerDOB() {
        return customerDOB;
    }

    public void setCustomerDOB(Date customerDOB) {
        this.customerDOB = customerDOB;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(String customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public City getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(City customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerCityName() {
        return customerCityName;
    }

    public void setCustomerCityName(String customerCityName) {
        this.customerCityName = customerCityName;
    }

    public Province getCustomerProvince() {
        return customerProvince;
    }

    public void setCustomerProvince(Province customerProvince) {
        this.customerProvince = customerProvince;
    }

    public String getCustomerProvinceName() {
        return customerProvinceName;
    }

    public void setCustomerProvinceName(String customerProvinceName) {
        this.customerProvinceName = customerProvinceName;
    }

    public SalesPerson getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(SalesPerson salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getSalesPersonFirstName() {
        return salesPersonFirstName;
    }

    public void setSalesPersonFirstName(String salesPersonFirstName) {
        this.salesPersonFirstName = salesPersonFirstName;
    }

    public String getSalesPersonLastName() {
        return salesPersonLastName;
    }

    public void setSalesPersonLastName(String salesPersonLastName) {
        this.salesPersonLastName = salesPersonLastName;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreCityName() {
        return storeCityName;
    }

    public void setStoreCityName(String storeCityName) {
        this.storeCityName = storeCityName;
    }

    public String getStoreProvinceName() {
        return storeProvinceName;
    }

    public void setStoreProvinceName(String storeProvinceName) {
        this.storeProvinceName = storeProvinceName;
    }

    public String getFiscalQuarter() {
        return fiscalQuarter;
    }

    public void setFiscalQuarter(String fiscalQuarter) {
        this.fiscalQuarter = fiscalQuarter;
    }

    public String getCustomerAwareByAdCompaign() {
        return customerAwareByAdCompaign;
    }

    public void setCustomerAwareByAdCompaign(String customerAwareByAdCompaign) {
        this.customerAwareByAdCompaign = customerAwareByAdCompaign;
    }
}
