package com.ptmahent.beans.FACT;

import com.ptmahent.beans.OLTP.City;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class Demographics {

    private String cityName;
    private String provinceName;
    private long year;
    private long population;
    private City city;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getPopulation() {
        return population;
    }

    public void setPopulation(long population) {
        this.population = population;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
