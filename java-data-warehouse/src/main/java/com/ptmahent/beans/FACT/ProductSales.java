package com.ptmahent.beans.FACT;

import com.ptmahent.beans.OLTP.*;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class ProductSales {

    private long factSalesLineOrderId;
    private Date salesDate;


    private Product product;
    private String productName;
    private double productCost;
    private double productSellingPrice;

    private ProductCategory productCategory;
    private String producCategoryName;

    private ProductAgeGroup productAgeGroup;
    private String productAgeGroupCode;
    private String productAgeGroupDesc;

    private long productMinAge;
    private long producMaxAge;

    private Store store;
    private String storeName;
    private String storeAddress;
    private String storeCity;
    private String storeProvince;
    private String fiscalPeriod;

    // INSTANCE_DATE_KEY
    // STORE_KEY
    // PROD_KEY
    // PROD_AG_KEY
    // FISCAL_PERIOD_KEY

    public long getFactSalesLineOrderId() {
        return factSalesLineOrderId;
    }

    public void setFactSalesLineOrderId(long factSalesLineOrderId) {
        this.factSalesLineOrderId = factSalesLineOrderId;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getProductCost() {
        return productCost;
    }

    public void setProductCost(double productCost) {
        this.productCost = productCost;
    }

    public double getProductSellingPrice() {
        return productSellingPrice;
    }

    public void setProductSellingPrice(double productSellingPrice) {
        this.productSellingPrice = productSellingPrice;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public String getProducCategoryName() {
        return producCategoryName;
    }

    public void setProducCategoryName(String producCategoryName) {
        this.producCategoryName = producCategoryName;
    }

    public ProductAgeGroup getProductAgeGroup() {
        return productAgeGroup;
    }

    public void setProductAgeGroup(ProductAgeGroup productAgeGroup) {
        this.productAgeGroup = productAgeGroup;
    }

    public String getProductAgeGroupCode() {
        return productAgeGroupCode;
    }

    public void setProductAgeGroupCode(String productAgeGroupCode) {
        this.productAgeGroupCode = productAgeGroupCode;
    }

    public String getProductAgeGroupDesc() {
        return productAgeGroupDesc;
    }

    public void setProductAgeGroupDesc(String productAgeGroupDesc) {
        this.productAgeGroupDesc = productAgeGroupDesc;
    }

    public long getProductMinAge() {
        return productMinAge;
    }

    public void setProductMinAge(long productMinAge) {
        this.productMinAge = productMinAge;
    }

    public long getProducMaxAge() {
        return producMaxAge;
    }

    public void setProducMaxAge(long producMaxAge) {
        this.producMaxAge = producMaxAge;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreCity() {
        return storeCity;
    }

    public void setStoreCity(String storeCity) {
        this.storeCity = storeCity;
    }

    public String getStoreProvince() {
        return storeProvince;
    }

    public void setStoreProvince(String storeProvince) {
        this.storeProvince = storeProvince;
    }

    public String getFiscalPeriod() {
        return fiscalPeriod;
    }

    public void setFiscalPeriod(String fiscalPeriod) {
        this.fiscalPeriod = fiscalPeriod;
    }
}
