package com.ptmahent.beans.FACT;

import java.util.Date;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */
public class AverageLifeSpan {
    private long factAverageLifeSpanId;
    private String averageLifeSpan;
    private Date refStartDate;
    private Date refEndDate;
    private String geo;
    private String gender;
    private String ageGroup;
    private String lifeSpan;

    public long getFactAverageLifeSpanId() {
        return factAverageLifeSpanId;
    }

    public void setFactAverageLifeSpanId(long factAverageLifeSpanId) {
        this.factAverageLifeSpanId = factAverageLifeSpanId;
    }

    public String getAverageLifeSpan() {
        return averageLifeSpan;
    }

    public void setAverageLifeSpan(String averageLifeSpan) {
        this.averageLifeSpan = averageLifeSpan;
    }

    public Date getRefStartDate() {
        return refStartDate;
    }

    public void setRefStartDate(Date refStartDate) {
        this.refStartDate = refStartDate;
    }

    public Date getRefEndDate() {
        return refEndDate;
    }

    public void setRefEndDate(Date refEndDate) {
        this.refEndDate = refEndDate;
    }

    public String getGeo() {
        return geo;
    }

    public void setGeo(String geo) {
        this.geo = geo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAgeGroup() {
        return ageGroup;
    }

    public void setAgeGroup(String ageGroup) {
        this.ageGroup = ageGroup;
    }

    public String getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }
}
