package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.Product;
import com.ptmahent.beans.OLTP.SalesOrder;
import com.ptmahent.beans.OLTP.SalesOrderLine;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-08.
 */
public class CSVtoDbSalesOrderLine {

    final static Logger logger = Logger.getLogger(CSVtoDbSalesOrder.class);
    private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
    private static final String RE_A_Z_A_Z_S = "/[a-zA-Z\\s,/]";


    public static void insFromCSVFileSalesOrderLine(Connection con, List<CSVRecord> csvRecords) throws SQLException {


        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_SALES_ORDER_LINE);

        PreparedStatement pstmtSearchForSalesOrderLine = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_SALES_ORDER_LINE);

        PreparedStatement pstmtSearchForSalesOrder = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_SALES_ORDER_BY_INVOICE_ID);

        PreparedStatement pstmtSearchProdID = con.prepareStatement(CSVtoDBSQL.SQL_SELECT_PROD_ID_BY_PROD_NAME);

        PreparedStatement pstmtSUMSalesOrderLineAttrs = con.prepareStatement(CSVtoDBSQL.SQL_SUM_SALES_ORDER_LINE_ATTRS);

        PreparedStatement pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs = con.prepareStatement(CSVtoDBSQL.SQL_UPDATE_SALES_ORDER_WITH_SUM_OF_SALES_ORDER_LINE_ATTRS);
        // SALES ORDER LINE
        //put(CSV_FILE_SALES_ORDER_LINE,
        //        new String[] {"INVOICE ID",  "PROD NAME", "PRICE", "DISCOUNT", "ITEM COUNT","LINE_TOTAL"});

        //"INSERT INTO PTMAHENT.OLTP_SALES_ORDER_LINE " +
        //        " (SALES_ORDER_LINE_ID, SALES_ORDER_ID, PROD_NAME, PROD_ID, PRICE, DISCOUNT, LINE_TOTAL) " +
        //        " VALUES(PTMAHENT.OLTP_SALES_ORDER_LINE_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?)";
        // Search Sales Order

        for (int i = 1; i < csvRecords.size(); i++) {

            CSVRecord record = csvRecords.get(i);
            con.setAutoCommit(false);

            SalesOrderLine salesOrderLine = new SalesOrderLine();
            salesOrderLine.setSalesOrder(new SalesOrder());
            salesOrderLine.setProduct(new Product());

            try {
                for (int pos = 0; pos < record.size(); pos++) {
                    //        new String[] {"INVOICE ID", "PROD NAME", "PRICE", "DISCOUNT", "PROD COUNT", "LINE_TOTAL"});
                    switch (pos) {
                        case 0: // invoice id
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.getSalesOrder().setInvoiceId(Long.parseLong(record.get(pos).trim()));
                            }
                            break;

                        case 1: // prod name
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.getProduct().setProductName(record.get(pos).trim());
                            }

                            break;
                        case 2: // PRICE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.setPrice(Double.parseDouble(record.get(pos).trim()));
                            }
                            break;
                        case 3: // DISCOUNT
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.setDiscount(Double.parseDouble(record.get(pos).trim()));
                            }

                            break;
                        case 4: // PROD COUNT
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.setProdCount(Long.parseLong(record.get(pos).trim()));
                            }


                            break;
                        case 5: // Line Total
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER_LINE)[pos])) {
                                salesOrderLine.setLineTotal(Double.parseDouble(record.get(pos).trim()));
                            }

                            break;
                    }

                }

                // search for SaleOrder by invoice id

                pstmtSearchForSalesOrder.clearParameters();
                pstmtSearchForSalesOrder.setLong(1, salesOrderLine.getSalesOrder().getInvoiceId());
                ResultSet rs = pstmtSearchForSalesOrder.executeQuery();
                salesOrderLine.getSalesOrder().setSalesOrderId(-1);
                if (rs.next()) {
                    salesOrderLine.getSalesOrder().setSalesOrderId(rs.getLong("SALES_ORDER_ID"));
                }
                rs.close();


                // search for product id
                salesOrderLine.getProduct().setProductId(-1);
                searchForProdId(pstmtSearchProdID, salesOrderLine);

                if (salesOrderLine != null && salesOrderLine.getSalesOrder() != null &&
                       salesOrderLine.getProduct() != null ) {
                    logger.info(salesOrderLine);
                    // search for sale order line
                    // "SELECT SALES_ORDER_LINE_ID FROM PTMAHENT.OLTP_SALES_ORDER_LINE WHERE " +
                    //        " SALES_ORDER_ID = ? AND PROD_NAME = ? AND PRICE = ?
                    // AND PROD_COUNT = ? AND DISCOUNT = ? ";
                    pstmtSearchForSalesOrderLine.clearParameters();
                    pstmtSearchForSalesOrderLine.setLong(1, salesOrderLine.getSalesOrder().getSalesOrderId());
                    pstmtSearchForSalesOrderLine.setString(2, salesOrderLine.getProduct().getProductName());
                    pstmtSearchForSalesOrderLine.setDouble(3, salesOrderLine.getPrice());
                    pstmtSearchForSalesOrderLine.setLong(4, salesOrderLine.getProdCount());
                    pstmtSearchForSalesOrderLine.setDouble(5, salesOrderLine.getDiscount());
                    rs = pstmtSearchForSalesOrderLine.executeQuery();

                    if (!rs.next()) {
                        // no sales order line previously inserted that could be duplicate

                   /* " (SALES_ORDER_LINE_ID, SALES_ORDER_ID, PROD_NAME, PROD_ID, PRICE, " +
                            "DISCOUNT, PROD_COUNT, LINE_TOTAL) " +
                            " VALUES(PTMAHENT.OLTP_SALES_ORDER_LINE_SEQ.NEXTVAL, ?, ?, ?, ?, ?,?, ?)";
                    */
                        pstmtInsert.clearParameters();
                        pstmtInsert.setLong(1, salesOrderLine.getSalesOrder().getSalesOrderId());
                        pstmtInsert.setString(2, salesOrderLine.getProduct().getProductName());
                        pstmtInsert.setLong(3, salesOrderLine.getProduct().getProductId());
                        pstmtInsert.setDouble(4, salesOrderLine.getPrice());
                        pstmtInsert.setDouble(5, salesOrderLine.getDiscount());
                        pstmtInsert.setLong(6, salesOrderLine.getProdCount());
                        pstmtInsert.setDouble(7, salesOrderLine.getLineTotal());
                        pstmtInsert.executeUpdate();
                        con.commit();

                    }
                    rs.close();
                    // update statement with summary into sales order table
                    pstmtSUMSalesOrderLineAttrs.clearParameters();
                    pstmtSUMSalesOrderLineAttrs.setLong(1, salesOrderLine.getSalesOrder().getSalesOrderId());
                    rs = pstmtSUMSalesOrderLineAttrs.executeQuery();
                    if (rs.next()) {
                        System.out.println("SUM TOTAL DISCOUNT: " + rs.getString("D"));
                        System.out.println("SUM_TOTAL_LINE : " + rs.getString("LD"));


                        double sumTotalDiscount = Double.parseDouble(rs.getString("D"));
                        double sumTotalLine = Double.parseDouble(rs.getString("LD"));

                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.clearParameters();
                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.setDouble(1,sumTotalLine);
                        double taxedAmount = sumTotalLine * .15;
                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.setDouble(2, sumTotalDiscount);
                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.setDouble(3, taxedAmount);

                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.setDouble(4, sumTotalLine + taxedAmount);

                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.setLong(5, salesOrderLine.getSalesOrder().getSalesOrderId());
                        pstmtUpdateSalesOrdrWithSalesOrdrLineAttrs.executeUpdate();

                    }
                    rs.close();

                    con.commit();
                } else {
                    logger.error("ROLLBACK OCCURED");
                    con.rollback();
                }
                // insert sale order line

                // acculumlate total


            }catch (SQLException ex) {
                con.rollback();
                logger.error(ex);
            }
        }


    }


    private static void searchForProdId(PreparedStatement pstmtSearchProdID, SalesOrderLine salesOrderLine) throws SQLException {
        // retrieve prod id
        pstmtSearchProdID.clearParameters();
        pstmtSearchProdID.setString(1,salesOrderLine.getProduct().getProductName());

        ResultSet rs = pstmtSearchProdID.executeQuery();

        salesOrderLine.getProduct().setProductId(-1);
        if (rs.next()) {
            salesOrderLine.getProduct().setProductId(rs.getLong("PROD_ID"));
        }
        rs.close();
    }

}
