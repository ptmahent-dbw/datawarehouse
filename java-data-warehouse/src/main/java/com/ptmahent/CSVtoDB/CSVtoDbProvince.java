package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.Province;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbProvince {
    final static Logger logger = Logger.getLogger(CSVtoDbProvince.class);

    public static void insFromCSVFileProvince(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //  Statement sqlStatement = con.createStatement();
        //  sqlStatement.execute("TRUNCATE TABLE OLTP_PROVINCE");

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_PROVINCE);
        PreparedStatement pstmtSearchForProv = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_PROVINCE_ID);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            Province prov = new Province();

            con.setAutoCommit(false);
            try {
                //Create a new student object and fill his data
                // DB -> (CITY_NAME) VALUES(?)
                // CSV ->  new String[] { "CITY_NAME"})
                for (int pos = 0; pos < record.size(); pos++) {
                    logger.info(pos + " - " + record.get(pos) + ",");
                    switch (pos) {
                        case 0: // CATEGORY_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PROVINCE)[pos])) {
                                prov.setProvinceName(record.get(pos).trim());
                            }
                            break;

                    }

                }

                if (prov != null && prov.getProvinceName() != null && !prov.getProvinceName().isEmpty()) {
                    //System.out.println(prov);
                    logger.info(prov);
                    searchForProvince(pstmtSearchForProv, prov);

                    insertProvince(pstmtInsert, prov);
                    con.commit();
                } else {
                    con.rollback();
                }
            }catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }
        }

        pstmtSearchForProv.close();
        pstmtInsert.close();
        con.setAutoCommit(true);
        System.out.println();

    }

    private static void insertProvince(PreparedStatement pstmtInsert, Province prov) throws SQLException {
        if (prov.getProvinceId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,prov.getProvinceName());
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForProvince(PreparedStatement pstmtSearchForProv, Province prov) throws SQLException {
        pstmtSearchForProv.clearParameters();
        pstmtSearchForProv.setString(1, prov.getProvinceName());
        ResultSet rs = pstmtSearchForProv.executeQuery();
        prov.setProvinceId(-1);
        if (rs.next()) {
            prov.setProvinceId(rs.getLong("PROV_ID"));
        }
        rs.close();
    }


}
