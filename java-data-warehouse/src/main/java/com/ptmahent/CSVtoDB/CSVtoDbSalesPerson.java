package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.City;
import com.ptmahent.beans.OLTP.Province;
import com.ptmahent.beans.OLTP.SalesPerson;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbSalesPerson {
    // CSV_FILE_CUSTOMERS

    final static Logger logger = Logger.getLogger(CSVtoDbSalesPerson.class);

    public static void insFromCSVFileSalesPerson(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //   Statement sqlStatement = con.createStatement();
        //   sqlStatement.execute("TRUNCATE TABLE OLTP_SALES_PERSON");

        PreparedStatement pstmtInsert = con.
                prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_SALES_PERSON);

        PreparedStatement pstmtSearchForSalesPersonId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_SALES_PERSON_ID);
        PreparedStatement pstmtSearchCity = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CITY_ID);

        PreparedStatement pstmtInsertCity = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_CITY);

        PreparedStatement pstmtInsertProv = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_OLTP_PROVINCE_PROV_NAME_VALUES);
        PreparedStatement pstmtSearchProv = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_PROVINCE_ID);


        //PreparedStatement pstmtInsertCity = con.prepareStatement
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);

            SalesPerson salesPerson = new SalesPerson();
            salesPerson.setCity(new City());
            salesPerson.setProvince(new Province());
            // CSV ->new String[] { "FIRST_NAME", "LAST_NAME", "GENDER", "DOB", "ADDRESS", "PHONE_NUMBER", "EMAIL_ADDRESS", "CITY", "PROVINCE"});
            // DB -> "(FIRST_NAME, LAST_NAME, GENDER, DOB, ADDRESS, PHONE_NUMBER, EMAIL_ADDR, CITY_ID, PROV_ID) " +
            //" VALUES(?,?,?,?,?,?,?,?,?)"
            //Create a new student object and fill his data

            pstmtInsert.clearParameters();
            pstmtSearchCity.clearParameters();
            pstmtInsertCity.clearParameters();
            pstmtInsertProv.clearParameters();
            pstmtSearchProv.clearParameters();

            con.setAutoCommit(false);
            try {

                for (int pos = 0; pos < record.size(); pos++) {
                    System.out.print(pos + " - " + record.get(pos) + ",");
                    switch (pos) {
                        case 0: // FIRST_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setFirstName(record.get(pos).trim());
                            }
                            break;

                        case 1: // LAST_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setLastName(record.get(pos).trim());
                            }
                            break;
                        case 2: // GENDER
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setGender("" + record.get(pos).trim().charAt(0));
                            }
                            break;
                        case 3: // DOB
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
                                try {
                                    // Parse date into dateformat and then convert it to Date(SQL)
                                    salesPerson.setDob(df.parse(record.get(pos).trim()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case 4: // ADDRESS
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setAddress(record.get(pos).trim());
                            }
                            break;
                        case 5: // PHONE_NUMBER
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setPhoneNumber(record.get(pos).trim());
                            }
                            break;
                        case 6: // EMAIL_ADDRESS
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.setEmailAddress(record.get(pos).trim());
                            }
                            break;
                        case 7: // CITY
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {

                                salesPerson.getCity().setCityName(record.get(pos).trim());
                                searchForCityId(pstmtSearchCity, salesPerson);


                                insertCityAndReturnId(pstmtSearchCity, pstmtInsertCity, salesPerson);

                            }
                            break;
                        case 8: // PROVINCE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_PERSON)[pos])) {
                                salesPerson.getProvince().setProvinceName(record.get(pos).trim());

                                searchForProvId(pstmtSearchProv, salesPerson);

                                insertProvAndRetrieveId(pstmtInsertProv, pstmtSearchProv, salesPerson);

                            }
                            break;
                    }

                }

                if (salesPerson != null && salesPerson.getFirstName() != null && !salesPerson.getFirstName().isEmpty() &&
                        salesPerson.getLastName() != null && !salesPerson.getLastName().isEmpty()) {
                    //System.out.println(salesPerson);
                    logger.info(salesPerson);
                    searchForSalesPersonId(pstmtSearchForSalesPersonId, salesPerson);

                    insertSalesPerson(pstmtInsert, salesPerson);
                    con.commit();
                } else {
                    con.rollback();
                }

            } catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }

        }

        //pstmtInsert.executeBatch();

        pstmtInsert.close();
        pstmtInsertCity.close();
        pstmtInsertProv.close();
        pstmtSearchCity.close();
        pstmtSearchProv.close();
        pstmtSearchForSalesPersonId.close();
        System.out.println();
        con.setAutoCommit(true);
    }

    private static void insertSalesPerson(PreparedStatement pstmtInsert, SalesPerson salesPerson) throws SQLException {
        if (salesPerson.getSalesPersonId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,salesPerson.getFirstName());
            pstmtInsert.setString(2,salesPerson.getLastName());
            pstmtInsert.setString(3,salesPerson.getGender());
            if (salesPerson.getDob() != null) {
                pstmtInsert.setDate(4, new Date(salesPerson.getDob().getTime()));
            } else {
                pstmtInsert.setDate(4, null);
            }
            pstmtInsert.setString(5, salesPerson.getAddress());
            pstmtInsert.setString(6, salesPerson.getPhoneNumber());
            pstmtInsert.setString(7, salesPerson.getEmailAddress());
            if (salesPerson.getCity() != null) {
                pstmtInsert.setLong(8, salesPerson.getCity().getCityId());
            } else {
                pstmtInsert.setLong(8, 0);
            }
            if (salesPerson.getProvince() != null) {
                pstmtInsert.setLong(9, salesPerson.getProvince().getProvinceId());
            } else {
                pstmtInsert.setLong(9, 0);
            }
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForSalesPersonId(PreparedStatement pstmtSearchForSalesPersonId, SalesPerson salesPerson) throws SQLException {
        pstmtSearchForSalesPersonId.clearParameters();
        pstmtSearchForSalesPersonId.setString(1, salesPerson.getFirstName());
        pstmtSearchForSalesPersonId.setString(2, salesPerson.getLastName());
        pstmtSearchForSalesPersonId.setString(3, salesPerson.getGender());
        pstmtSearchForSalesPersonId.setString(4, salesPerson.getEmailAddress());
        ResultSet rs = pstmtSearchForSalesPersonId.executeQuery();

        salesPerson.setSalesPersonId(-1);
        if (rs.next()) {
            salesPerson.setSalesPersonId(rs.getLong("SALES_PERSON_ID"));
        }
        rs.close();
    }

    private static void insertProvAndRetrieveId(PreparedStatement pstmtInsertProv, PreparedStatement pstmtSearchProv, SalesPerson salesPerson) throws SQLException {
        if (salesPerson.getProvince().getProvinceId() == -1) {
            // if cityId is not found then insert city
            pstmtInsertProv.clearParameters();
            pstmtInsertProv.setString(1, salesPerson.getProvince().getProvinceName());
            pstmtInsertProv.executeUpdate();

            searchForProvId(pstmtSearchProv, salesPerson);
        }
    }

    private static void searchForProvId(PreparedStatement pstmtSearchProv, SalesPerson salesPerson) throws SQLException {
        pstmtSearchProv.clearParameters();
        pstmtSearchProv.setString(1, salesPerson.getProvince().getProvinceName());
        salesPerson.getProvince().setProvinceId(-1);
        ResultSet rs = pstmtSearchProv.executeQuery();
        if (rs.next()) {
            salesPerson.getProvince().setProvinceId(rs.getLong("PROV_ID"));
        }
        rs.close();
    }

    private static void insertCityAndReturnId(PreparedStatement pstmtSearchCity, PreparedStatement pstmtInsertCity, SalesPerson salesPerson) throws SQLException {
        if (salesPerson.getCity().getCityId() == -1) {
            // if cityId is not found then insert city
            pstmtInsertCity.clearParameters();
            pstmtInsertCity.setString(1, salesPerson.getCity().getCityName());
            pstmtInsertCity.executeUpdate();

            searchForCityId(pstmtSearchCity, salesPerson);
        }
    }

    private static void searchForCityId(PreparedStatement pstmtSearchCity, SalesPerson salesPerson) throws SQLException {
        pstmtSearchCity.clearParameters();
        pstmtSearchCity.setString(1, salesPerson.getCity().getCityName());
        salesPerson.getCity().setCityId(-1);
        ResultSet rs = pstmtSearchCity.executeQuery();
        if (rs.next()) {
            salesPerson.getCity().setCityId(rs.getLong("CITY_ID"));
        }
        rs.close();
    }


}
