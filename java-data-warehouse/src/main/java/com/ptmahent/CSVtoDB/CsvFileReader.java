package com.ptmahent.CSVtoDB;

/**
 * Created by Piratheep Mahent on 2016-12-06.
 */


import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;


public class CsvFileReader {


    public static final String CSV_FILE_PRODUCTS = "data\\products.csv";
    public static final String CSV_FILE_STORES = "data\\stores.csv";
    public static final String CSV_FILE_SALES_PERSON = "data\\sales_person.csv";
    public static final String CSV_FILE_PROVINCE = "data\\provinces.csv";
    public static final String CSV_FILE_CUSTOMERS = "data\\customers.csv";
    public static final String CSV_FILE_CITIES = "data\\cities.csv";
    public static final String CSV_FILE_AGE_GROUP = "data\\age_group.csv";
    public static final String CSV_FILE_CATEGORIES = "data\\categories.csv";
    public static final String CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN = "data\\averageExpectedLifeSpan.csv";
    public static final String CSV_FILE_DEMOGRAPHICS = "data\\demographicsENG.csv";
    public static final String CSV_FILE_BABY_NAMES_MALE = "data\\topMaleBabyEnglishNames.csv";
    public static final String CSV_FILE_BABY_NAMES_FEMALE = "data\\topFemaleBabyEnglishNames.csv";
    public static final String CSV_FILE_SALES_ORDER = "data\\sales_order.csv";
    public static final String CSV_FILE_SALES_ORDER_LINE = "data\\sales_order_line.csv";


    private static final String FEMALE = "F";
    private static final String MALE = "M";

    //CSV file header
    public static Map<String, String[]> fileHeaderMappings = new HashMap<String, String[]>(){
        {
            put(CSV_FILE_PRODUCTS,
                    new String [] {"PRODUCT_CATEGORY","PRODUCT_AGE_GROUP","PRODUCT_NAME","PRODUCT_COST","PRODUCT_SELLING_PRICE"});

            put(CSV_FILE_BABY_NAMES_FEMALE,
                    new String [] {"Year", "Name", "Frequency"});
            put(CSV_FILE_BABY_NAMES_MALE,
                    new String[] {"Year", "Name", "Frequency"});


            // STORES
            put (CSV_FILE_STORES,
                    new String[] {"STORE_NAME","CITY_NAME", "PROVINCE", "ADDRESS"});

            // SALES_PERSON
            put(CSV_FILE_SALES_PERSON,
                    new String[] {"FIRST_NAME","LAST_NAME","GENDER", "DOB", "ADDRESS", "PHONE_NUMBER", "EMAIL_ADDRESS", "CITY", "PROVINCE"});

            // PROVINCE
            put(CSV_FILE_PROVINCE,
                    new String[] { "PROVINCE_NAME"});

            // PRODUCT
            put(CSV_FILE_PRODUCTS,
                    new String[] {"PRODUCT_CATEGORY", "PRODUCT_AGE_GROUP", "PRODUCT_NAME", "PRODUCT_COST", "PRODUCT_SELLING_PRICE","ITEMS_IN_STOCK","ITEMS_ORDER_FROM_OUR_VENDOR","SHELF","BIN"});
            // CUSTOMERS
            put(CSV_FILE_CUSTOMERS,
                    new String[] { "FIRST_NAME", "LAST_NAME", "GENDER", "DOB", "ADDRESS", "PHONE_NUMBER", "EMAIL_ADDRESS", "CITY", "PROVINCE"});
            // CITIES
            put(CSV_FILE_CITIES,
                    new String[] { "CITY_NAME"});
            // AGE GROUP
            put(CSV_FILE_AGE_GROUP,
                    new String[] { "AGE_GROUP_CODE", "MIN_AGE", "MAX_AGE"});
            // CATEGORIES
            put(CSV_FILE_CATEGORIES,
                    new String[] { "CATEGORY_NAME"});

            // AverageExpectedLifeSpan
            put(CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN,
                    new String[] { "Ref_Date", "GEO", "SEX", "AGE", "Vector", "Coordinates", "Value"});
            // Demographics

            put (CSV_FILE_DEMOGRAPHICS,
                    new String[] {"CITY", "2012","2013","2014","2015"});

            // BABY NAMES - MALE
            put (CSV_FILE_BABY_NAMES_MALE,
                    new String[] {"Year","Name","Frequency"});

            // BABY NAMES - FEMALE
            put (CSV_FILE_BABY_NAMES_FEMALE,
                    new String[] {"Year","Name","Frequency"});

            // SALES ORDER
            put(CSV_FILE_SALES_ORDER,
                    new String[] {"INVOICE ID", "STORE", "SALES_PERSON", "SALES_DATE", "TOTAL_SALES_BEFORE_TAX", "TOTAL_DISCOUNT", "TOTAL_TAX_AMOUNT", "TOTAL_AMOUNT_CHARGED", "CUSTOMER NAME", "CUSTOMER EMAIL", "CUSTOMER AWARE BY", "PAYMENT METHOD"});

            // SALES ORDER LINE
            put(CSV_FILE_SALES_ORDER_LINE,
                    new String[] {"INVOICE ID", "PROD NAME", "PRICE", "DISCOUNT", "PROD COUNT", "LINE_TOTAL"});

        }
    };


    public static void readCsvFile(String fileName) {
        readCsvFile(fileName, null);
    }

    public static void readCsvFile(String fileName, Connection con) {

        FileReader fileReader = null;

        CSVParser csvFileParser = null;

        //Create the CSVFormat object with the header mapping
        CSVFormat csvFileFormat = CSVFormat.DEFAULT;

        String correctFileName = ".\\src\\main\\resources\\" + fileName;

        try {
            // C:\Users\theepanm\OneDrive\Seneca\fall-2016\dbw-624-s1a-MON\project\java-data-warehouse\src\main\java\com\ptmahent\CreateTestData.java
            // C:\Users\theepanm\OneDrive\Seneca\fall-2016\dbw-624-s1a-MON\project\java-data-warehouse\src\main\resources\data
            //Create a new list of student to be filled by CSV file data
            List students = new ArrayList();

            //initialize FileReader object
            fileReader = new FileReader( correctFileName);

            //initialize CSVParser object
            csvFileParser = new CSVParser(fileReader, csvFileFormat);

            //Get a list of CSV file records
            List<CSVRecord> csvRecords = csvFileParser.getRecords();

            //Read the CSV file records starting from the second record to skip the header
            /*for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = csvRecords.get(i);
                //Create a new student object and fill his data
                //System.out.println(record);
            }*/

            System.out.println(fileName);
            switch (fileName) {
                case CSV_FILE_AGE_GROUP:
                    CSVtoDbAgeGroup.insFromCSVIntoAgeGroup(con, csvRecords);


                    break;
                case CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN:
                    CSVtoDbAverageExpectedLifeSpan.insFromCSVIntoAverageExpectedLifeSpan(con, csvRecords);
                    break;

                case CSV_FILE_BABY_NAMES_FEMALE:
                    CSVtoDbTopBabyNames.insFromCSVIntoBabyNames(con, csvRecords, FEMALE);
                    break;

                case CSV_FILE_BABY_NAMES_MALE:
                    CSVtoDbTopBabyNames.insFromCSVIntoBabyNames(con, csvRecords, MALE);

                    break;

                case CSV_FILE_CATEGORIES:
                    CSVtoDbCategories.insFromCSVFileCategories(con, csvRecords);
                    break;

                case CSV_FILE_CITIES:
                    CSVtoDbCities.insFromCSVFileCities(con, csvRecords);

                    break;

                case CSV_FILE_CUSTOMERS:
                    CSVtoDbCustomers.insFromCSVFileCustomers(con, csvRecords);
                    break;

                case CSV_FILE_DEMOGRAPHICS:
                    CSVtoDbDemograpics.insFromCSVIntoDemoGrapics(con, csvRecords);
                    break;


                case CSV_FILE_PRODUCTS:
                    CSVtoDbProducts.insFromCSVIntoProducts(con, csvRecords);
                    break;

                case CSV_FILE_PROVINCE:
                    CSVtoDbProvince.insFromCSVFileProvince(con, csvRecords);

                    break;
                case CSV_FILE_SALES_PERSON:
                    CSVtoDbSalesPerson.insFromCSVFileSalesPerson(con, csvRecords);

                    break;

                case CSV_FILE_STORES:
                    CSVtoDbStores.insFromCSVFileStores(con, csvRecords);

                    break;
                case CSV_FILE_SALES_ORDER:
                    CSVtoDbSalesOrder.insFromCSVFileSalesOrder(con, csvRecords);
                    break;
                case CSV_FILE_SALES_ORDER_LINE:
                    CSVtoDbSalesOrderLine.insFromCSVFileSalesOrderLine(con, csvRecords);
                    break;
            }

        }
        catch (Exception e) {
            System.out.println("Error in CsvFileReader !!!");
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
                csvFileParser.close();
            } catch (IOException e) {
                System.out.println("Error while closing fileReader/csvFileParser !!!");
                e.printStackTrace();
            }
        }

    }



}