package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.ProductCategory;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbCategories {

    final static Logger logger = Logger.getLogger(CSVtoDbCategories.class);

    public static void insFromCSVFileCategories(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //  Statement sqlStatement = con.createStatement();
        //  sqlStatement.execute(TRUNCATE_TABLE_OLTP_PRODUCT_CATEGORY);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_OLTP_PRODUCT_CATEGORY);
        PreparedStatement pstmtSearchForCategory = con.prepareStatement(CSVtoDBSQL.SQL_SELECT_PROD_CAT_ID_FROM_PTMAHENT_OLTP_PRODUCT_CATEGORY_WHERE_PROD_CAT_NAME);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            ProductCategory prodCat = new ProductCategory();
            //Create a new student object and fill his data
            // CSV -> new String[] { "CATEGORY_NAME"})
            // DB -> (PROD_CAT_NAME) VALUES(?)

            con.setAutoCommit(false);
            try {

                for (int pos = 0; pos < record.size(); pos++) {
                    //System.out.print(pos + " - " + record.get(pos) + ",");
                    switch (pos) {
                        case 0: // CATEGORY_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CATEGORIES)[pos])) {

                                prodCat.setProductCategoryName(record.get(pos).trim());
                            }
                            break;

                    }

                }

                if (prodCat != null && prodCat.getProductCategoryName() != null && !prodCat.getProductCategoryName().isEmpty()) {
                    //System.out.println(prodCat);
                    logger.info(prodCat);
                    searchForCategory(pstmtSearchForCategory, prodCat);

                    insertCategory(pstmtInsert, prodCat);
                    con.commit();

                } else {
                    logger.error("ROLLBACK OCCURED " + prodCat);
                    con.rollback();
                }

            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                logger.error(ex);
                con.rollback();
            }
        }
        //pstmtInsert.executeBatch();
        pstmtInsert.close();
        pstmtSearchForCategory.close();
        System.out.println();
        con.setAutoCommit(true);
    }

    private static void insertCategory(PreparedStatement pstmtInsert, ProductCategory prodCat) throws SQLException {
        if (prodCat.getProductCategoryId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,prodCat.getProductCategoryName());
            pstmtInsert.executeQuery();
        }
    }

    private static void searchForCategory(PreparedStatement pstmtSearchForCategory, ProductCategory prodCat) throws SQLException {
        pstmtSearchForCategory.clearParameters();
        pstmtSearchForCategory.setString(1, prodCat.getProductCategoryName());
        ResultSet rs = pstmtSearchForCategory.executeQuery();
        prodCat.setProductCategoryId(-1);
        if (rs.next()) {
            prodCat.setProductCategoryId(rs.getLong("PROD_CAT_ID"));
        }
        rs.close();
    }

}
