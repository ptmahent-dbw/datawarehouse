package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.City;
import com.ptmahent.beans.OLTP.Province;
import com.ptmahent.beans.OLTP.Store;
import com.ptmahent.beans.OLTP.StoreLocation;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbStores {
    final static Logger logger = Logger.getLogger(CSVtoDbStores.class);

    public static void insFromCSVFileStores(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        // Statement sqlStatement = con.createStatement();
        // sqlStatement.execute("TRUNCATE TABLE OLTP_STORE");

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_STORE);
        PreparedStatement pstmtSearchForStore = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_STORE);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            //Create a new student object and fill his data
            // DB -> (CITY_NAME) VALUES(?)
            // CSV ->  new String[] { "CITY_NAME"})
            con.setAutoCommit(false);
            Store store = new Store();
            StoreLocation storeLocation = new StoreLocation();

            try {

                for (int pos = 0; pos < record.size(); pos++) {

                    logger.info(pos + " - " + record.get(pos) + ",");
                    switch (pos) {
                        case 0: // STORE NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_STORES)[pos])) {

                                store.setStoreName(record.get(pos).trim());
                            }
                            break;
                        case 1: // STORE CITY
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_STORES)[pos])) {

                                storeLocation.setCity(new City(record.get(pos).trim()));

                                searchCity(con, storeLocation);

                                if (storeLocation.getCity().getCityId() == -1) {
                                    insertCity(con, storeLocation);
                                    searchCity(con, storeLocation);
                                }
                            }
                            break;

                        case 2: // Store Prov
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_STORES)[pos])) {

                                storeLocation.setProvince(new Province(record.get(pos).trim()));

                                searchProv(con, storeLocation);

                                if (storeLocation.getCity().getCityId() == -1) {
                                    insertProv(con, storeLocation);
                                    searchProv(con, storeLocation);
                                }
                            }


                            break;

                        case 3: // Store Address
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_STORES)[pos])) {

                                storeLocation.setAddress(record.get(pos).trim());
                            }
                            break;

                    }

                }
                if (store != null && store.getStoreName() != null && !store.getStoreName().isEmpty()) {

                    //System.out.println(store);
                    logger.info(store);
                    searchForStore(pstmtSearchForStore, store);
                    if (store.getStoreId() == -1) {
                        insertStore(pstmtInsert, store, storeLocation);
                        searchForStore(pstmtSearchForStore, store);
                    } else {

                        updateStore(con, store, storeLocation);
                    }

                    con.commit();


                } else {
                    logger.error("ROLLBACK - " + store);
                    con.rollback();
                }
            } catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }
        }

        //pstmtInsert.executeBatch();

        pstmtInsert.close();
        pstmtSearchForStore.close();
        con.setAutoCommit(true);
        System.out.println();
    }

    private static void updateStore(Connection con, Store store, StoreLocation storeLocation) throws SQLException {
        PreparedStatement pstmtUpdateStore = con.prepareStatement("UPDATE PTMAHENT.OLTP_STORE " +
                " SET STORE_NAME = ?, PROV_ID = ?, CITY_ID = ?, STORE_ADDRESS = ? " +
                " WHERE STORE_ID = ?");
        pstmtUpdateStore.setString(1, store.getStoreName());
        pstmtUpdateStore.setLong(2, storeLocation.getProvince().getProvinceId());
        pstmtUpdateStore.setLong(3, storeLocation.getCity().getCityId());
        pstmtUpdateStore.setString(4, storeLocation.getAddress());
        pstmtUpdateStore.setLong(5, store.getStoreId());
        pstmtUpdateStore.executeUpdate();
        pstmtUpdateStore.close();
    }

    private static void insertProv(Connection con, StoreLocation storeLocation) throws SQLException {
        PreparedStatement pstmtInsertProv = con.prepareStatement("INSERT INTO PTMAHENT.OLTP_PROVINCE (PROV_ID, PROV_NAME) " +
                "VALUES(PTMAHENT.OLTP_PROV_SEQ.NEXTVAL, ?)");

        pstmtInsertProv.setString(1, storeLocation.getProvince().getProvinceName());
        pstmtInsertProv.executeUpdate();
    }

    private static void searchProv(Connection con, StoreLocation storeLocation) throws SQLException {
        PreparedStatement pstmSearchProv = con.prepareStatement("SELECT PROV_ID FROM PTMAHENT.OLTP_PROVINCE WHERE PROV_NAME = ?");
        pstmSearchProv.setString(1, storeLocation.getProvince().getProvinceName());
        ResultSet rsSearchProv = pstmSearchProv.executeQuery();

        storeLocation.getProvince().setProvinceId(-1);
        if (rsSearchProv.next()) {
            storeLocation.getProvince().setProvinceId(rsSearchProv.getLong("PROV_ID"));
        }
        rsSearchProv.close();
        pstmSearchProv.close();
    }

    private static void insertCity(Connection con, StoreLocation storeLocation) throws SQLException {
        PreparedStatement pstmtInsertCity = con.prepareStatement("INSERT INTO PTMAHENT.OLTP_CITY (CITY_ID, CITY_NAME) " +
                "VALUES(PTMAHENT.OLTP_CITY_SEQ.NEXTVAL, ?)");

        pstmtInsertCity.setString(1, storeLocation.getCity().getCityName());
        pstmtInsertCity.executeUpdate();
    }

    private static void searchCity(Connection con, StoreLocation storeLocation) throws SQLException {
        PreparedStatement pstmSearchCity = con.prepareStatement("SELECT CITY_ID FROM PTMAHENT.OLTP_CITY WHERE CITY_NAME = ?");
        pstmSearchCity.setString(1, storeLocation.getCity().getCityName());
        ResultSet rsSearchCity = pstmSearchCity.executeQuery();

        storeLocation.getCity().setCityId(-1);
        if (rsSearchCity.next()) {
            storeLocation.getCity().setCityId(rsSearchCity.getLong("CITY_ID"));
        }
        rsSearchCity.close();
        pstmSearchCity.close();
    }

    private static void insertStore(PreparedStatement pstmtInsert, Store store, StoreLocation storeLocation) throws SQLException {

            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,store.getStoreName());
            pstmtInsert.setLong(2, storeLocation.getProvince().getProvinceId());
            pstmtInsert.setLong(3, storeLocation.getCity().getCityId());
            pstmtInsert.setString(4, storeLocation.getAddress());
            pstmtInsert.executeUpdate();
            pstmtInsert.getConnection().commit();

    }

    private static void searchForStore(PreparedStatement pstmtSearchForStore, Store store) throws SQLException {
        pstmtSearchForStore.clearParameters();
        pstmtSearchForStore.setString(1,store.getStoreName());
        ResultSet rs = pstmtSearchForStore.executeQuery();
        store.setStoreId(-1);

        if (rs.next()) {
            store.setStoreId(rs.getLong("STORE_ID"));
        }
        rs.close();
    }


}
