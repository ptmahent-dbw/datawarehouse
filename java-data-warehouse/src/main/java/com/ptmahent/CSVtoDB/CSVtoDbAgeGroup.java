package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.ProductAgeGroup;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbAgeGroup {

    final static Logger logger = Logger.getLogger(CSVtoDbAgeGroup.class);
    public static void insFromCSVIntoAgeGroup(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //Statement sqlStatement = con.createStatement();
        // sqlStatement.execute(SQL_TRUNCATE_TABLE_OLTP_PRODUCT_AGE_GROUP);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_AGE_GROUP_AG_CODE_MIN_AGE_MAX_AGE);
        PreparedStatement pstmtSearchForProdAgeGroup = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_AGE_GROUP_BY_PROD_AG_CODE);

        //Read the CSV file records starting from the second record to skip the header

        try {
            con.setAutoCommit(false);

            for (int i = 1; i < csvRecords.size(); i++) {
                CSVRecord record = csvRecords.get(i);
                pstmtInsert.clearParameters();
                ProductAgeGroup prodAgeGroup = new ProductAgeGroup();

                // CSV -> new String[] { "AGE_GROUP_CODE", "MIN_AGE", "MAX_AGE"})
                // DB -> (PROD_AG_CODE, PROD_MIN_AGE, PROD_MAX_AGE) VALUES(?,?,?)
                //Create a new student object and fill his data
                for (int pos = 0; pos < record.size(); pos++) {
                    logger.info(pos + " - " + record.get(pos) + ",");
                    switch(pos) {
                        case 0: // AG_CODE

                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AGE_GROUP)[pos])) {
                                prodAgeGroup.setProductAgeGroupCode(record.get(pos).trim());
                            }
                            break;
                        case 1: // MIN AGE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AGE_GROUP)[pos])) {
                                prodAgeGroup.setProductMinAge(Long.parseLong(record.get(pos).trim()));
                            }
                            break;
                        case 2: // MAX_AGE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AGE_GROUP)[pos])) {
                                prodAgeGroup.setProductMaxAge(Long.parseLong(record.get(pos).trim()));
                            }
                            break;
                    }

                }
                if (prodAgeGroup != null && prodAgeGroup.getProductAgeGroupCode() != null && !prodAgeGroup.getProductAgeGroupCode().isEmpty()) {


                        //System.out.println(prodAgeGroup);
                        logger.info(prodAgeGroup);
                        searchForProdAgeGroup(pstmtSearchForProdAgeGroup, prodAgeGroup);

                        insertProdAgeGroup(pstmtInsert, prodAgeGroup);
                        con.commit();

                } else {
                    logger.error("ROLL BACK occured - " + prodAgeGroup);
                    con.rollback();
                }


            }
        } catch (SQLException ex) {
            //System.out.println(ex.getMessage());
            logger.error(ex);
            con.rollback();


        }
        //pstmtInsert.executeBatch();

        pstmtInsert.close();
        pstmtSearchForProdAgeGroup.close();
        pstmtInsert.close();

        con.setAutoCommit(true);
        System.out.println();
    }

    private static void insertProdAgeGroup(PreparedStatement pstmtInsert, ProductAgeGroup prodAgeGroup) throws SQLException {
        if (prodAgeGroup.getProductAgeGroupId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,prodAgeGroup.getProductAgeGroupCode());
            pstmtInsert.setLong(2,prodAgeGroup.getProductMinAge());
            pstmtInsert.setLong(3,prodAgeGroup.getProductMaxAge());
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForProdAgeGroup(PreparedStatement pstmtSearchForProdAgeGroup, ProductAgeGroup prodAgeGroup) throws SQLException {
        pstmtSearchForProdAgeGroup.clearParameters();
        pstmtSearchForProdAgeGroup.setString(1,prodAgeGroup.getProductAgeGroupCode());
        ResultSet rs = pstmtSearchForProdAgeGroup.executeQuery();

        prodAgeGroup.setProductAgeGroupId(-1);
        if (rs.next()) {
            prodAgeGroup.setProductAgeGroupId(rs.getLong("PROD_AG_ID"));
        }
        rs.close();
    }
}
