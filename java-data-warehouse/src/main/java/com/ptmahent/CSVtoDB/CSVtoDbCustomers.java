package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.City;
import com.ptmahent.beans.OLTP.Customer;
import com.ptmahent.beans.OLTP.Province;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbCustomers {

    final static Logger logger = Logger.getLogger(CSVtoDbCustomers.class);
    private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";

    // CSV_FILE_CUSTOMERS
    public static void insFromCSVFileCustomers(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //  Statement sqlStatement = con.createStatement();
        //  sqlStatement.execute(TRUNCATE_TABLE_OLTP_CUSTOMER);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_OLTP_CUSTOMER);

        PreparedStatement pstmtSearchCity = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CITY_ID);

        PreparedStatement pstmtInsertCity = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_CITY);

        PreparedStatement pstmtInsertProv = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_OLTP_PROVINCE_PROV_NAME_VALUES);
        PreparedStatement pstmtSearchProv = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_PROVINCE_ID);

        PreparedStatement pstmtSearchForCustomerId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CUSTOMER_ID);

        //PreparedStatement pstmtInsertCity = con.prepareStatement
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            // CSV ->new String[] { "FIRST_NAME", "LAST_NAME", "GENDER", "DOB", "ADDRESS", "PHONE_NUMBER", "EMAIL_ADDRESS", "CITY", "PROVINCE"});
            // DB -> "(FIRST_NAME, LAST_NAME, GENDER, DOB, ADDRESS, PHONE_NUMBER, EMAIL_ADDR, CITY_ID, PROV_ID) " +
            //" VALUES(?,?,?,?,?,?,?,?,?)"
            //Create a new student object and fill his data
            pstmtInsert.clearParameters();
            pstmtInsertCity.clearParameters();
            pstmtInsertProv.clearParameters();
            pstmtSearchCity.clearParameters();
            pstmtSearchProv.clearParameters();
            Customer customer = new Customer();

            con.setAutoCommit(false);

            try {
                for (int pos = 0; pos < record.size(); pos++) {

                    logger.info(pos + " - " + record.get(pos) + ",");
                    switch (pos) {
                        case 0: // FIRST_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setFirstName(record.get(pos).trim());
                            }
                            break;

                        case 1: // LAST_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setLastName(record.get(pos).trim());
                            }
                            break;
                        case 2: // GENDER
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setGender("" + record.get(pos).trim().charAt(0));
                            }
                            break;
                        case 3: // DOB
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                DateFormat df = new SimpleDateFormat(DATE_FORMAT_MM_DD_YYYY);
                                try {
                                    // Parse date into dateformat and then convert it to Date(SQL)
                                    customer.setDob(df.parse(record.get(pos).trim()));
                                    logger.info("DATE: " + customer.getDob());
                                    logger.info("DATE IN TIME: " + customer.getDob().getTime());
                                    logger.info("DATE IN SQL: " + new Date(customer.getDob().getTime()));
                                } catch (ParseException e) {
                                    logger.fatal(e);
                                    e.printStackTrace();
                                }
                            }
                            break;
                        case 4: // ADDRESS
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setAddress(record.get(pos).trim());
                            }
                            break;
                        case 5: // PHONE_NUMBER
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setPhoneNumber(record.get(pos).trim());
                            }
                            break;
                        case 6: // EMAIL_ADDRESS
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setEmailAddr(record.get(pos).trim());
                            }
                            break;
                        case 7: // CITY
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {

                                customer.setCity(new City(record.get(pos).trim()));

                                searchForCityId(pstmtSearchCity, customer);


                                insertCityAndReturnId(pstmtSearchCity, pstmtInsertCity, customer);

                            }
                            break;
                        case 8: // PROVINCE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CUSTOMERS)[pos])) {
                                customer.setProv(new Province(record.get(pos).trim()));

                                searchForProvId(pstmtSearchProv, customer);

                                insertProvAndRetrieveId(pstmtInsertProv, pstmtSearchProv, customer);
                            }
                            break;
                    }

                }

                if (customer != null && customer.getFirstName() != null && !customer.getFirstName().isEmpty() &&
                        customer.getLastName() != null && !customer.getLastName().isEmpty()) {
                    //System.out.println(customer);
                    logger.info(customer);
                    searchForCustomerId(pstmtSearchForCustomerId, customer);

                    insertCustomer(pstmtInsert, customer);
                    con.commit();

                } else {
                    logger.error("ROLLBACK OCCURED - " + customer);
                    con.rollback();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                logger.error(ex);
                con.rollback();
            }

            //pstmtInsert.executeUpdate();
            // pstmtInsert.executeBatch();

        }

        pstmtInsert.close();
        pstmtInsertCity.close();
        pstmtInsertProv.close();
        pstmtSearchCity.close();
        pstmtSearchForCustomerId.close();
        pstmtSearchProv.close();
        con.setAutoCommit(true);

        System.out.println();
    }

    private static void insertCustomer(PreparedStatement pstmtInsert, Customer customer) throws SQLException {
        if (customer.getCustomerId() == -1) {
            System.out.println("INSERTING INTO : " + customer);
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1,customer.getFirstName());
            pstmtInsert.setString(2,customer.getLastName());
            pstmtInsert.setString(3,customer.getGender());
            if (customer.getDob() != null) {
                pstmtInsert.setDate(4, new java.sql.Date(customer.getDob().getTime()));
                logger.info("INSERT DATE IN SQL: " + new java.sql.Date(customer.getDob().getTime()));
            } else {
                pstmtInsert.setDate(4, null);
            }
            pstmtInsert.setString(5, customer.getAddress());
            pstmtInsert.setString(6, customer.getPhoneNumber());
            pstmtInsert.setString(7, customer.getEmailAddr());
            if (customer.getCity() != null) {
                pstmtInsert.setLong(8, customer.getCity().getCityId());
            } else {
                pstmtInsert.setLong(8, 0);
            }
            if (customer.getProv() != null) {
                pstmtInsert.setLong(9, customer.getProv().getProvinceId());
            } else {
                pstmtInsert.setLong(9, 0);
            }
            logger.info(CSVtoDBSQL.SQL_INSERT_OLTP_CUSTOMER);
            logger.info(pstmtInsert.toString());
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForCustomerId(PreparedStatement pstmtSearchForCustomerId, Customer customer) throws SQLException {
        pstmtSearchForCustomerId.clearParameters();
        pstmtSearchForCustomerId.setString(1, customer.getFirstName());
        pstmtSearchForCustomerId.setString(2, customer.getLastName());
        pstmtSearchForCustomerId.setString(3, customer.getGender());
        pstmtSearchForCustomerId.setString(4, customer.getEmailAddr());
        ResultSet rs = pstmtSearchForCustomerId.executeQuery();

        customer.setCustomerId(-1);
        if (rs.next()) {
            customer.setCustomerId(rs.getLong("CUST_ID"));
        }
        rs.close();
    }


    private static void insertProvAndRetrieveId(PreparedStatement pstmtInsertProv, PreparedStatement pstmtSearchProv, Customer customer) throws SQLException {
        if (customer.getProv().getProvinceId() == -1) {
            // if cityId is not found then insert city
            pstmtInsertProv.getConnection().setAutoCommit(false);
            pstmtInsertProv.clearParameters();
            pstmtInsertProv.setString(1, customer.getProv().getProvinceName());
            pstmtInsertProv.executeUpdate();
            pstmtInsertProv.getConnection().commit();

            searchForProvId(pstmtSearchProv, customer);
        }
    }

    private static void searchForProvId(PreparedStatement pstmtSearchProv, Customer customer) throws SQLException {
        pstmtSearchProv.clearParameters();
        pstmtSearchProv.setString(1, customer.getProv().getProvinceName());
        customer.getProv().setProvinceId(-1);
        ResultSet rs = pstmtSearchProv.executeQuery();
        if (rs.next()) {
            customer.getProv().setProvinceId(rs.getLong("PROV_ID"));
        }
        rs.close();
    }
    private static void searchForCityId(PreparedStatement pstmtSearchCity, Customer customer) throws SQLException {
        pstmtSearchCity.clearParameters();
        pstmtSearchCity.setString(1, customer.getCity().getCityName());
        customer.getCity().setCityId(-1);
        ResultSet rs = pstmtSearchCity.executeQuery();
        if (rs.next()) {
            customer.getCity().setCityId(rs.getLong("CITY_ID"));
        }
        rs.close();
    }

    private static void insertCityAndReturnId(PreparedStatement pstmtSearchCity, PreparedStatement pstmtInsertCity, Customer customer) throws SQLException {
        if (customer.getCity().getCityId() == -1) {
            // if cityId is not found then insert city
            logger.info("INSERTED CITY: " + customer.getCity());
            pstmtInsertCity.getConnection().setAutoCommit(false);
            pstmtInsertCity.clearParameters();
            pstmtInsertCity.setString(1, customer.getCity().getCityName());
            pstmtInsertCity.executeUpdate();
            pstmtInsertCity.getConnection().commit();

            searchForCityId(pstmtSearchCity, customer);
            logger.info("AFTER INSERTING: " + customer.getCity());
        }
    }

}
