package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.Customer;
import com.ptmahent.beans.OLTP.SalesOrder;
import com.ptmahent.beans.OLTP.SalesPerson;
import com.ptmahent.beans.OLTP.Store;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-08.
 */
public class CSVtoDbSalesOrder {


    final static Logger logger = Logger.getLogger(CSVtoDbSalesOrder.class);
    private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
    private static final String RE_A_Z_A_Z_S = "/[a-zA-Z\\s,/]";

    public static void insFromCSVFileSalesOrder(Connection con, List<CSVRecord> csvRecords) throws SQLException{

        // Insert Sales Order
        // SALES ORDER
        //put(CSV_FILE_SALES_ORDER,
         //       new String[] {"INVOICE ID", "STORE", "SALES_PERSON", "SALES_DATE",
        // "TOTAL_SALES_BEFORE_TAX", "TOTAL_DISCOUNT", "TOTAL_TAX_AMOUNT", "TOTAL_AMOUNT_CHARGED",
        // "CUSTOMER NAME", "CUSTOMER EMAIL", "CUSTOMER AWARE BY", "PAYMENT METHOD"});

        //"INSERT INTO PTMAHENT.OLTP_SALES_ORDER " +
        //        " (SALES_ORDER_ID, STORE_ID, SALES_PERSON_ID, SALES_DATE, TOTAL_SALES_BEFORE_TAX, TOTAL_DISCOUNT, "
        //        + " TOTAL_TAX_AMOUNT, TOTAL_AMOUNT_CHARGED, PAYMENT_METHOD, CUSTOMER_ID, CUSTOMER_AWARE_BY_AD, INVOICE_ID) "
         //       + " VALUES(PTMAHENT.OLTP_SALES_ORDER_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        // SALES ORDER LINE
        //put(CSV_FILE_SALES_ORDER_LINE,
        //        new String[] {"INVOICE ID", "PROD NAME", "PRICE", "DISCOUNT", "LINE_TOTAL"});

        //"INSERT INTO PTMAHENT.OLTP_SALES_ORDER_LINE " +
        //        " (SALES_ORDER_LINE_ID, SALES_ORDER_ID, PROD_NAME, PROD_ID, PRICE, DISCOUNT, LINE_TOTAL) " +
        //        " VALUES(PTMAHENT.OLTP_SALES_ORDER_LINE_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?)";
        // Search Sales Order

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_SALES_ORDER);

        PreparedStatement pstmtSearchForSalesOrder = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_SALES_ORDER);

        PreparedStatement pstmtInsertSalesOrderLine = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_SALES_ORDER_LINE);

        PreparedStatement pstmtSearchForSalesOrderLine = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_SALES_ORDER_LINE);

        PreparedStatement pstmtSearchForCustomerId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CUSTOMER_ID_NO_GENDER);

        PreparedStatement pstmtInsertStore = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_STORE_WITH_ONLY_NAME);
        PreparedStatement pstmtSearchForStore = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_STORE);

        PreparedStatement pstmtSearchForSalesPersonId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_SALES_PERSON_ID_FOR_SALES_ORDER);

        // get storeId

        // get SalesPersonId

        // get CustomerId

        //"SELECT CUST_ID FROM PTMAHENT.OLTP_CUSTOMER WHERE " +
         //       " FIRST_NAME = ? AND LAST_NAME = ? AND EMAIL_ADDR = ?";



        for (int i = 1; i < csvRecords.size(); i++) {

            CSVRecord record = csvRecords.get(i);
            con.setAutoCommit(false);

            SalesOrder salesOrder = new SalesOrder();
            try {
                for (int pos = 0; pos < record.size(); pos++) {
                    //       new String[] {"INVOICE ID", "STORE", "SALES_PERSON", "SALES_DATE",
                    // "TOTAL_SALES_BEFORE_TAX", "TOTAL_DISCOUNT", "TOTAL_TAX_AMOUNT",
                    // "TOTAL_AMOUNT_CHARGED",
                    // "CUSTOMER NAME", "CUSTOMER EMAIL", "CUSTOMER AWARE BY", "PAYMENT METHOD"});
                    logger.info(record);
                    System.out.println(record);
                    switch (pos) {
                        case 0: // INVOICE ID
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                salesOrder.setInvoiceId(Long.parseLong(record.get(pos).trim()));
                            }
                                break;
                        case 1: // STORE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                salesOrder.setStore(new Store(record.get(pos).trim()));
                            }
                                break;
                        case 2: // SALES PERSON
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                String[] salePersonNames = record.get(pos).trim().split(" ");
                                salesOrder.setSalesPerson(new SalesPerson(salePersonNames[0], salePersonNames[1]));
                            }
                            break;
                        case 3: // SALES DATE
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                DateFormat df = new SimpleDateFormat(DATE_FORMAT_MM_DD_YYYY);
                                try {
                                    // Parse date into dateformat and then convert it to Date(SQL)
                                    salesOrder.setSalesDate(df.parse(record.get(pos).trim()));
                                    logger.info("DATE: " + salesOrder.getSalesDate());
                                    logger.info("DATE IN TIME: " + salesOrder.getSalesDate().getTime());
                                    logger.info("DATE IN SQL: " + new Date(salesOrder.getSalesDate().getTime()));
                                } catch (ParseException e) {
                                    logger.fatal(e);
                                    e.printStackTrace();
                                }

                            }

                            break;
                        case 4: // TOTAL_SALES_BEFORE_TAX
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setTotalSalesBeforeTax(Double.parseDouble(record.get(pos).trim().replaceAll(RE_A_Z_A_Z_S,"")));
                            }

                            break;
                        case 5: // TOTAL_DISCOUNT
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setTotalDiscount(Double.parseDouble(record.get(pos).trim().replaceAll(RE_A_Z_A_Z_S,"")));
                            }

                            break;
                        case 6: // TOTAL_TAX_AMOUNT
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setTotalTaxAmount(Double.parseDouble(record.get(pos).trim().replaceAll(RE_A_Z_A_Z_S,"")));
                            }

                            break;
                        case 7: // TOTAL_AMOUNT_CHARGED
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setTotalAmountCharged(Double.parseDouble(record.get(pos).trim().replaceAll(RE_A_Z_A_Z_S,"")));
                            }


                            break;
                        case 8: // CUSTOMER NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                String[] customerNames = record.get(pos).trim().split(" ");
                                salesOrder.setCustomer(new Customer(customerNames[0], customerNames[1]));
                            }
                            break;

                        case 9: // CUSTOMER EMAIL
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {
                                if (salesOrder.getCustomer() != null) {
                                    salesOrder.getCustomer().setEmailAddr(record.get(pos).trim());
                                } else {
                                    Customer customer = new Customer();
                                    customer.setEmailAddr(record.get(pos).trim());
                                    salesOrder.setCustomer(customer);
                                }
                            }
                            break;

                        case 10: // CUSTOMER AWARE BY
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setCustomerAwareByAdCompaign(record.get(pos).trim());
                            }

                            break;
                        case 11: // PAYMENT METHOD
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_SALES_ORDER)[pos])) {

                                salesOrder.setPaymentMethod(record.get(pos).trim());
                            }
                            break;

                    }
                }


                // search for store id first time
                searchForStore(pstmtSearchForStore, salesOrder);
                if (salesOrder.getStore().getStoreId() == -1) {
                    // insert new store, if not found
                    insertStore(pstmtInsertStore, salesOrder.getStore());
                    // retrieve store id again
                    searchForStore(pstmtSearchForStore, salesOrder);
                }
                salesOrder.getSalesPerson().setSalesPersonId(-1);
                // get SalesPersonId
                searchForSalesPersonId(pstmtSearchForSalesPersonId, salesOrder);

                if (salesOrder.getSalesPerson().getSalesPersonId() == -1) {


                }
                salesOrder.getCustomer().setCustomerId(-1);

                // get CustomerId
                searchForCustomerId(pstmtSearchForCustomerId, salesOrder);

                if (salesOrder != null && salesOrder.getCustomer() != null &&
                        salesOrder.getStore() != null && salesOrder.getSalesPerson() != null &&
                        salesOrder.getCustomer().getCustomerId() != -1 &&
                        salesOrder.getSalesPerson().getSalesPersonId() != -1 &&
                        salesOrder.getStore().getStoreId() != -1) {
                    // Search for Sales order
                    pstmtSearchForSalesOrder.clearParameters();
                    pstmtSearchForSalesOrder.setLong(1, salesOrder.getStore().getStoreId());
                    pstmtSearchForSalesOrder.setLong(2, salesOrder.getSalesPerson().getSalesPersonId());
                    pstmtSearchForSalesOrder.setDate(3, new Date(salesOrder.getSalesDate().getTime()));
                    pstmtSearchForSalesOrder.setLong(4, salesOrder.getCustomer().getCustomerId());
                    pstmtSearchForSalesOrder.setString(5, salesOrder.getCustomerAwareByAdCompaign());
                    pstmtSearchForSalesOrder.setLong(6, salesOrder.getInvoiceId());
                    ResultSet rs = pstmtSearchForSalesOrder.executeQuery();

                    if (!rs.next()) {
                        // No Record found so insert new record
                        pstmtInsert.clearParameters();
                        pstmtInsert.setLong(1, salesOrder.getStore().getStoreId());
                        pstmtInsert.setLong(2, salesOrder.getSalesPerson().getSalesPersonId());
                        pstmtInsert.setDate(3, new Date(salesOrder.getSalesDate().getTime()));
                        pstmtInsert.setDouble(4, salesOrder.getTotalSalesBeforeTax());
                        pstmtInsert.setDouble(5, salesOrder.getTotalDiscount());
                        pstmtInsert.setDouble(6, salesOrder.getTotalTaxAmount());
                        pstmtInsert.setDouble(7, salesOrder.getTotalAmountCharged());
                        pstmtInsert.setString(8, salesOrder.getPaymentMethod());
                        pstmtInsert.setLong(9, salesOrder.getCustomer().getCustomerId());
                        pstmtInsert.setString(10, salesOrder.getCustomerAwareByAdCompaign());
                        pstmtInsert.setLong(11, salesOrder.getInvoiceId());
                        pstmtInsert.executeQuery();

                       /* " (SALES_ORDER_ID, STORE_ID, SALES_PERSON_ID,
                        SALES_DATE, TOTAL_SALES_BEFORE_TAX, TOTAL_DISCOUNT, "
                                + " TOTAL_TAX_AMOUNT, TOTAL_AMOUNT_CHARGED,
                                PAYMENT_METHOD, CUSTOMER_ID, CUSTOMER_AWARE_BY_AD, INVOICE_ID) "
                                + " VALUES(PTMAHENT.OLTP_SALES_ORDER_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                        */

                    }
                    //pstmtSearchForSalesOrder.set
                    //" STORE_ID = ? AND SALES_PERSON_ID = ? AND SALES_DATE = ? AND CUSTOMER_ID = ? AND CUSTOMER_AWARE_BY_AD = ? " +
                    //        " AND INVOICE_ID = ?";
                    // Insert into db
                    //
                    con.commit();
                } else {
                    logger.error("ROLLBACK OCCURED: " + salesOrder);
                    con.rollback();
                }

            } catch (SQLException ex) {
                logger.error(ex);
                con.rollback();
            }
        }

        pstmtInsert.close();
        pstmtInsertSalesOrderLine.close();
        pstmtInsertStore.close();
        pstmtSearchForCustomerId.close();
        pstmtSearchForSalesOrder.close();
        pstmtSearchForSalesOrderLine.close();
        pstmtSearchForSalesPersonId.close();
        con.setAutoCommit(true);



    }


    private static void searchForCustomerId(PreparedStatement pstmtSearchForCustomerId, SalesOrder salesOrder) throws SQLException {
        pstmtSearchForCustomerId.clearParameters();
        pstmtSearchForCustomerId.setString(1, salesOrder.getCustomer().getFirstName());
        pstmtSearchForCustomerId.setString(2, salesOrder.getCustomer().getLastName());
        pstmtSearchForCustomerId.setString(3, salesOrder.getCustomer().getEmailAddr());
        ResultSet rs = pstmtSearchForCustomerId.executeQuery();

        salesOrder.getCustomer().setCustomerId(-1);
        if (rs.next()) {
            salesOrder.getCustomer().setCustomerId(rs.getLong("CUST_ID"));
        }
        rs.close();
    }
    private static void searchForSalesPersonId(PreparedStatement pstmtSearchForSalesPersonId, SalesOrder salesOrder) throws SQLException {
        pstmtSearchForSalesPersonId.clearParameters();
        pstmtSearchForSalesPersonId.setString(1, salesOrder.getSalesPerson().getFirstName());
        pstmtSearchForSalesPersonId.setString(2, salesOrder.getSalesPerson().getLastName());
        ResultSet rs = pstmtSearchForSalesPersonId.executeQuery();

        salesOrder.getSalesPerson().setSalesPersonId(-1);
        if (rs.next()) {
            salesOrder.getSalesPerson().setSalesPersonId(rs.getLong("SALES_PERSON_ID"));
        }
        rs.close();
    }

    private static void searchForStore(PreparedStatement pstmtSearchForStore, SalesOrder salesOrder) throws SQLException {
        // get storeId
        pstmtSearchForStore.clearParameters();
        pstmtSearchForStore.setString(1, salesOrder.getStore().getStoreName());
        ResultSet rs = pstmtSearchForStore.executeQuery();

        salesOrder.getStore().setStoreId(-1);
        if (rs.next()) {
            salesOrder.getStore().setStoreId(rs.getLong("STORE_ID"));
        }
        rs.close();
    }

    private static void insertStore(PreparedStatement pstmtInsert, Store store) throws SQLException {
        if (store.getStoreId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.getConnection().setAutoCommit(false);
            pstmtInsert.setString(1, store.getStoreName());
            pstmtInsert.executeUpdate();
            pstmtInsert.getConnection().commit();
        }
    }

}
