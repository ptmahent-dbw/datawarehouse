package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.BabyFrequencyPerYear;
import com.ptmahent.beans.OLTP.TopBabyNames;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbTopBabyNames {
    final static Logger logger = Logger.getLogger(CSVtoDbTopBabyNames.class);

    // CSV_FILE_BABY_NAMES_FEMALE && CSV_FILE_BABY_NAMES_MALE
    public static void insFromCSVIntoBabyNames(Connection con, List<CSVRecord> csvRecords, String gender) throws SQLException {
        // truncate the existing data on the table
        String CSVTopBabyNameMode;
        if (gender.equalsIgnoreCase(CSVtoDBSQL.MALE)) {
            // male
            CSVTopBabyNameMode = CsvFileReader.CSV_FILE_BABY_NAMES_MALE;
        } else {
            // female
            CSVTopBabyNameMode = CsvFileReader.CSV_FILE_BABY_NAMES_FEMALE;
        }
        Statement sqlStatement = con.createStatement();
        // sqlStatement.execute(TRUNCATE_TABLE_OLTP_TOP_BABY_NAMES);
        // sqlStatement.execute(TRUNCATE_TABLE_OLTP_BABY_FREQUENCY_PER_YEAR);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_TOP_BABY_NAMES);

        PreparedStatement pstmtInsertFrequecy = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_BABY_FREQUENCY_PER_YEAR);

        PreparedStatement pstmtSearchForBabyId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_TOP_BABY_NAMES);

        PreparedStatement pstmtSearchFrequencyByBabyIdAndYear = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FREQUENCY_BY_BABY_NAME_ID_AND_YEAR);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);

            TopBabyNames topBabyNames = new TopBabyNames();
            BabyFrequencyPerYear babyFrequencyPerYear = new BabyFrequencyPerYear();
            //Create a new student object and fill his data
            // CSV -> new String[] {"Year","Name","Frequency"})
            // DB -> (BABY_NAME, GENDER) VALUES(?,?)
            // DB -> (BABY_NAME_ID, YEAR, FREQUENCY) VALUES(?, ?, ?)
            con.setAutoCommit(false);
            try {

                for (int pos = 0; pos < record.size(); pos++) {
                    //System.out.print(pos + " - " + record.get(pos) + ",");


                    switch (pos) {
                        case 0: // Year

                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CSVTopBabyNameMode)[pos])) {
                                //year = record.get(pos).trim();
                                babyFrequencyPerYear.setYear(Long.parseLong(record.get(pos).trim()));
                            } else {
                                babyFrequencyPerYear.setYear(0);
                            }

                            break;
                        case 1: // Name
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CSVTopBabyNameMode)[pos])) {
                                //babyName = record.get(pos).trim();
                                topBabyNames.setBabyName(record.get(pos).trim());
                            } else {
                                topBabyNames.setBabyName("");
                            }

                            break;

                        case 2: // Frequency
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CSVTopBabyNameMode)[pos])) {

                                babyFrequencyPerYear.setFrequency(Long.parseLong(record.get(pos).trim().replaceAll("[a-zA-Z\\s]+", "")));
                            } else {
                                babyFrequencyPerYear.setFrequency(0);
                            }
                            break;

                    }
                    if (topBabyNames != null && babyFrequencyPerYear != null &&
                            babyFrequencyPerYear.getYear() > 0 &&
                            topBabyNames.getBabyName() != null &&
                            !topBabyNames.getBabyName().isEmpty() &&
                            babyFrequencyPerYear.getFrequency() > 0) {

                        //System.out.println(topBabyNames);
                        //System.out.println(babyFrequencyPerYear);
                        logger.info(topBabyNames);
                        logger.info(babyFrequencyPerYear);
                        // Search for baby id, first time
                        searchForBabyId(gender, pstmtSearchForBabyId, topBabyNames);

                        insertTopBabyName(gender, pstmtInsert, topBabyNames);

                        // Search for baby id second time after inserting baby name
                        searchForBabyId(gender, pstmtSearchForBabyId, topBabyNames);

                        searchAndInsertFrequency(pstmtInsertFrequecy, pstmtSearchFrequencyByBabyIdAndYear, topBabyNames, babyFrequencyPerYear);
                        con.commit();

                    } else {
                        logger.error("ROLLBACK OCCURED - " + topBabyNames + babyFrequencyPerYear);
                        con.rollback();
                    }


                }
            }catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }


        }

        //pstmtInsertFrequecy.executeBatch();
        pstmtInsertFrequecy.close();
        pstmtInsert.close();
        pstmtSearchForBabyId.close();
        pstmtSearchFrequencyByBabyIdAndYear.close();
        con.setAutoCommit(true);

        //System.out.println();
    }

    private static void searchAndInsertFrequency(PreparedStatement pstmtInsertFrequecy, PreparedStatement pstmtSearchFrequencyByBabyIdAndYear, TopBabyNames topBabyNames, BabyFrequencyPerYear babyFrequencyPerYear) throws SQLException {
        pstmtSearchFrequencyByBabyIdAndYear.clearParameters();
        pstmtSearchFrequencyByBabyIdAndYear.setLong(1, topBabyNames.getBabyNameId());
        pstmtSearchFrequencyByBabyIdAndYear.setLong(2, babyFrequencyPerYear.getYear());
        ResultSet rs = pstmtSearchFrequencyByBabyIdAndYear.executeQuery();

        // if no record found, then insert otherwise ignore
        if (!rs.next()) {
            pstmtInsertFrequecy.clearParameters();
            pstmtInsertFrequecy.setLong(1, topBabyNames.getBabyNameId());
            pstmtInsertFrequecy.setLong(2,babyFrequencyPerYear.getYear());
            pstmtInsertFrequecy.setLong(3,babyFrequencyPerYear.getFrequency());
            pstmtInsertFrequecy.executeUpdate();
        }
        rs.close();
    }

    private static void insertTopBabyName(String gender, PreparedStatement pstmtInsert, TopBabyNames topBabyNames) throws SQLException {
        if (topBabyNames.getBabyNameId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1, topBabyNames.getBabyName());
            pstmtInsert.setString(2, gender);
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForBabyId(String gender, PreparedStatement pstmtSearchForBabyId, TopBabyNames topBabyNames) throws SQLException {
        pstmtSearchForBabyId.clearParameters();
        pstmtSearchForBabyId.setString(1,topBabyNames.getBabyName());
        pstmtSearchForBabyId.setString(2,gender);
        ResultSet rs = pstmtSearchForBabyId.executeQuery();
        topBabyNames.setBabyNameId(-1);
        if (rs.next()) {
            topBabyNames.setBabyNameId(rs.getLong("BABY_NAME_ID"));
        }
        rs.close();
    }
}
