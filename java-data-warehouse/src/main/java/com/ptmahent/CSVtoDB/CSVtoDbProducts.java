package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.Product;
import com.ptmahent.beans.OLTP.ProductAgeGroup;
import com.ptmahent.beans.OLTP.ProductCategory;
import com.ptmahent.beans.OLTP.ProductInventory;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbProducts {

    final static Logger logger = Logger.getLogger(CSVtoDbProducts.class);
    public static void insFromCSVIntoProducts(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //Statement sqlStatement = con.createStatement();
        //sqlStatement.execute("TRUNCATE TABLE OLTP_PRODUCT");

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_PRODUCT);


        PreparedStatement pstmtInsertProdInven = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_PRODUCT_INVENTORY);

        PreparedStatement pstmtSearchProdID = con.prepareStatement(CSVtoDBSQL.SQL_SELECT_PROD_ID_FROM_PTMAHENT_OLTP_PRODUCT_WHERE_PROD_NAME_AND_PROD_CAT_ID_AND_PROD_AG_ID);

        PreparedStatement pstmtInsertProdAG = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_AGE_GROUP_AG_CODE_MIN_AGE_MAX_AGE);
        PreparedStatement pstmtSearchForProdAgID = con.prepareStatement(CSVtoDBSQL.SQL_SELECT_PROD_AG_ID_FROM_PTMAHENT_OLTP_PRODUCT_AGE_GROUP_WHERE_PROD_AG_CODE);

        PreparedStatement pstmtInsertProdCat = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_OLTP_PRODUCT_CATEGORY);
        PreparedStatement pstmtSearchForProdCatId = con.prepareStatement(CSVtoDBSQL.SQL_SELECT_PROD_CAT_ID_FROM_PTMAHENT_OLTP_PRODUCT_CATEGORY_WHERE_PROD_CAT_NAME);
        //Read the CSV file records starting from the second record to skip the header
        PreparedStatement pstmtSearchProdInven = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_PRODUCT_INVENTORY);
        for (int i = 1; i < csvRecords.size(); i++) {
            pstmtInsert.clearParameters();
            pstmtInsertProdInven.clearParameters();
            pstmtSearchProdID.clearParameters();
            pstmtInsertProdAG.clearParameters();
            pstmtInsertProdCat.clearParameters();
            pstmtInsertProdInven.clearParameters();
            pstmtSearchForProdAgID.clearParameters();
            pstmtSearchForProdCatId.clearParameters();

            pstmtSearchProdInven.clearParameters();
            Product product = new Product();
            ProductInventory productInventory= new ProductInventory();
            CSVRecord record = csvRecords.get(i);
            // CSV -> new String[] {"PRODUCT_CATEGORY", "PRODUCT_AGE_GROUP", "PRODUCT_NAME", "PRODUCT_COST", "PRODUCT_SELLING_PRICE",
            // "ITEMS_IN_STOCK","ITEMS_ORDER_FROM_OUR_VENDOR","SHELF","BIN"});
            // DB -> INSERT INTO OLTP_PRODUCT (PROD_CAT_ID,PROD_AG_ID,PROD_NAME,PROD_COST,PROD_SELLING_PRICE) " +
            //" VALUES(?,?,?,?,?)
            // DB -> "INSERT INTO OLTP_PRODUCT_INVENTORY (PROD_ID,ITEMS_IN_STOCK,ITEMS_ORDER_FROM_VENDOR,SHELF,BIN) " +
            //" VALUES(?,?,?,?,?)"
            //Create a new student object and fill his data
            con.setAutoCommit(false);
            try {

                for (int pos = 0; pos < record.size(); pos++) {
                    //System.out.println(record.get(pos));
                    logger.info(record.get(pos));
                    String prodName = "";
                    long prodId = -1;
                    long prodCategoryId = -1;
                    long prodAGId = -1;
                    switch (pos) {
                        case 0: // PRODUCT CATEGORY -> 1
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                product.setProductCategory(new ProductCategory(record.get(pos).trim()));

                                searchForProdCatId(pstmtSearchForProdCatId, product);


                                insertProdCat(pstmtInsertProdCat, product);
                                // after inserting product category retrieve the category id
                                searchForProdCatId(pstmtSearchForProdCatId, product);
                            }
                            break;
                        case 1: // PRODUCT AGE GROUP
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {

                                product.setProductAgeGroup(new ProductAgeGroup(record.get(pos).trim()));

                                searchForProdAGId(pstmtSearchForProdAgID, product);

                                prodAGId = insertProdAGIdAndReturnId(pstmtInsertProdAG, pstmtSearchForProdAgID, product, prodAGId);

                            }
                            break;
                        case 2: // PRODUCT NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                product.setProductName(record.get(pos).trim());
                            }
                            break;
                        case 3: // PRODUCT COST
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                product.setProductCost(Double.parseDouble(record.get(pos).trim()));
                            }
                            break;
                        case 4: // PRODUCT SELLING PRIC
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                product.setProductSellingPrice(Double.parseDouble(record.get(pos).trim()));
                            }


                            break;
                        //"ITEMS_IN_STOCK","ITEMS_ORDER_FROM_OUR_VENDOR","SHELF","BIN"
                        case 5:
                            if (prodId != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                productInventory.setItemsInStock(Long.parseLong(record.get(pos).trim()));
                                //pstmtInsertProdInven.setLong(2,Long.parseLong(record.get(pos).trim()));

                            }
                            break;
                        case 6:
                            if (prodId != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                productInventory.setItemsOrderFromVendor(Long.parseLong(record.get(pos).trim()));
                                //pstmtInsertProdInven.setLong(3,Long.parseLong(record.get(pos).trim()));
                            }
                            break;
                        case 7:
                            if (prodId != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                productInventory.setShelf(Long.parseLong(record.get(pos).trim()));
                                //pstmtInsertProdInven.setLong(4,Long.parseLong(record.get(pos).trim()));
                            }
                            break;
                        case 8:
                            if (prodId != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_PRODUCTS)[pos])) {
                                productInventory.setBin(Long.parseLong(record.get(pos).trim()));
                                //pstmtInsertProdInven.setLong(5,Long.parseLong(record.get(pos).trim()));
                            }
                            break;
                    }

                }

                searchForProdId(pstmtSearchProdID, product);
                //System.out.println(product);
                logger.info(product);
                insertProd(pstmtInsert, pstmtSearchProdID, product);

                insertProdInventory(pstmtInsertProdInven, pstmtSearchProdInven, product, productInventory);

                con.commit();
                //System.out.println(record);
            } catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }
        }

        pstmtInsert.close();
        pstmtInsertProdAG.close();
        pstmtInsertProdCat.close();
        pstmtInsertProdInven.close();
        pstmtSearchForProdAgID.close();
        pstmtSearchForProdCatId.close();
        pstmtSearchProdID.close();
        con.setAutoCommit(true);
    }

    private static void insertProdInventory(PreparedStatement pstmtInsertProdInven, PreparedStatement pstmtSearchProdInven, Product product, ProductInventory productInventory) throws SQLException {
        ResultSet rs;
        if (product.getProductId() != -1) {
            productInventory.setProductId(product.getProductId());

            pstmtSearchProdInven.clearParameters();
            pstmtSearchProdInven.setLong(1, productInventory.getProductId());
            rs = pstmtSearchProdInven.executeQuery();

            if (!rs.next()) {
                // No records found
                pstmtInsertProdInven.setLong(1, productInventory.getProductId());
                pstmtInsertProdInven.setLong(2, productInventory.getItemsInStock());
                pstmtInsertProdInven.setLong(3, productInventory.getItemsOrderFromVendor());
                pstmtInsertProdInven.setLong(4, productInventory.getShelf());
                pstmtInsertProdInven.setLong(5, productInventory.getBin());
                pstmtInsertProdInven.executeUpdate();
            }
            rs.close();
        }
    }

    private static void insertProd(PreparedStatement pstmtInsert, PreparedStatement pstmtSearchProdID, Product product) throws SQLException {
        if (product.getProductId() == -1) {
            //System.out.println(product);

            pstmtInsert.clearParameters();
            pstmtInsert.setLong(1,product.getProductCategory().getProductCategoryId());
            pstmtInsert.setLong(2,product.getProductAgeGroup().getProductAgeGroupId());
            pstmtInsert.setString(3, product.getProductName());
            pstmtInsert.setDouble(4, product.getProductCost());
            pstmtInsert.setDouble(5, product.getProductSellingPrice());
            pstmtInsert.executeUpdate();

            searchForProdId(pstmtSearchProdID, product);
        }
    }

    private static void searchForProdId(PreparedStatement pstmtSearchProdID, Product product) throws SQLException {
        // retrieve prod id
        pstmtSearchProdID.clearParameters();
        pstmtSearchProdID.setString(1,product.getProductName());
        pstmtSearchProdID.setLong(2,product.getProductCategory().getProductCategoryId());
        pstmtSearchProdID.setLong(3,product.getProductAgeGroup().getProductAgeGroupId());

        ResultSet rs = pstmtSearchProdID.executeQuery();

        product.setProductId(-1);
        if (rs.next()) {
            product.setProductId(rs.getLong("PROD_ID"));
        }
        rs.close();
    }

    private static long insertProdAGIdAndReturnId(PreparedStatement pstmtInsertProdAG, PreparedStatement pstmtSearchForProdAgID, Product product, long prodAGId) throws SQLException {
        ResultSet rs;
        if (product.getProductAgeGroup().getProductAgeGroupId() == -1) {
            pstmtInsertProdAG.getConnection().setAutoCommit(false);
            pstmtInsertProdAG.clearParameters();

            pstmtInsertProdAG.setString(1,product.getProductAgeGroup().getProductAgeGroupCode());
            pstmtInsertProdAG.setLong(2,0);
            pstmtInsertProdAG.setLong(3,0);
            pstmtInsertProdAG.executeUpdate();
            pstmtInsertProdAG.getConnection().commit();
        }

        searchForProdAGId(pstmtSearchForProdAgID, product);

        return prodAGId;
    }

    private static void searchForProdAGId(PreparedStatement pstmtSearchForProdAgID, Product product) throws SQLException {
        pstmtSearchForProdAgID.clearParameters();
        pstmtSearchForProdAgID.setString(1,product.getProductAgeGroup().getProductAgeGroupCode());

        ResultSet rs = pstmtSearchForProdAgID.executeQuery();
        product.getProductAgeGroup().setProductAgeGroupId(-1);
        if (rs.next()) {
            product.getProductAgeGroup().setProductAgeGroupId(rs.getLong("PROD_AG_ID"));
        }
        rs.close();
    }

    private static void insertProdCat(PreparedStatement pstmtInsertProdCat, Product product) throws SQLException {
        if (product.getProductCategory().getProductCategoryId() == -1) {
            pstmtInsertProdCat.getConnection().setAutoCommit(false);
            pstmtInsertProdCat.clearParameters();
            pstmtInsertProdCat.setString(1,product.getProductCategory().getProductCategoryName());
            pstmtInsertProdCat.executeUpdate();
            pstmtInsertProdCat.getConnection().commit();

        }
    }

    private static void searchForProdCatId(PreparedStatement pstmtSearchForProdCatId, Product product) throws SQLException {
        pstmtSearchForProdCatId.clearParameters();
        pstmtSearchForProdCatId.setString(1,product.getProductCategory().getProductCategoryName());

        ResultSet rs = pstmtSearchForProdCatId.executeQuery();
        product.getProductCategory().setProductCategoryId(-1);
        if (rs.next()) {
            product.getProductCategory().setProductCategoryId(rs.getLong("PROD_CAT_ID"));
        }
        rs.close();
    }


}
