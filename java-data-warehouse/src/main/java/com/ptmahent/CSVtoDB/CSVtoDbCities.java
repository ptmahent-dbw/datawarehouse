package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.City;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbCities {
    final static Logger logger = Logger.getLogger(CSVtoDbCities.class);
    public static void insFromCSVFileCities(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //  Statement sqlStatement = con.createStatement();
        //  sqlStatement.execute(TRUNCATE_TABLE_OLTP_CITY);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_CITY);
        PreparedStatement pstmtSearchForCityId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CITY_ID);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            //Create a new student object and fill his data
            // DB -> (CITY_NAME) VALUES(?)
            // CSV ->  new String[] { "CITY_NAME"})
            City city = new City();
            con.setAutoCommit(false);
            try {

                for (int pos = 0; pos < record.size(); pos++) {
                    logger.info(pos + " - " + record.get(pos) + ",");
                    switch(pos) {
                        case 0: // CATEGORY_NAME
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_CITIES)[pos])) {
                                city.setCityName(record.get(pos).trim());
                            }
                            break;

                    }


                }
                if (city != null && city.getCityName() != null && !city.getCityName().isEmpty()) {
                    //System.out.println(city);
                    logger.info(city);

                        searchForCity(pstmtSearchForCityId, city);
                        insertCity(pstmtInsert, city);
                        con.commit();

                } else {
                    logger.error("ROLLBACK OCCURED - " + city);
                    con.rollback();
                }
            } catch (SQLException ex) {
                logger.error(ex);
                System.out.println(ex.getMessage());
                con.rollback();
            }
        }


        pstmtSearchForCityId.close();
        pstmtInsert.close();
        con.setAutoCommit(true);
        System.out.println();
    }

    private static void insertCity(PreparedStatement pstmtInsert, City city) throws SQLException {
        if (city.getCityId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setString(1, city.getCityName());
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForCity(PreparedStatement pstmtSearchForCityId, City city) throws SQLException {
        pstmtSearchForCityId.clearParameters();
        pstmtSearchForCityId.setString(1,city.getCityName());
        ResultSet rs = pstmtSearchForCityId.executeQuery();
        city.setCityId(-1);
        if (rs.next()) {
            city.setCityId(rs.getLong("CITY_ID"));
        }
        rs.close();
    }


}
