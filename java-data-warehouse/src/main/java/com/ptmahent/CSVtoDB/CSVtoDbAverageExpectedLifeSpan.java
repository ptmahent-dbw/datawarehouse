package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.AverageLifeSpan;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbAverageExpectedLifeSpan {
    final static Logger logger = Logger.getLogger(CSVtoDbAverageExpectedLifeSpan.class);

    // CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN
    public static void insFromCSVIntoAverageExpectedLifeSpan(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        // Statement sqlStatement = con.createStatement();
        //sqlStatement.execute(TRUNCATE_TABLE_OLTP_AVERAGE_LIFE_SPAN);

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.PREPARED_STMT_INSERT_INTO_AVERAGE_LIFE_SPAN);
        PreparedStatement pstmtSearchForALSpanId = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_AVG_LIFE_SPAN_ID_BY_ALL);
        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            AverageLifeSpan averageLifeSpan = new AverageLifeSpan();
            //Create a new student object and fill his data
            for (int pos = 0; pos < record.size(); pos++) {
                logger.info(pos + " - " + record.get(pos) + ",");
                // CSV -> new String[] { "Ref_Date", "GEO", "SEX", "AGE", "Vector", "Coordinates", "Value"}
                // DB -> (REF_START_YEAR, REF_END_YEAR, GEO, GENDER, AGE_GROUP, LIFE_SPAN) VALUES(?,?,?,?,?,?)
                switch(pos) {
                    case 0: // REF_DATE --> INS INTO 1 , 2

                        if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN)[pos])) {
                            String refDates[] = record.get(pos).trim().split("\\/");
                            averageLifeSpan.setRefStartYear(Long.parseLong(refDates[0]));
                            averageLifeSpan.setRefEndYear(Long.parseLong(refDates[1]));
                        }

                        break;
                    case 1: // GEO
                        if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN)[pos])) {
                            averageLifeSpan.setGeo(record.get(pos).trim());
                        }

                        break;

                    case 2: // SEX
                        if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN)[pos])) {
                            String sex = "" + record.get(pos).trim().charAt(0); // get only first character
                            averageLifeSpan.setGender(sex);
                        }
                        break;

                    case 3: // AGE
                        if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN)[pos])) {
                            averageLifeSpan.setAgeGroup(record.get(pos).trim());

                        }
                        break;

                    case 4: // VECTOR
                        break;
                    case 5: // COORDINATES
                        break;

                    case 6: // Value
                        if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase( CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN)[pos])) {

                            averageLifeSpan.setLifeSpan(Double.parseDouble(record.get(pos).trim()));
                        }
                        break;
                }

            }

            if (averageLifeSpan != null && averageLifeSpan.getAgeGroup() != null && !averageLifeSpan.getAgeGroup().isEmpty()) {
                con.setAutoCommit(false);
                try {
                    logger.info(averageLifeSpan);
                    //System.out.println(averageLifeSpan);
                    searchForAvgLifeSpanId(pstmtSearchForALSpanId, averageLifeSpan);

                    insertAvgLifeSpanRec(pstmtInsert, averageLifeSpan);
                    con.commit();
                } catch (SQLException ex) {
                    logger.error(ex);
                    System.out.println(ex.getMessage());
                    con.rollback();
                }
            }
        }

        //pstmtInsert.executeBatch();

        pstmtInsert.close();
        pstmtSearchForALSpanId.close();
        con.setAutoCommit(true);
        System.out.println();
    }

    private static void insertAvgLifeSpanRec(PreparedStatement pstmtInsert, AverageLifeSpan averageLifeSpan) throws SQLException {
        if (averageLifeSpan.getAverageLifeSpanId() == -1) {
            pstmtInsert.clearParameters();
            pstmtInsert.setLong(1, averageLifeSpan.getRefStartYear());
            pstmtInsert.setLong(2, averageLifeSpan.getRefEndYear());
            pstmtInsert.setString(3, averageLifeSpan.getGeo());
            pstmtInsert.setString(4, averageLifeSpan.getGender());
            pstmtInsert.setString(5, averageLifeSpan.getAgeGroup());
            pstmtInsert.setDouble(6, averageLifeSpan.getLifeSpan());
            pstmtInsert.executeUpdate();
        }
    }

    private static void searchForAvgLifeSpanId(PreparedStatement pstmtSearchForALSpanId, AverageLifeSpan averageLifeSpan) throws SQLException {
        pstmtSearchForALSpanId.clearParameters();
        pstmtSearchForALSpanId.setLong(1,averageLifeSpan.getRefStartYear());
        pstmtSearchForALSpanId.setLong(2,averageLifeSpan.getRefEndYear());
        pstmtSearchForALSpanId.setString(3,averageLifeSpan.getGeo());
        pstmtSearchForALSpanId.setString(4,averageLifeSpan.getGender());
        pstmtSearchForALSpanId.setString(5,averageLifeSpan.getAgeGroup());
        pstmtSearchForALSpanId.setDouble(6,averageLifeSpan.getLifeSpan());
        ResultSet rs = pstmtSearchForALSpanId.executeQuery();

        averageLifeSpan.setAverageLifeSpanId(-1);
        if (rs.next()) {
            averageLifeSpan.setAverageLifeSpanId(rs.getLong("AVERAGE_LIFE_SPAN_ID"));
        }
        rs.close();
    }


}
