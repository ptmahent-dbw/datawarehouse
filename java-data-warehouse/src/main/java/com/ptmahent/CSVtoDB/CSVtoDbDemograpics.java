package com.ptmahent.CSVtoDB;

import com.ptmahent.beans.OLTP.City;
import com.ptmahent.beans.OLTP.DemographicsByCity;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Piratheep Mahent on 2016-12-07.
 */
public class CSVtoDbDemograpics {

    final static Logger logger = Logger.getLogger(CSVtoDbDemograpics.class);
    public static void insFromCSVIntoDemoGrapics(Connection con, List<CSVRecord> csvRecords) throws SQLException {
        // truncate the existing data on the table

        //   Statement sqlStatement = con.createStatement();
        //   sqlStatement.execute("TRUNCATE TABLE OLTP_DEMOGRAPHICS_BY_CITY");

        PreparedStatement pstmtInsert = con.prepareStatement(CSVtoDBSQL.SQL_INS_OLTP_DEMOGRAPHICS);
        PreparedStatement pstmtSearchCity = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_CITY_ID);

        PreparedStatement pstmtInsertCity = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_CITY);

        PreparedStatement pstmtInsertProv = con.prepareStatement(CSVtoDBSQL.SQL_INSERT_INTO_OLTP_PROVINCE_PROV_NAME_VALUES);
        PreparedStatement pstmtSearchProv = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_PROVINCE_ID);

        PreparedStatement pstmtSearchForDemograpicsByYearCity = con.prepareStatement(CSVtoDBSQL.SQL_SEARCH_FOR_DEMOGRAPICS_BY_CITY_YEAR);


        //Read the CSV file records starting from the second record to skip the header
        for (int i = 1; i < csvRecords.size(); i++) {
            CSVRecord record = csvRecords.get(i);
            //Create a new student object and fill his data
            // CSV -> new String[] {"CITY", "2012","2013","2014","2015"});
            // DB ->
            DemographicsByCity demoGrapicsByCity = new DemographicsByCity();

            con.setAutoCommit(false);
            try {
                long cityId = -1;
                for (int pos = 0; pos < record.size(); pos++) {
                    //System.out.print(pos + " - " + record.get(pos) + ",");
                    logger.info(pos + " - " + record.get(pos) + ",");
                    int year = -1;
                    switch (pos) {
                        case 0: // CITY
                            if (!record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_DEMOGRAPHICS)[pos])) {
                                demoGrapicsByCity.setCity(new City(record.get(pos).trim()));
                                searchForCityId(pstmtSearchCity, demoGrapicsByCity);
                                insertCityAndReturnId(pstmtSearchCity, pstmtInsertCity, demoGrapicsByCity);
                            }
                            break;
                        case 1: // YEAR - 2012
                            if (demoGrapicsByCity.getCity() != null && demoGrapicsByCity.getCity().getCityId() != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_DEMOGRAPHICS)[pos])) {
                                String recordElem = record.get(pos).trim().replaceAll(",", "");
                                DemographicsByCity cityDemograpicsByYear = new DemographicsByCity(
                                        demoGrapicsByCity.getCity(),
                                        2012,
                                        Double.parseDouble(recordElem));
                                //System.out.println(cityDemograpicsByYear);
                                logger.info(cityDemograpicsByYear);

                                insertCityDemographics(pstmtInsert, pstmtSearchForDemograpicsByYearCity, cityDemograpicsByYear);
                            }
                            break;
                        case 2: // YEAR - 2013
                            if (demoGrapicsByCity.getCity() != null && demoGrapicsByCity.getCity().getCityId() != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_DEMOGRAPHICS)[pos])) {
                                DemographicsByCity cityDemograpicsByYear = new DemographicsByCity(
                                        demoGrapicsByCity.getCity(),
                                        2013,
                                        Double.parseDouble(record.get(pos).trim().replaceAll(",", "")));
                                //System.out.println(cityDemograpicsByYear);
                                logger.info(cityDemograpicsByYear);

                                insertCityDemographics(pstmtInsert, pstmtSearchForDemograpicsByYearCity, cityDemograpicsByYear);
                            }
                            break;
                        case 3: // YEAR - 2014
                            if (demoGrapicsByCity.getCity() != null && demoGrapicsByCity.getCity().getCityId() != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_DEMOGRAPHICS)[pos])) {
                                DemographicsByCity cityDemograpicsByYear = new DemographicsByCity(
                                        demoGrapicsByCity.getCity(),
                                        2014,
                                        Double.parseDouble(record.get(pos).trim().replaceAll(",", "")));
                                //System.out.println(cityDemograpicsByYear);
                                logger.info(cityDemograpicsByYear);
                                insertCityDemographics(pstmtInsert, pstmtSearchForDemograpicsByYearCity, cityDemograpicsByYear);

                            }
                            break;
                        case 4: // YEAR - 2015
                            if (demoGrapicsByCity.getCity() != null && demoGrapicsByCity.getCity().getCityId() != -1 && !record.get(pos).isEmpty() && !record.get(pos).equalsIgnoreCase(CsvFileReader.fileHeaderMappings.get(CsvFileReader.CSV_FILE_DEMOGRAPHICS)[pos])) {

                                DemographicsByCity cityDemograpicsByYear = new DemographicsByCity(
                                        demoGrapicsByCity.getCity(),
                                        2015,
                                        Double.parseDouble(record.get(pos).trim().replaceAll(",", "")));

                                //System.out.println(cityDemograpicsByYear);
                                logger.info(cityDemograpicsByYear);
                                insertCityDemographics(pstmtInsert, pstmtSearchForDemograpicsByYearCity, cityDemograpicsByYear);

                            }
                            break;
                    }
                }
                con.commit();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                logger.error(ex);
                con.rollback();
            }
        }


        //pstmtInsert.executeBatch();
        pstmtInsert.close();
        pstmtInsertCity.close();
        pstmtInsertProv.close();
        pstmtSearchCity.close();
        pstmtSearchProv.close();

        con.setAutoCommit(true);
        System.out.println();
    }

    private static void insertCityDemographics(PreparedStatement pstmtInsert, PreparedStatement pstmtSearchForDemograpicsByYearCity, DemographicsByCity cityDemograpicsByYear) throws SQLException {
        pstmtSearchForDemograpicsByYearCity.clearParameters();
        pstmtSearchForDemograpicsByYearCity.setLong(1, cityDemograpicsByYear.getCity().getCityId());
        pstmtSearchForDemograpicsByYearCity.setLong(2, cityDemograpicsByYear.getYear());
        ResultSet rs = pstmtSearchForDemograpicsByYearCity.executeQuery();
        if (!rs.next()) {
            pstmtInsert.clearParameters();
            pstmtInsert.setLong(1, cityDemograpicsByYear.getCity().getCityId());
            pstmtInsert.setLong(2, cityDemograpicsByYear.getYear());
            pstmtInsert.setDouble(3, cityDemograpicsByYear.getPopulation());
            pstmtInsert.executeUpdate();
        }
        rs.close();
    }


    private static void insertCityAndReturnId(PreparedStatement pstmtSearchCity, PreparedStatement pstmtInsertCity, DemographicsByCity demographicsByCity) throws SQLException {
        if (demographicsByCity.getCity().getCityId() == -1) {
            // if cityId is not found then insert city
            pstmtInsertCity.getConnection().setAutoCommit(false);
            pstmtInsertCity.clearParameters();
            pstmtInsertCity.setString(1, demographicsByCity.getCity().getCityName());
            pstmtInsertCity.executeUpdate();
            pstmtInsertCity.getConnection().commit();
            searchForCityId(pstmtSearchCity, demographicsByCity);
        }
    }

    private static void searchForCityId(PreparedStatement pstmtSearchCity, DemographicsByCity demographicsByCity) throws SQLException {
        pstmtSearchCity.clearParameters();
        pstmtSearchCity.setString(1, demographicsByCity.getCity().getCityName());
        demographicsByCity.getCity().setCityId(-1);
        ResultSet rs = pstmtSearchCity.executeQuery();
        if (rs.next()) {
            demographicsByCity.getCity().setCityId(rs.getLong("CITY_ID"));
        }
        rs.close();
    }


}
