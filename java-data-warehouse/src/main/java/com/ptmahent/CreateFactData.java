package com.ptmahent;

import com.ptmahent.beans.DIM.*;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Piratheep Mahent on 2016-12-09.
 */
public class CreateFactData {
    final static Logger logger = Logger.getLogger(CreateFactData.class);

    public static void main(String[] args) throws SQLException {
        Connection con = null;
        ResultSet rs = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            con = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1522:ORCL", "ptmahent", "test@2016");
            con.setAutoCommit(true);


            ArrayList<String> sqlSequenceList = new ArrayList<String>();
            createAndResetSequences(con, sqlSequenceList);


            //
            ArrayList<String> sqlTableList = new ArrayList<String>();

            DeleteOrTruncateTables(con, sqlTableList);
            // Fact Sales && Fact Product Sales
            rs = createTBFactOfSales(con, rs);
            // Fact Average Life Span
            convertOltpAvgLifeSpnToFact(con);
            // Fact Top Baby Names
            TopBabyNamesOltpToFact(con);
            // Fact Demograpics

            // retri


            FactDemoGrapics(con);
            /*
            SELECT
            OLTP_CITY.CITY_NAME,
                    OLTP_DEMOGRAPHICS_BY_CITY.CITY_ID,
                    OLTP_DEMOGRAPHICS_BY_CITY.YEAR,
                    OLTP_DEMOGRAPHICS_BY_CITY.POPULATION
            FROM OLTP_DEMOGRAPHICS_BY_CITY,
                    OLTP_CITY


            WHERE  OLTP_DEMOGRAPHICS_BY_CITY.CITY_ID = OLTP_CITY.CITY_ID;
            */

        } catch (Exception ex) {
            if (!con.isClosed()) {
                con.rollback();

            }
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (con != null ) {
                try {
                   con.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void FactDemoGrapics(Connection con) throws SQLException {
        PreparedStatement pstmtSearchDemograpics = con.prepareStatement(" SELECT " +
                "            OLTP_CITY.CITY_NAME, " +
                "                    OLTP_DEMOGRAPHICS_BY_CITY.CITY_ID, " +
                "                    OLTP_DEMOGRAPHICS_BY_CITY.YEAR, " +
                "                    OLTP_DEMOGRAPHICS_BY_CITY.POPULATION " +
                "            FROM PTMAHENT.OLTP_DEMOGRAPHICS_BY_CITY, " +
                "                    PTMAHENT.OLTP_CITY " +
                "            WHERE  OLTP_DEMOGRAPHICS_BY_CITY.CITY_ID = OLTP_CITY.CITY_ID" +
                "           ");

        ResultSet rsSearchDemograpics = pstmtSearchDemograpics.executeQuery();
        long cntPos = 0;
        while (rsSearchDemograpics.next()) {
            // insert into DIM_CITY
            City dimCity = new City();
            dimCity.setCityId(rsSearchDemograpics.getLong("CITY_ID"));
            dimCity.setCityName(rsSearchDemograpics.getString("CITY_NAME"));
            long city_year = rsSearchDemograpics.getLong("YEAR");
            double city_population = rsSearchDemograpics.getDouble("POPULATION");

            searchCity(con, dimCity);

            if (dimCity.getCityKey() == -1){
                insertCity(con, dimCity);
                searchCity(con, dimCity);
            }

            PreparedStatement pstmtSearchFactDemo = con.prepareStatement("SELECT CITY_KEY YEAR FROM PTMAHENT.FACT_DEMOGRAPHICS WHERE CITY_ID = ? AND YEAR = ?");
            pstmtSearchFactDemo.setLong(1, dimCity.getCityId());
            pstmtSearchFactDemo.setLong(2, city_year);
            ResultSet rsSearchFactDemo = pstmtSearchFactDemo.executeQuery();
            if (!rsSearchFactDemo.next()) {
                PreparedStatement  pstmtInsertFactDmo = con.prepareStatement("INSERT INTO PTMAHENT.FACT_DEMOGRAPHICS " +
                        " (CITY_NAME, YEAR, POPULATION, CITY_ID, CITY_KEY) " +
                        " VALUES(?,?,?,?,PTMAHENT.FACT_DEMOGRAPHICS_SEQ.NEXTVAL)");
                pstmtInsertFactDmo.setString(1, dimCity.getCityName());
                pstmtInsertFactDmo.setLong(2, city_year);
                pstmtInsertFactDmo.setDouble(3, city_population);
                pstmtInsertFactDmo.setLong(4, dimCity.getCityId());
                pstmtInsertFactDmo.executeUpdate();
                pstmtInsertFactDmo.close();
                con.commit();
            }
            rsSearchFactDemo.close();


            //
            System.out.print(" - " + cntPos++);


        }
        rsSearchDemograpics.close();
        pstmtSearchDemograpics.close();
    }

    private static void TopBabyNamesOltpToFact(Connection con) throws SQLException {
        PreparedStatement pstmtQueryOltpBabyNames = con.prepareStatement("SELECT   " +
                " OLTP_TBN.BABY_NAME_ID, OLTP_TBN.BABY_NAME, OLTP_TBN.GENDER, " +
                        " OLTP_BFPY.YEAR, OLTP_BFPY.FREQUENCY " +
                " FROM OLTP_BABY_FREQUENCY_PER_YEAR OLTP_BFPY, OLTP_TOP_BABY_NAMES OLTP_TBN " +
                "WHERE OLTP_BFPY.BABY_NAME_ID = OLTP_TBN.BABY_NAME_ID");
        long cntPos = 0;
        ResultSet rsQueryOltpBabyNames = pstmtQueryOltpBabyNames.executeQuery();
        PreparedStatement pstmtInsertBabyFreq = con.prepareStatement("INSERT INTO PTMAHENT.FACT_TOP_BABY_NAMES (FACT_TOP_BABY_NAME_ID, BABY_NAME_KEY, BABY_YEAR, FREQUENCY ) " +
                " VALUES(PTMAHENT.FACT_TOP_BABY_NAMES_SEQ.NEXTVAL, ?, ?, ?)");

        while (rsQueryOltpBabyNames.next()) {
            BabyName tBabyName = new BabyName();
            tBabyName.setBabyName(rsQueryOltpBabyNames.getString("BABY_NAME"));
            tBabyName.setBabyNameId(rsQueryOltpBabyNames.getLong("BABY_NAME_ID"));
            tBabyName.setGender(rsQueryOltpBabyNames.getString("GENDER"));

            //TopBabyNames tBabyName = new TopBabyNames();

            tBabyName.setBabyYear(rsQueryOltpBabyNames.getLong("YEAR"));
            tBabyName.setFrequency(rsQueryOltpBabyNames.getLong("FREQUENCY"));


            searchBabyName(con, tBabyName);
            if (tBabyName.getBabyNameKey() == -1) {
                insertDimBaby(con, tBabyName);
                searchBabyName(con, tBabyName);
            }



            PreparedStatement pstmtSearchBabyFreq = con.prepareStatement("SELECT FACT_TOP_BABY_NAME_ID FROM FACT_TOP_BABY_NAMES WHERE BABY_NAME_KEY = ? AND BABY_YEAR = ?");
            pstmtSearchBabyFreq.setLong(1, tBabyName.getBabyNameKey());
            pstmtSearchBabyFreq.setLong(2, tBabyName.getBabyYear());
            ResultSet rsSearchBabyFreq = pstmtSearchBabyFreq.executeQuery();

            if (!rsSearchBabyFreq.next()) {
                pstmtInsertBabyFreq.clearParameters();
                pstmtInsertBabyFreq.setLong(1, tBabyName.getBabyNameKey());
                pstmtInsertBabyFreq.setLong(2, tBabyName.getBabyYear());
                pstmtInsertBabyFreq.setLong(3, tBabyName.getFrequency());
                pstmtInsertBabyFreq.addBatch();

                System.out.print(" - " + cntPos++);
                System.out.print(((cntPos % 100) == 0 ? "\n" : ""));
            }
            rsSearchBabyFreq.close();
            pstmtSearchBabyFreq.close();


        }
        pstmtInsertBabyFreq.executeBatch();
        con.commit();
        rsQueryOltpBabyNames.close();
    }

    private static void insertDimBaby(Connection con, BabyName babyName) throws SQLException {
        PreparedStatement pstmtInsertBabyName = con.prepareStatement("INSERT INTO DIM_BABY_NAME (BABY_NAME_KEY, BABY_NAME_ID, GENDER, BABY_NAME) " +
                        " VALUES( PTMAHENT.DIM_BABY_NAME_SEQ.NEXTVAL, ?, ?, ?)");
        pstmtInsertBabyName.setLong(1, babyName.getBabyNameId());
        pstmtInsertBabyName.setString(2, babyName.getGender());
        pstmtInsertBabyName.setString(3, babyName.getBabyName());
        pstmtInsertBabyName.executeUpdate();
        pstmtInsertBabyName.getConnection().commit();
        pstmtInsertBabyName.close();

    }

    private static void searchBabyName(Connection con, BabyName babyName) throws SQLException {
        PreparedStatement pstmtSearchBabyNameKey = con.prepareStatement("SELECT BABY_NAME_KEY FROM DIM_BABY_NAME WHERE BABY_NAME = ?");
        pstmtSearchBabyNameKey.setString(1, babyName.getBabyName());
        ResultSet rsSearchBabyNameKey = pstmtSearchBabyNameKey.executeQuery();

        babyName.setBabyNameKey(-1);
        if (rsSearchBabyNameKey.next()) {
            babyName.setBabyNameKey(rsSearchBabyNameKey.getLong("BABY_NAME_KEY"));
        }
        rsSearchBabyNameKey.close();
        pstmtSearchBabyNameKey.close();
    }

    private static void convertOltpAvgLifeSpnToFact(Connection con) throws SQLException {
        //8: CSV_FILE_AVERAGE_EXPECTED_LIFE_SPAN
        con.setAutoCommit(false);
        try {
            PreparedStatement pstmtAVGExpectedLifeSpan =
                    con.prepareStatement("SELECT * FROM PTMAHENT.OLTP_AVERAGE_LIFE_SPAN");
            ResultSet rsAVGExpectedLifeSpan = pstmtAVGExpectedLifeSpan.executeQuery();
            PreparedStatement pstmtInsertAvgExpLifeSpn = con.prepareStatement("INSERT INTO PTMAHENT.FACT_AVERAGE_LIFE_SPAN (FACT_AVG_LIFE_SPAN_ID, " +
                    "AVG_LIFE_SPAN_ID, REF_START_DATE, " +
                    " REF_END_DATE, GEO, GENDER, AGE_GROUP, LIFE_SPAN) VALUES(PTMAHENT.FACT_AVERAGE_LIFE_SPAN_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)");
            long recCnt = 0;
            while (rsAVGExpectedLifeSpan.next()) {

                PreparedStatement pstmtSearchAVGExpLifeSpan = con.prepareStatement("SELECT FACT_AVG_LIFE_SPAN_ID FROM PTMAHENT.FACT_AVERAGE_LIFE_SPAN WHERE AVG_LIFE_SPAN_ID = ?");
                ResultSet rsSearchAVGExpLSpn = pstmtSearchAVGExpLifeSpan.executeQuery();

                if (!rsSearchAVGExpLSpn.next()) {
                    pstmtAVGExpectedLifeSpan.clearParameters();
                    pstmtInsertAvgExpLifeSpn.setLong(1, rsAVGExpectedLifeSpan.getLong("AVG_LIFE_SPAN_ID"));
                    pstmtInsertAvgExpLifeSpn.setDate(2, rsAVGExpectedLifeSpan.getDate("REF_START_DATE"));
                    pstmtInsertAvgExpLifeSpn.setDate(3, rsAVGExpectedLifeSpan.getDate("REF_END_DATE"));
                    pstmtInsertAvgExpLifeSpn.setString(4, rsAVGExpectedLifeSpan.getString("GEO"));
                    pstmtInsertAvgExpLifeSpn.setString(5, rsAVGExpectedLifeSpan.getString("GENDER"));
                    pstmtInsertAvgExpLifeSpn.setString(6, rsAVGExpectedLifeSpan.getString("AGE_GROUP"));
                    pstmtInsertAvgExpLifeSpn.setDouble(7, rsAVGExpectedLifeSpan.getDouble("LIFE_SPAN"));
                    pstmtInsertAvgExpLifeSpn.addBatch();
                    recCnt++;
                }
                rsSearchAVGExpLSpn.close();
                pstmtSearchAVGExpLifeSpan.close();


            }
            pstmtInsertAvgExpLifeSpn.executeBatch();
            con.commit();
            logger.info("ADDED " + recCnt + " records to FACT_AVERAGE_LIFE_SPAN");
            pstmtInsertAvgExpLifeSpn.close();

            rsAVGExpectedLifeSpan.close();
            //9: CSV_FILE_DEMOGRAPHICS

            //10: CSV_FILE_BABY_NAMES

        } catch (SQLException ex) {
            con.rollback();
            logger.error(ex);
        }
    }

    private static ResultSet createTBFactOfSales(Connection con, ResultSet rs) throws SQLException {
        // Read all data

        String sqlSearchFactTbl = "" +
                " SELECT OLTP_SALES_PERSON.CITY_ID    AS SP_CITY_ID," +
                "  OLTP_SALES_PERSON.PROV_ID         AS SP_PROV_ID," +
                "  OLTP_SALES_PERSON.EMAIL_ADDRESS   AS SP_EMAIL_ADDRESS," +
                "  OLTP_SALES_PERSON.ADDRESS         AS SP_ADDRESS," +
                "  OLTP_SALES_PERSON.PHONE_NUMBER    AS SP_PHONE_NUMBER," +
                "  OLTP_SALES_PERSON.DOB             AS SP_DOB," +
                "  OLTP_SALES_PERSON.GENDER          AS SP_GENDER," +
                "  OLTP_SALES_PERSON.LAST_NAME       AS SP_LAST_NAME," +
                "  OLTP_SALES_PERSON.FIRST_NAME      AS SP_FIRST_NAME," +
                "  OLTP_CUSTOMER.CUST_ID             AS CMR_CUST_ID," +
                "  OLTP_CUSTOMER.FIRST_NAME          AS CMR_FIRST_NAME," +
                "  OLTP_CUSTOMER.LAST_NAME           AS CMR_LAST_NAME," +
                "  OLTP_CUSTOMER.GENDER              AS CMR_GENDER," +
                "  OLTP_CUSTOMER.DOB                 AS CMR_DOB," +
                "  OLTP_CUSTOMER.ADDRESS             AS CMR_ADDRESS," +
                "  OLTP_CUSTOMER.PHONE_NUMBER        AS CMR_PHONE_NUMBER," +
                "  OLTP_CUSTOMER.EMAIL_ADDR          AS CMR_EMAIL_ADDR," +
                "  OLTP_CUSTOMER.CITY_ID             AS CMR_CITY_ID," +
                "  OLTP_CUSTOMER.PROV_ID             AS CMR_PROV_ID," +
                "  OLTP_STORE.STORE_NAME," +
                "  (SELECT OLTP_CITY.CITY_NAME AS CMR_CITY_NAME " +
                "  FROM OLTP_CITY" +
                "  WHERE OLTP_CITY.CITY_ID = OLTP_CUSTOMER.CITY_ID " +
                "  ) CMR_CITY_NAME, " +
                "  (SELECT OLTP_CITY.CITY_NAME AS SP_CITY_NAME " +
                "  FROM OLTP_CITY " +
                "  WHERE OLTP_CITY.CITY_ID = OLTP_SALES_PERSON.CITY_ID " +
                "  ) SP_CITY_NAME, " +
                "  (SELECT OLTP_PROVINCE.PROV_NAME AS CMR_PROV_NAME" +
                "  FROM OLTP_PROVINCE" +
                "  WHERE OLTP_PROVINCE.PROV_ID = OLTP_CUSTOMER.PROV_ID" +
                "  ) CMR_PROV_NAME," +
                "  (SELECT OLTP_PROVINCE.PROV_NAME AS SP_PROV_NAME" +
                "  FROM OLTP_PROVINCE" +
                "  WHERE OLTP_PROVINCE.PROV_ID = OLTP_SALES_PERSON.PROV_ID" +
                "  ) SP_PROV_NAME," +
                "  OLTP_SALES_ORDER_LINE.SALES_ORDER_LINE_ID," +
                "  OLTP_SALES_ORDER_LINE.PROD_NAME," +
                "  OLTP_SALES_ORDER_LINE.PRICE," +
                "  OLTP_SALES_ORDER_LINE.DISCOUNT," +
                "  OLTP_SALES_ORDER_LINE.LINE_TOTAL," +
                "  OLTP_SALES_ORDER_LINE.PROD_COUNT," +
                "  OLTP_PRODUCT.PROD_CAT_ID," +
                "  (SELECT OLTP_PRODUCT_CATEGORY.PROD_CAT_NAME" +
                "  FROM OLTP_PRODUCT_CATEGORY" +
                "  WHERE OLTP_PRODUCT_CATEGORY.PROD_CAT_ID = OLTP_PRODUCT.PROD_CAT_ID" +
                "  ) PROD_CAT_NAME," +
                "  OLTP_PRODUCT.PROD_AG_ID," +
                "  (SELECT OLTP_PRODUCT_AGE_GROUP.PROD_AG_CODE" +
                "  FROM OLTP_PRODUCT_AGE_GROUP" +
                "  WHERE OLTP_PRODUCT_AGE_GROUP.PROD_AG_ID = OLTP_PRODUCT.PROD_AG_ID" +
                "  ) PROD_AG_CODE," +
                " (SELECT OLTP_PRODUCT_AGE_GROUP.PROD_MIN_AGE " +
                "  FROM OLTP_PRODUCT_AGE_GROUP" +
                "  WHERE OLTP_PRODUCT_AGE_GROUP.PROD_AG_ID = OLTP_PRODUCT.PROD_AG_ID" +
                "  ) PROD_MIN_AGE," +
                " (SELECT OLTP_PRODUCT_AGE_GROUP.PROD_MAX_AGE " +
                "  FROM OLTP_PRODUCT_AGE_GROUP" +
                "  WHERE OLTP_PRODUCT_AGE_GROUP.PROD_AG_ID = OLTP_PRODUCT.PROD_AG_ID" +
                "  ) PROD_MAX_AGE," +
                "  OLTP_PRODUCT.PROD_COST," +
                "  OLTP_PRODUCT.PROD_SELLING_PRICE," +
                "  OLTP_SALES_ORDER_LINE.PROD_ID," +
                "  OLTP_PRODUCT_INVENTORY.ITEMS_IN_STOCK," +
                "  OLTP_PRODUCT_INVENTORY.ITEMS_ORDER_FROM_VENDOR," +
                "  OLTP_PRODUCT_INVENTORY.SHELF," +
                "  OLTP_PRODUCT_INVENTORY.BIN," +
                "  OLTP_SALES_ORDER.SALES_ORDER_ID," +
                "  OLTP_SALES_ORDER.STORE_ID," +
                "  OLTP_SALES_ORDER.SALES_PERSON_ID," +
                "  OLTP_SALES_ORDER.SALES_DATE," +
                "  OLTP_SALES_ORDER.TOTAL_SALES_BEFORE_TAX," +
                "  OLTP_SALES_ORDER.TOTAL_DISCOUNT," +
                "  OLTP_SALES_ORDER.TOTAL_TAX_AMOUNT," +
                "  OLTP_SALES_ORDER.TOTAL_AMOUNT_CHARGED," +
                "  OLTP_SALES_ORDER.PAYMENT_METHOD," +
                "  OLTP_SALES_ORDER.CUSTOMER_ID," +
                "  OLTP_SALES_ORDER.CUSTOMER_AWARE_BY_AD_COMPAIGN," +
                "  OLTP_SALES_ORDER.INVOICE_ID, " +
                "  OLTP_STORE.STORE_NAME, " +
                " OLTP_STORE.STORE_ADDRESS, " +
                " OLTP_STORE.PROV_ID        AS STORE_PROV_ID, " +
                " OLTP_STORE.CITY_ID        AS STORE_CITY_ID, " +
                " (SELECT PROV_NAME " +
                " FROM PTMAHENT.OLTP_PROVINCE" +
                " WHERE  OLTP_PROVINCE.PROV_ID = OLTP_STORE.PROV_ID) STORE_PROV_NAME, " +
                " (SELECT CITY_NAME " +
                " FROM PTMAHENT.OLTP_CITY " +
                " WHERE  OLTP_CITY.CITY_ID = OLTP_STORE.CITY_ID) STORE_CITY_NAME " +
                " FROM OLTP_SALES_ORDER," +
                "  OLTP_STORE, " +
                "  OLTP_SALES_PERSON," +
                "  OLTP_CUSTOMER," +
                "  OLTP_SALES_ORDER_LINE," +
                "  OLTP_PRODUCT," +
                "  OLTP_PRODUCT_INVENTORY " +
                " WHERE OLTP_SALES_ORDER.CUSTOMER_ID       = OLTP_CUSTOMER.CUST_ID" +
                " AND OLTP_SALES_ORDER.STORE_ID            = OLTP_STORE.STORE_ID" +
                " AND OLTP_SALES_ORDER.SALES_PERSON_ID     = OLTP_SALES_PERSON.SALES_PERSON_ID" +
                " AND OLTP_SALES_ORDER_LINE.SALES_ORDER_ID = OLTP_SALES_ORDER.SALES_ORDER_ID" +
                " AND OLTP_SALES_ORDER_LINE.PROD_ID        = OLTP_PRODUCT.PROD_ID" +
                " AND OLTP_SALES_ORDER_LINE.PROD_ID        = OLTP_PRODUCT_INVENTORY.PROD_ID";

        System.out.println(sqlSearchFactTbl);


        PreparedStatement pstmtSalesOrderRetrieveAll = con.prepareStatement(sqlSearchFactTbl);


        rs = pstmtSalesOrderRetrieveAll.executeQuery();
        long recCnt = 1;
        while (rs.next()) {
            System.out.println("NEW RECORD: " + recCnt);
            for (int pos = 1; pos <= 54; pos++) {

                System.out.println(pos + " -- " + rs.getString(pos));
            }
                AgeGroup dAgeGroup = dimAgeGroup(con, rs);
                City dSalesPersonCity = dimSalesPersonCity(con, rs.getLong("SP_CITY_ID"), rs.getString("SP_CITY_NAME"));
                Province dSalesPersonProv = dimSalesProv(con, rs.getLong("SP_PROV_ID"), rs.getString("SP_PROV_NAME"));
                SalesPerson salesPerson = dimSalesPerson(con, rs, dSalesPersonCity, dSalesPersonProv);
                City dCustomerCity = dimCustomerCity(con, rs.getLong("CMR_CITY_ID"), rs.getString("CMR_CITY_NAME"));
                Province dCustomerProv = dimCustomerProv(con, rs.getLong("CMR_PROV_ID"), rs.getString("CMR_PROV_NAME"));
                Customer dCustomer = dimCustomer(con, rs, dCustomerCity, dCustomerProv);
                DimDate dSalesDate = dimSalesDate(con, rs);
                FiscalPeriod dSalesFiscalPeriod = dimFiscalPeriod(con, dSalesDate);
                dimPaymentMethod(con, rs);
                Product product = dimProduct(con, rs);
                Store store = dimStore(con, rs);
                dimFactSales(con, rs, salesPerson, dCustomer, dSalesDate, dSalesFiscalPeriod, store);
                dimProductSalesFact(con, rs, dAgeGroup, dSalesDate, dSalesFiscalPeriod, product, store);

            recCnt++;
        }




        rs.close();

        return rs;
    }

    private static void dimProductSalesFact(Connection con, ResultSet rs, AgeGroup dAgeGroup, DimDate dSalesDate, FiscalPeriod dSalesFiscalPeriod, Product product, Store store) throws SQLException {
        // SEARCH PRODUCT SALES FACT TABLE
        PreparedStatement pstmtSearchProdSalesFact = con.prepareStatement("SELECT FACT_SALES_LINE_ORDER_ID FROM FACT_PRODUCT_SALES WHERE SALES_ORDER_LINE_ID = ? AND SALES_ORDER_ID = ?");
        pstmtSearchProdSalesFact.setLong(1, rs.getLong("SALES_ORDER_LINE_ID"));

        pstmtSearchProdSalesFact.setLong(2, rs.getLong("SALES_ORDER_ID"));
        ResultSet rsSearchProdSalesFact = pstmtSearchProdSalesFact.executeQuery();

        if (!rsSearchProdSalesFact.next()) {
            PreparedStatement pstmtInsertProdSalesFact = con.prepareStatement("INSERT INTO PTMAHENT.FACT_PRODUCT_SALES (" +
                    " FACT_SALES_LINE_ORDER_ID, SALE_DATE,  STORE_NAME, PROD_NAME, PROD_COST, " +
                    " PROD_SELLING_PRICE, PROD_CAT_NAME, PROD_AG_CODE, PROD_AG_DESC, PROD_MIN_AGE, " +
                    " PROD_MAX_AGE,  SALES_ORDER_LINE_ID, STORE_ADDRESS, STORE_CITY, STORE_PROV, " +
                    " FISCAL_PERIOD, PROD_ID, STORE_ID, INSTANCE_DATE_KEY, STORE_KEY, " +
                    " PROD_KEY, PROD_AG_KEY, FISCAL_PERIOD_KEY, SALES_ORDER_ID) " +
                    " VALUES (PTMAHENT.FACT_PRODUCT_SALES_SEQ.NEXTVAL, ?, ?, ?, ?," +
                    " ?, ?, ?, ?, ?," +
                    " ?, ?, ?, ?, ?," +
                    " ?, ?, ?, ?, ?," +
                    " ?, ?, ?, ?)");
            pstmtInsertProdSalesFact.setDate(1, new Date(dSalesDate.getInstanceDate().getTime()));
            pstmtInsertProdSalesFact.setString(2, store.getStoreName());
            pstmtInsertProdSalesFact.setString(3, product.getProductName());
            pstmtInsertProdSalesFact.setDouble(4, product.getProductCost());
            pstmtInsertProdSalesFact.setDouble(5, product.getProductSellingPrice());
            pstmtInsertProdSalesFact.setString(6, rs.getString("PROD_CAT_NAME"));
            pstmtInsertProdSalesFact.setString(7, dAgeGroup.getAgeGroupCode());
            pstmtInsertProdSalesFact.setString(8, dAgeGroup.getAgeGroupDesc());
            pstmtInsertProdSalesFact.setLong(9, dAgeGroup.getMinAge());
            pstmtInsertProdSalesFact.setLong(10, dAgeGroup.getMaxAge());
            pstmtInsertProdSalesFact.setLong(11, rs.getLong("SALES_ORDER_LINE_ID"));
            pstmtInsertProdSalesFact.setString(12, store.getStoreAddress());
            pstmtInsertProdSalesFact.setString(13, store.getCity().getCityName());
            pstmtInsertProdSalesFact.setString(14, store.getProv().getProvName());
            pstmtInsertProdSalesFact.setString(15, dSalesDate.getFiscalQuarter());
            pstmtInsertProdSalesFact.setLong(16, product.getProductId());
            pstmtInsertProdSalesFact.setLong(17, store.getStoreId());
            pstmtInsertProdSalesFact.setLong(18, dSalesDate.getInstanceDateKey());
            pstmtInsertProdSalesFact.setLong(19, store.getStoreKey());
            pstmtInsertProdSalesFact.setLong(20, product.getProductKey());
            pstmtInsertProdSalesFact.setLong(21, dAgeGroup.getAgeGroupKey());
            pstmtInsertProdSalesFact.setLong(22, dSalesFiscalPeriod.getFiscalPeriodKey());
            pstmtInsertProdSalesFact.setLong(23, rs.getLong("SALES_ORDER_ID"));
            pstmtInsertProdSalesFact.executeUpdate();
            pstmtInsertProdSalesFact.close();

        }
        rsSearchProdSalesFact.close();
        pstmtSearchProdSalesFact.close();
    }

    private static void dimFactSales(Connection con, ResultSet rs, SalesPerson salesPerson, Customer dCustomer, DimDate dSalesDate, FiscalPeriod dSalesFiscalPeriod, Store store) throws SQLException {
        // INSERT FACT SALES

        PreparedStatement pstmSearchFactSales = con.prepareStatement("SELECT SALES_ID FROM PTMAHENT.FACT_SALES WHERE SALES_ORDER_ID = ?");
        pstmSearchFactSales.setLong(1, rs.getLong("SALES_ORDER_ID"));
        ResultSet rsSearchFactSales = pstmSearchFactSales.executeQuery();

        if (!rsSearchFactSales.next()) {
            PreparedStatement pstmtInsertFactSales = con.prepareStatement("INSERT INTO PTMAHENT.FACT_SALES (SALES_ID, " +
                    "SALES_DATE, TOTAL_SALES_BEFORE_TAX,  TOTAL_DISCOUNT,  TOTAL_TAX_AMOUNT, TOTAL_AMOUNT_CHARGED, " +
                    "PAYMENT_METHOD,  CUST_FIRST_NAME,  CUST_LAST_NAME, CUST_GENDER, CUST_DOB, " +
                    "CUST_ADDRESS, CUST_PHONE_NUMBER,  CUST_EMAIL_ADDR, CUST_CITY_NAME, CUST_PROV_NAME, " +
                    "SP_FIRST_NAME, SP_LAST_NAME,  STORE_NAME, STORE_ID, STORE_ADDRESS, " +
                    "STORE_CITY, STORE_PROV, FISCAL_QUARTER,  CUST_ID, SALES_PERSON_ID, " +
                    "CUST_KEY, STORE_KEY,SALES_PERSON_KEY, INSTANCE_DATE_KEY, CITY_KEY, " +
                    "PROV_KEY,  FISCAL_PERIOD_KEY, CUSTOMER_AWARE_BY_AD_COMPAIGN, SALES_ORDER_ID) " +
                    "VALUES(PTMAHENT.FACT_SALES_SEQ.NEXTVAL, " +
                    "?, ?, ?, ?, ?, " +
                    "?, ?, ?, ?, ?, " +
                    "?, ?, ?, ?, ?, " +
                    "?, ?, ?, ?, ?," +
                    "?, ?, ?, ?, ?," +
                    "?, ?, ?, ?, ?," +
                    "?, ?, ?, ? " +
                    ")");

            pstmtInsertFactSales.setDate(1, new Date(dSalesDate.getInstanceDate().getTime()));
            pstmtInsertFactSales.setDouble(2, rs.getDouble("TOTAL_SALES_BEFORE_TAX"));
            pstmtInsertFactSales.setDouble(3, rs.getDouble("TOTAL_DISCOUNT"));
            pstmtInsertFactSales.setDouble(4, rs.getDouble("TOTAL_TAX_AMOUNT"));
            pstmtInsertFactSales.setDouble(5, rs.getDouble("TOTAL_AMOUNT_CHARGED"));
            pstmtInsertFactSales.setString(6, rs.getString("PAYMENT_METHOD"));
            pstmtInsertFactSales.setString(7,dCustomer.getCustFirstName());
            pstmtInsertFactSales.setString(8, dCustomer.getCustLastName());
            pstmtInsertFactSales.setString(9, dCustomer.getGender().getGender());
            pstmtInsertFactSales.setDate(10, new Date(dCustomer.getDob().getTime()));
            pstmtInsertFactSales.setString(11, dCustomer.getCustAddress());
            pstmtInsertFactSales.setString(12, dCustomer.getPhoneNumber());
            pstmtInsertFactSales.setString(13, dCustomer.getEmailAddress());
            pstmtInsertFactSales.setString(14, dCustomer.getCity().getCityName());

            pstmtInsertFactSales.setString(15, dCustomer.getProv().getProvName());
            pstmtInsertFactSales.setString(16, salesPerson.getFirstName());
            pstmtInsertFactSales.setString(17, salesPerson.getLastName());
            pstmtInsertFactSales.setString(18, store.getStoreName());
            pstmtInsertFactSales.setLong(19, store.getStoreId());
            pstmtInsertFactSales.setString(20, store.getStoreAddress());
            pstmtInsertFactSales.setString(21, store.getCity().getCityName());
            pstmtInsertFactSales.setString(22, store.getProv().getProvName());
            pstmtInsertFactSales.setString(23, dSalesDate.getFiscalQuarter());
            pstmtInsertFactSales.setLong(24, dCustomer.getCustId());
            pstmtInsertFactSales.setLong(25, salesPerson.getSalesPersonId());
            pstmtInsertFactSales.setLong(26, dCustomer.getCustKey());
            pstmtInsertFactSales.setLong(27, store.getStoreKey());
            pstmtInsertFactSales.setLong(28, salesPerson.getSalesPersonKey());
            pstmtInsertFactSales.setLong(29, dSalesDate.getInstanceDateKey());
            pstmtInsertFactSales.setLong(30, store.getCity().getCityKey());
            pstmtInsertFactSales.setLong(31, store.getProv().getProvKey());
            pstmtInsertFactSales.setLong(32, dSalesFiscalPeriod.getFiscalPeriodKey());
            pstmtInsertFactSales.setString(33, rs.getString("CUSTOMER_AWARE_BY_AD_COMPAIGN"));
            pstmtInsertFactSales.setLong(34, rs.getLong("SALES_ORDER_ID"));
            pstmtInsertFactSales.executeUpdate();

            pstmtInsertFactSales.close();
        }
        rsSearchFactSales.close();
        pstmSearchFactSales.close();
        // INSERT PRODUCT FACT SALES
    }

    private static Store dimStore(Connection con, ResultSet rs) throws SQLException {
        // DIMENSION DIM_STORE
        Store store = new Store();
        store.setStoreName(rs.getString("STORE_NAME"));
        store.setStoreId(rs.getLong("STORE_ID"));
        store.setCity(new City());
        store.getCity().setCityId(rs.getLong("STORE_CITY_ID"));
        store.getCity().setCityName(rs.getString("STORE_CITY_NAME"));

        searchCity(con, store.getCity());
        if (store.getCity().getCityKey() == -1){
            insertCity(con, store.getCity());
            searchCity(con, store.getCity());
        }

        store.setProv(new Province());
        store.getProv().setProvId(rs.getLong("STORE_PROV_ID"));
        store.getProv().setProvName(rs.getString("STORE_PROV_NAME"));
        searchProv(con, store.getProv());
        if (store.getProv().getProvKey() == -1) {
            insertProv(con, store.getProv());
            searchProv(con, store.getProv());
        }
        store.setStoreAddress(rs.getString("STORE_ADDRESS"));


        searchStore(con, store);
        if (store.getStoreKey() == -1) {
            insertStore(con, store);
            searchStore(con, store);
        }
        return store;
    }

    private static Product dimProduct(Connection con, ResultSet rs) throws SQLException {
        // DIMENSION DIM_PRODUCT
        Product product = new Product();
        product.setProductId(rs.getLong("PROD_ID"));
        product.setProductName(rs.getString("PROD_NAME"));
        product.setProductCost(rs.getDouble("PROD_COST"));
        product.setProductSellingPrice(rs.getDouble("PRICE"));

        searchProduct(con, product);
        if (product.getProductKey() == -1) {
            insertProduct(con, product);
            searchProduct(con, product);
        }
        return product;
    }

    private static void dimPaymentMethod(Connection con, ResultSet rs) throws SQLException {
        // DIMENSION DIM_PAYMENT_METHOD
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setPaymentMethod(rs.getString("PAYMENT_METHOD"));


        searchPaymentMethod(con, paymentMethod);
        if (paymentMethod.getPaymentMethodKey() == -1) {
            insertPaymentMethod(con, paymentMethod);
            searchPaymentMethod(con, paymentMethod);
        }
    }

    private static FiscalPeriod dimFiscalPeriod(Connection con, DimDate dSalesDate) throws SQLException {
        // DIMENSION DIM_FISCAL_PERIOD
        FiscalPeriod dSalesFiscalPeriod = new FiscalPeriod();
        dSalesFiscalPeriod.setFiscalPeriod(dSalesDate.getFiscalQuarter());

        // Initial search
        searchFiscalPeriodKey(con, dSalesFiscalPeriod);
        // if not found then insert
        if (dSalesFiscalPeriod.getFiscalPeriodKey() == -1) {
            insertFiscalPeriod(con, dSalesDate, dSalesFiscalPeriod);
            // search again for new key
            searchFiscalPeriodKey(con, dSalesFiscalPeriod);
        }
        return dSalesFiscalPeriod;
    }

    private static DimDate dimSalesDate(Connection con, ResultSet rs) throws SQLException {
        // DIMENSION DIM_DATE
        DimDate dSalesDate = new DimDate();
        dSalesDate.setInstanceDate(rs.getDate("SALES_DATE"));

        searchDate(con, dSalesDate);
        if (dSalesDate.getInstanceDateKey() == -1) {
            insertDate(con, dSalesDate);
            searchDate(con, dSalesDate);
        }
        return dSalesDate;
    }

    private static Customer dimCustomer(Connection con, ResultSet rs, City dCustomerCity, Province dCustomerProv) throws SQLException {
        // DIMENSION DIM_CUSTOMER
        Customer dCustomer = new Customer();
        dCustomer.setCity(dCustomerCity);
        dCustomer.setCustId(rs.getLong("CMR_CUST_ID"));
        dCustomer.setCustFirstName(rs.getString("CMR_FIRST_NAME"));
        dCustomer.setCustLastName(rs.getString("CMR_LAST_NAME"));
        dCustomer.setProv(dCustomerProv);
        dCustomer.setCity(dCustomerCity);
        dCustomer.setGender(new Gender());
        dCustomer.getGender().setGender(rs.getString("CMR_GENDER"));
        dCustomer.setCustAddress(rs.getString("CMR_ADDRESS"));
        dCustomer.setDob(rs.getDate("CMR_DOB"));
        dCustomer.setEmailAddress(rs.getString("CMR_EMAIL_ADDR"));
        dCustomer.setPhoneNumber(rs.getString("CMR_PHONE_NUMBER"));

        searchCustomer(con, dCustomer);
        if (dCustomer.getCustKey() == -1) {
            insertCustomer(con, dCustomer);
            searchCustomer(con, dCustomer);
        }
        return dCustomer;
    }

    private static Province dimCustomerProv(Connection con, long cmr_prov_id, String cmr_prov_name) throws SQLException {
        // Dimension DIM_PROVINCE - Customer
        Province dCustomerProv = new Province();
        dCustomerProv.setProvId(cmr_prov_id);
        dCustomerProv.setProvName(cmr_prov_name);

        searchProv(con, dCustomerProv);
        if (dCustomerProv.getProvKey() == -1) {
            insertProv(con, dCustomerProv);
            searchProv(con, dCustomerProv);
        }
        return dCustomerProv;
    }

    private static City dimCustomerCity(Connection con, long cmr_city_id, String cmr_city_name) throws SQLException {
        // DIMENSION DIM_CITY -- Customer Person
        City dCustomerCity = new City();
        dCustomerCity.setCityId(cmr_city_id);
        dCustomerCity.setCityName(cmr_city_name);

        searchCity(con, dCustomerCity);
        if (dCustomerCity.getCityKey() == -1) {
            insertCity(con, dCustomerCity);
            searchCity(con, dCustomerCity);
        }
        return dCustomerCity;
    }

    private static SalesPerson dimSalesPerson(Connection con, ResultSet rs, City dSalesPersonCity, Province dSalesPersonProv) throws SQLException {
        // DIMENSION DIM_SALES_PERSON
        SalesPerson salesPerson = new SalesPerson();
        salesPerson.setSalesPersonId(rs.getLong("SALES_PERSON_ID"));
        salesPerson.setFirstName(rs.getString("SP_FIRST_NAME"));
        salesPerson.setLastName(rs.getString("SP_LAST_NAME"));
        salesPerson.setGender(new Gender());
        salesPerson.getGender().setGender(rs.getString("CMR_GENDER"));
        salesPerson.setEmailAddress(rs.getString("SP_EMAIL_ADDRESS"));
        salesPerson.setAddress(rs.getString("SP_ADDRESS"));
        salesPerson.setCity(dSalesPersonCity);
        salesPerson.setProv(dSalesPersonProv);

        searchForSalesPerson(con, salesPerson);
        if (salesPerson.getSalesPersonKey() == -1) {
            insertSalesPerson(con, salesPerson);
            searchForSalesPerson(con, salesPerson);
        }
        return salesPerson;
    }

    private static Province dimSalesProv(Connection con, long sp_prov_id, String sp_prov_name) throws SQLException {
        // DIMENSION DIM_PROVINCE -- Sales Person
        Province dSalesPersonProv = new Province();
        dSalesPersonProv.setProvId(sp_prov_id);
        dSalesPersonProv.setProvName(sp_prov_name);

        searchProv(con, dSalesPersonProv);
        if (dSalesPersonProv.getProvKey() == -1) {
            insertProv(con, dSalesPersonProv);
            searchProv(con, dSalesPersonProv);
        }
        return dSalesPersonProv;
    }

    private static City dimSalesPersonCity(Connection con, long sp_city_id, String sp_city_name) throws SQLException {
        // DIMENSION DIM_CITY -- Sales Person
        City dSalesPersonCity = new City();
        dSalesPersonCity.setCityId(sp_city_id);
        dSalesPersonCity.setCityName(sp_city_name);

        // Search City first time for Key
        searchCity(con, dSalesPersonCity);
        // Insert city first
        if (dSalesPersonCity.getCityKey() == -1) {
            insertCity(con, dSalesPersonCity);
            // Search city again for Key
            searchCity(con, dSalesPersonCity);
        }
        return dSalesPersonCity;
    }

    private static AgeGroup dimAgeGroup(Connection con, ResultSet rs) throws SQLException {
        // DIMENSION DIM_AGE_GROUP
        AgeGroup dAgeGroup = new AgeGroup();
        dAgeGroup.setAgeGroupId(rs.getLong("PROD_AG_ID"));
        dAgeGroup.setAgeGroupCode(rs.getString("PROD_AG_CODE"));
        dAgeGroup.setMinAge(rs.getLong("PROD_MIN_AGE"));
        dAgeGroup.setMaxAge(rs.getLong("PROD_MAX_AGE"));

        System.out.println("PROD AGE GROUP ID : " + dAgeGroup.getAgeGroupId());
        System.out.println("PROD AGE GROUP CODE: " + dAgeGroup.getAgeGroupCode());

        searchDimAgeGroupForKey(con, dAgeGroup);
        ResultSet rsSearch;
        if (dAgeGroup.getAgeGroupKey() == -1) {
            insertDimAgeGroup(con, dAgeGroup);
            searchDimAgeGroupForKey(con, dAgeGroup);

        }
        return dAgeGroup;
    }

    private static void DeleteOrTruncateTables(Connection con, ArrayList<String> sqlTableList) {
        sqlTableList.add("PTMAHENT.FACT_SALES");
        sqlTableList.add("PTMAHENT.FACT_PRODUCT_SALES");
        sqlTableList.add("PTMAHENT.FACT_DEMOGRAPHICS");
        sqlTableList.add("PTMAHENT.FACT_AVERAGE_LIFE_SPAN");
        sqlTableList.add("PTMAHENT.FACT_TOP_BABY_NAMES");

        sqlTableList.add("PTMAHENT.DIM_AGE_GROUP");
        sqlTableList.add("PTMAHENT.DIM_CITY");
        sqlTableList.add("PTMAHENT.DIM_CUSTOMER");
        sqlTableList.add("PTMAHENT.DIM_DATE");
        sqlTableList.add("PTMAHENT.DIM_FISCAL_PERIOD");
        sqlTableList.add("PTMAHENT.DIM_GENDER");
        sqlTableList.add("PTMAHENT.DIM_PAYMENT_METHOD");
        sqlTableList.add("PTMAHENT.DIM_PRODUCT");
        sqlTableList.add("PTMAHENT.DIM_PROVINCE");
        sqlTableList.add("PTMAHENT.DIM_SALES_PERSON");
        sqlTableList.add("PTMAHENT.DIM_STORE");

        for (String sqlTable : sqlTableList) {

            try (Statement stmtTruncateTbl = con.createStatement()) {
                stmtTruncateTbl.execute("TRUNCATE TABLE " + sqlTable);

                System.out.println("TRUNCATING TABLE " + sqlTable + " - succeeded");
                stmtTruncateTbl.close();

            } catch(SQLException ex) {
                System.out.println(" TRUNCATING TABLE " + sqlTable + " - failed");

                try (Statement stmtDeleteRows = con.createStatement() ) {
                    stmtDeleteRows.execute("DELETE FROM " + sqlTable);
                    stmtDeleteRows.getConnection().commit();
                    stmtDeleteRows.close();
                    System.out.println("DELETE ALL ROWS from " + sqlTable + " - success");
                } catch (SQLException sqlEx) {
                    System.out.println("DELETE ALL ROWS from " + sqlTable + " - failed");
                }


            }
        }
    }

    private static void createAndResetSequences(Connection con, ArrayList<String> sqlSequenceList) {
        // Fact Tables
        sqlSequenceList.add("PTMAHENT.FACT_SALES_SEQ");
        sqlSequenceList.add("PTMAHENT.FACT_PRODUCT_SALES_SEQ");
        sqlSequenceList.add("PTMAHENT.FACT_DEMOGRAPHICS_SEQ");
        sqlSequenceList.add("PTMAHENT.FACT_AVERAGE_LIFE_SPAN_SEQ");
        sqlSequenceList.add("PTMAHENT.FACT_TOP_BABY_NAMES_SEQ");

        // DIM Tables
        sqlSequenceList.add("PTMAHENT.DIM_AGE_GROUP_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_CITY_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_CUSTOMER_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_DATE_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_FISCAL_PERIOD_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_GENDER_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_PAYMENT_METHOD_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_PRODUCT_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_PROVINCE_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_SALES_PERSON_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_STORE_SEQ");
        sqlSequenceList.add("PTMAHENT.DIM_BABY_NAME_SEQ");
        //PTMAHENT.DIM_BABY_NAME_SEQ

        for (String seq: sqlSequenceList) {
            String sSqlDropSeq =
                    " DROP SEQUENCE " + seq;

            System.out.println(sSqlDropSeq);

            try (Statement stmtDropSeq = con.createStatement()){

                stmtDropSeq.execute(sSqlDropSeq);
                stmtDropSeq.close();
                System.out.println("DROP SEQUENCE " + seq + " - SUCCEEDED");
            } catch (SQLException ex) {
                System.out.println("DROP SEQUENCE " + seq + " - FAILED");
            }
            System.out.println("DROPPING SEQ " + seq);
            // CREATE SEQUENCE DIM_FISCAL_PERIOD_SEQ INCREMENT BY 1
            // MAXVALUE 9999999999999999999999999999 MINVALUE 1 CACHE 20;

            String sSqlCreateSeq = "CREATE SEQUENCE " + seq + " INCREMENT BY 1 " +
                    " MAXVALUE 999999999999999 MINVALUE 1 CACHE 20";

            try (Statement stmtCreateSeq = con.createStatement()) {
                System.out.println(sSqlCreateSeq);
                stmtCreateSeq.execute(sSqlCreateSeq);
                stmtCreateSeq.close();
                System.out.println("CREATING SEQ " + seq + " - SUCCEEDED");

            } catch (SQLException ex) {
                System.out.println("CREATING SEQ " + seq + " - FAILED");
            }

        }
    }

    private static void insertStore(Connection con, Store store) throws SQLException {

            PreparedStatement pstmInsertStore = con.prepareStatement("INSERT INTO PTMAHENT.DIM_STORE (STORE_KEY, STORE_ID, STORE_NAME) " +
                    " VALUES(PTMAHENT.DIM_STORE_SEQ.NEXTVAL, ?, ?)");

            pstmInsertStore.setLong(1, store.getStoreId());
            pstmInsertStore.setString(2, store.getStoreName());
            pstmInsertStore.executeUpdate();
            pstmInsertStore.close();

    }

    private static void searchStore(Connection con, Store store) throws SQLException {
        PreparedStatement pstmSearchStore = con.prepareStatement("SELECT STORE_KEY FROM PTMAHENT.DIM_STORE WHERE STORE_ID = ?");
        pstmSearchStore.setLong(1, store.getStoreId());
        ResultSet rsSearchStore = pstmSearchStore.executeQuery();
        store.setStoreKey(-1);
        if (rsSearchStore.next()) {
            store.setStoreKey(rsSearchStore.getLong("STORE_KEY"));
        }
        rsSearchStore.close();
        pstmSearchStore.close();
    }

    private static void insertProduct(Connection con, Product product) throws SQLException {

            PreparedStatement pstmInsertProduct = con.prepareStatement("INSERT INTO PTMAHENT.DIM_PRODUCT " +
                    " (PROD_KEY, PROD_ID, PROD_NAME, PROD_COST, PROD_SELLING_PRICE) " +
                    " VALUES (PTMAHENT.DIM_PRODUCT_SEQ.NEXTVAL, ?,?,?,?)");
            pstmInsertProduct.setLong(1, product.getProductId());
            pstmInsertProduct.setString(2, product.getProductName());
            pstmInsertProduct.setDouble(3, product.getProductCost());
            pstmInsertProduct.setDouble(4, product.getProductSellingPrice());
            pstmInsertProduct.executeUpdate();
            pstmInsertProduct.close();

    }

    private static void searchProduct(Connection con, Product product) throws SQLException {
        PreparedStatement pstmSearchProduct = con.prepareStatement("SELECT PROD_KEY FROM PTMAHENT.DIM_PRODUCT WHERE PROD_ID = ?");
        pstmSearchProduct.setLong(1, product.getProductId());
        ResultSet rsSearchProduct = pstmSearchProduct.executeQuery();

        product.setProductKey(-1);
        if (rsSearchProduct.next()) {
            product.setProductKey(rsSearchProduct.getLong("PROD_KEY"));
        }
        rsSearchProduct.close();
        pstmSearchProduct.close();
    }

    private static void insertPaymentMethod(Connection con, PaymentMethod paymentMethod) throws SQLException {

            // Insert payment method
            PreparedStatement pstmtInsertPaymentMethod = con.prepareStatement("INSERT INTO PTMAHENT.DIM_PAYMENT_METHOD (PAYMENT_METHOD_KEY, PAYMENT_METHOD) " +
                    " VALUES(PTMAHENT.DIM_PAYMENT_METHOD_SEQ.NEXTVAL, ?)");

            pstmtInsertPaymentMethod.setString(1, paymentMethod.getPaymentMethod());
            pstmtInsertPaymentMethod.executeUpdate();
            pstmtInsertPaymentMethod.close();

    }

    private static void searchPaymentMethod(Connection con, PaymentMethod paymentMethod) throws SQLException {
        PreparedStatement pstmtSearchPaymentMethod = con.prepareStatement("SELECT PAYMENT_METHOD_KEY FROM PTMAHENT.DIM_PAYMENT_METHOD WHERE PAYMENT_METHOD = ?");
        pstmtSearchPaymentMethod.setString(1, paymentMethod.getPaymentMethod());
        ResultSet rsSearchPaymentMethod = pstmtSearchPaymentMethod.executeQuery();

        paymentMethod.setPaymentMethodKey(-1);
        if (rsSearchPaymentMethod.next()) {
            paymentMethod.setPaymentMethodKey(rsSearchPaymentMethod.getLong("PAYMENT_METHOD_KEY"));
        }
        rsSearchPaymentMethod.close();
        pstmtSearchPaymentMethod.close();
    }

    private static void insertFiscalPeriod(Connection con, DimDate dSalesDate, FiscalPeriod dSalesFiscalPeriod) throws SQLException {

            // Insert Fiscal Period
            PreparedStatement pstmtInsertFiscalQuarter = con.prepareStatement("INSERT INTO PTMAHENT.DIM_FISCAL_PERIOD (FISCAL_PERIOD_KEY, YEAR, PERIOD, FISCAL_PERIOD) " +
                    " VALUES(PTMAHENT.DIM_FISCAL_PERIOD_SEQ.NEXTVAL, ?,?,?)");
            pstmtInsertFiscalQuarter.setLong(1, dSalesDate.getFSYear());
            pstmtInsertFiscalQuarter.setLong(2, dSalesDate.getFSQuarter());
            pstmtInsertFiscalQuarter.setString(3, dSalesDate.getFiscalQuarter());
            pstmtInsertFiscalQuarter.executeUpdate();
            pstmtInsertFiscalQuarter.close();

    }

    private static void searchFiscalPeriodKey(Connection con, FiscalPeriod dSalesFiscalPeriod) throws SQLException {
        PreparedStatement pstmtSearchFiscalPeriod = con.prepareStatement("SELECT FISCAL_PERIOD_KEY FROM PTMAHENT.DIM_FISCAL_PERIOD WHERE FISCAL_PERIOD = ?");
        pstmtSearchFiscalPeriod.setString(1,dSalesFiscalPeriod.getFiscalPeriod());
        ResultSet rsSearchFiscalPeriod = pstmtSearchFiscalPeriod.executeQuery();

        dSalesFiscalPeriod.setFiscalPeriodKey(-1);
        if (rsSearchFiscalPeriod.next()) {
            dSalesFiscalPeriod.setFiscalPeriodKey(rsSearchFiscalPeriod.getLong("FISCAL_PERIOD_KEY"));
        }
        rsSearchFiscalPeriod.close();
        pstmtSearchFiscalPeriod.close();
    }

    private static void insertDate(Connection con, DimDate dSalesDate) throws SQLException {

            PreparedStatement pstmtInsertDate = con.prepareStatement("INSERT INTO PTMAHENT.DIM_DATE (INSTANCE_DATE_KEY, INSTANCE_DATE, FISCAL_PERIOD) " +
                    " VALUES(PTMAHENT.DIM_DATE_SEQ.NEXTVAL, ?, ?)");
            pstmtInsertDate.setDate(1, new Date(dSalesDate.getInstanceDate().getTime()));
            pstmtInsertDate.setString(2, dSalesDate.getFiscalQuarter());
            pstmtInsertDate.executeUpdate();
            pstmtInsertDate.close();

    }

    private static void searchDate(Connection con, DimDate dSalesDate) throws SQLException {
        PreparedStatement pstmtSearchDate = con.prepareStatement("SELECT INSTANCE_DATE_KEY FROM PTMAHENT.DIM_DATE WHERE INSTANCE_DATE = ?");
        pstmtSearchDate.setDate(1, new Date(dSalesDate.getInstanceDate().getTime()));
        ResultSet rsSearchDate = pstmtSearchDate.executeQuery();

        dSalesDate.setInstanceDateKey(-1);
        if (rsSearchDate.next()) {
            dSalesDate.setInstanceDateKey(rsSearchDate.getLong("INSTANCE_DATE_KEY"));
        }
        rsSearchDate.close();
        pstmtSearchDate.close();
    }

    private static void insertCustomer(Connection con, Customer dCustomer) throws SQLException {

            PreparedStatement pstmtInsertCust = con.prepareStatement("INSERT INTO PTMAHENT.DIM_CUSTOMER " +
                                    " (CUST_KEY, FIRST_NAME, LAST_NAME, " +
                                    " ADDRESS, CITY_ID, PROV_ID, CUST_ID, PHONE_NUMBER) " +
                                            " VALUES(PTMAHENT.DIM_CUSTOMER_SEQ.NEXTVAL, ?, ?, ?, ?, ?, ?, ?)");
            pstmtInsertCust.setString(1, dCustomer.getCustFirstName());
            pstmtInsertCust.setString(2, dCustomer.getCustLastName());
            pstmtInsertCust.setString(3, dCustomer.getCustAddress());

            pstmtInsertCust.setLong(4, dCustomer.getCity().getCityId());
            pstmtInsertCust.setLong(5, dCustomer.getProv().getProvId());
            pstmtInsertCust.setLong(6, dCustomer.getCustId());
            pstmtInsertCust.setString(7, dCustomer.getPhoneNumber());
            pstmtInsertCust.executeUpdate();
            pstmtInsertCust.close();

    }

    private static void searchCustomer(Connection con, Customer dCustomer) throws SQLException {
        PreparedStatement pstmtSearchCust = con.prepareStatement("SELECT CUST_KEY FROM PTMAHENT.DIM_CUSTOMER WHERE CUST_ID = ?");
        pstmtSearchCust.setLong(1, dCustomer.getCustId());
        ResultSet rsCustSearch = pstmtSearchCust.executeQuery();

        dCustomer.setCustKey(-1);
        if (rsCustSearch.next()) {
            dCustomer.setCustKey(rsCustSearch.getLong("CUST_KEY"));
        }
        rsCustSearch.close();
        pstmtSearchCust.close();
    }

    private static void insertSalesPerson(Connection con, SalesPerson salesPerson) throws SQLException {

            // insert Gender
            PreparedStatement pstmtInsertSP = con.prepareStatement("INSERT INTO PTMAHENT.DIM_SALES_PERSON " +
                " (SALES_PERSON_KEY, SALES_PERSON_ID, FIRST_NAME, LAST_NAME, GENDER, " +
                    " EMAIL_ADDRESS, ADDRESS, CITY_ID, PROV_ID) " +
                " VALUES (PTMAHENT.DIM_SALES_PERSON_SEQ.NEXTVAL, ?, ?, ?, ?, " +
                " ?, ?, ?, ?)");
            pstmtInsertSP.clearParameters();
            pstmtInsertSP.setLong(1, salesPerson.getSalesPersonId());
            pstmtInsertSP.setString(2, salesPerson.getFirstName());
            pstmtInsertSP.setString(3, salesPerson.getLastName());
            pstmtInsertSP.setString(4, salesPerson.getGender().getGender());
            pstmtInsertSP.setString(5, salesPerson.getEmailAddress());
            pstmtInsertSP.setString(6, salesPerson.getAddress());
            pstmtInsertSP.setLong(7, salesPerson.getCity().getCityId());
            pstmtInsertSP.setLong(8, salesPerson.getProv().getProvId());
            pstmtInsertSP.executeUpdate();
            pstmtInsertSP.close();



    }

    private static void searchForSalesPerson(Connection con, SalesPerson salesPerson) throws SQLException {
        // Search Sales Person
        PreparedStatement pstmtSearchSP = con.prepareStatement("SELECT SALES_PERSON_KEY FROM PTMAHENT.DIM_SALES_PERSON WHERE SALES_PERSON_ID = ?");
        pstmtSearchSP.clearParameters();
        pstmtSearchSP.setLong(1, salesPerson.getSalesPersonId());
        ResultSet rsSP = pstmtSearchSP.executeQuery();

        salesPerson.setSalesPersonKey(-1);
        if (rsSP.next()) {
            salesPerson.setSalesPersonKey(rsSP.getLong("SALES_PERSON_KEY"));
        }
        rsSP.close();
        pstmtSearchSP.close();
    }

    private static void insertProv(Connection con, Province dProv) throws SQLException {

            // insert new prov
            PreparedStatement pstmtInsertProv = con.prepareStatement("INSERT INTO DIM_PROVINCE (PROV_KEY, PROV_ID, PROV_NAME) "
                    + " VALUES(PTMAHENT.DIM_PROVINCE_SEQ.NEXTVAL, ?, ?)");
            pstmtInsertProv.setLong(1, dProv.getProvId());
            pstmtInsertProv.setString(2, dProv.getProvName());
            pstmtInsertProv.executeUpdate();
            pstmtInsertProv.close();


    }

    private static void searchProv(Connection con, Province dSalesPersonProv) throws SQLException {
        // Search Prov first time for Key
        PreparedStatement pstmtSearchProv = con.prepareStatement("SELECT PROV_KEY FROM DIM_PROVINCE WHERE PROV_ID = ?");
        pstmtSearchProv.setLong(1, dSalesPersonProv.getProvId());
        ResultSet rsProvSearch = pstmtSearchProv.executeQuery();

        dSalesPersonProv.setProvKey(-1);
        if (rsProvSearch.next()) {
            dSalesPersonProv.setProvKey(rsProvSearch.getLong("PROV_KEY"));
        }
        rsProvSearch.close();
        pstmtSearchProv.close();
    }

    private static void insertCity(Connection con, City dCity) throws SQLException {

            PreparedStatement pstmtInsertCity = con.prepareStatement("INSERT INTO PTMAHENT.DIM_CITY (CITY_KEY, CITY_ID, CITY_NAME) VALUES (PTMAHENT.DIM_CITY_SEQ.NEXTVAL, ?,?)");
            pstmtInsertCity.setLong(1, dCity.getCityId());
            pstmtInsertCity.setString(2, dCity.getCityName());
            pstmtInsertCity.executeUpdate();
            pstmtInsertCity.close();

    }

    private static void searchCity(Connection con, City dSalesPersonCity) throws SQLException {
        PreparedStatement pstmtSearchCity = con.prepareStatement("SELECT CITY_KEY FROM PTMAHENT.DIM_CITY WHERE CITY_ID = ?");
        pstmtSearchCity.setLong(1, dSalesPersonCity.getCityId());
        ResultSet rsSearchCity = pstmtSearchCity.executeQuery();

        dSalesPersonCity.setCityKey(-1);
        if (rsSearchCity.next()) {
            dSalesPersonCity.setCityKey(rsSearchCity.getLong("CITY_KEY"));
        }
        rsSearchCity.close();
        pstmtSearchCity.close();
    }

    private static void insertDimAgeGroup(Connection con, AgeGroup dAgeGroup) throws SQLException {
        // Insert age group
        if (dAgeGroup.getAgeGroupKey() == -1) {

            PreparedStatement pstmtInsertAgeGroup = con.prepareStatement("INSERT INTO PTMAHENT.DIM_AGE_GROUP (AG_KEY, AG_ID, AG_CODE, MIN_AGE, MAX_AGE) "
                    + " VALUES(PTMAHENT.DIM_AGE_GROUP_SEQ.NEXTVAL, ?,?, ?, ?) ");
            pstmtInsertAgeGroup.clearParameters();
            pstmtInsertAgeGroup.setLong(1, dAgeGroup.getAgeGroupId());
            pstmtInsertAgeGroup.setString(2, dAgeGroup.getAgeGroupCode());
            pstmtInsertAgeGroup.setLong(3, dAgeGroup.getMinAge());
            pstmtInsertAgeGroup.setLong(4, dAgeGroup.getMaxAge());
            pstmtInsertAgeGroup.executeUpdate();
            pstmtInsertAgeGroup.close();
        }
    }

    private static void searchDimAgeGroupForKey(Connection con, AgeGroup dAgeGroup) throws SQLException {
        PreparedStatement pstmtSearchAgeGroup = con.prepareStatement("SELECT AG_KEY FROM PTMAHENT.DIM_AGE_GROUP WHERE AG_ID = ? ");
        pstmtSearchAgeGroup.clearParameters();

        // Search Age Group
        pstmtSearchAgeGroup.setLong(1, dAgeGroup.getAgeGroupId());
        ResultSet rsSearch = pstmtSearchAgeGroup.executeQuery();
        dAgeGroup.setAgeGroupKey(-1);
        if (rsSearch.next()) {
            dAgeGroup.setAgeGroupKey(rsSearch.getLong("AG_KEY"));
        }
        rsSearch.close();
        pstmtSearchAgeGroup.close();
    }
}
